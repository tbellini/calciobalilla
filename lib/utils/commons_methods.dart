import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:calciobalilla24/models/EnrollTournament.dart';
import 'package:calciobalilla24/models/PushNotification.dart';
import 'package:calciobalilla24/models/TeamsPartecipant.dart';
import 'package:calciobalilla24/models/TournamentInfo.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';

double getheight(BuildContext context) {
  return MediaQuery.of(context).size.height / 4;
}

String formatTime(int milliseconds) {
  var secs = milliseconds ~/ 1000;
  var hours = (secs ~/ 3600).toString().padLeft(2, '0');
  var minutes = ((secs % 3600) ~/ 60).toString().padLeft(2, '0');
  var seconds = (secs % 60).toString().padLeft(2, '0');
  return "$hours:$minutes:$seconds";
}

getToken() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String? token = sharedPreferences.getString('token');
  return token;
}

isLogged() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String? token = sharedPreferences.getString('token');
  if (token == null) {
    return false;
  } else
    return true;
}

bool distanceCalculator(double lat1, double lon1, double lat2, double lon2, String unit, double range) {
  // 'M' is statute miles (default)
  //            'K' is kilometers
  //            'N' is nautical miles
  double theta = lon1 - lon2;
  double dist = sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * cos(deg2rad(theta));
  dist = acos(dist);
  dist = rad2deg(dist);
  dist = dist * 60 * 1.1515;
  if (unit == 'K') {
    dist = dist * 1.609344;
  } else if (unit == 'N') {
    dist = dist * 0.8684;
  }

  if (dist < range) {
    return true;
  } else
    return false;
}

double deg2rad(double deg) {
  return (deg * pi / 180.0);
}

double rad2deg(double rad) {
  return (rad * 180.0 / pi);
}

getDeviceID() async {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  if (Platform.isAndroid) {
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    print('Running on ${androidInfo.androidId}');
    return androidInfo.androidId.toString();
  } else if (Platform.isIOS) {
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    print('Running on ${iosInfo.identifierForVendor}');
    return iosInfo.identifierForVendor.toString();
  }
}

getListFromRange(List list, int start, int end) {
  List listReturned = [];
  if (end <= list.length) {
    for (var i = start; i < end; i++) {
      listReturned.add(list[i]);
    }
    return listReturned;
  } else
    return list;
}


Future<String> getRole() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String? role = sharedPreferences.getString('role');
  if (role != null) {
    return role;
  } else
    return 'user';
}

String getNotSpacedLowercaseString(String text) {
  String textMod;
  textMod = text.split(' ')[0];
  textMod = text.toLowerCase();
  print("");
  return textMod;
}

List<String> getPartecipantsUsersID(List<TeamsPartecipant> partecipantsList) {
  List<String> usersIdString = [];
  for (TeamsPartecipant team in partecipantsList) {
    usersIdString.add(team.playerOneId);
    usersIdString.add(team.playerTwoId);
  }
  return usersIdString;
}

changeRole(String role) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  sharedPreferences.setString('role', role);
}

//CREATE(POST)
authenticatedPost(url, body) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var _token = sharedPreferences.getString("token");
  var response = await http.post(url, headers: {HttpHeaders.authorizationHeader: 'Bearer $_token'}, body: body);
  return response;
}

authenticatedPostToken(url, body, token) async {
  var response = await http.post(url, headers: {"Authorization": "key=$token", "content-type": "application/json"}, body: jsonEncode(body));
  return response;
}

goToWidget(BuildContext context, Widget widget) {
  Navigator.pushReplacement(
    context,
    MaterialPageRoute(builder: (context) => widget),
  );
}

backFunction(BuildContext context, String routeName) {
  Navigator.of(context).pushReplacementNamed(routeName);
}

checkInternet(BuildContext context, {String? title, String? content}) async {
  try {
    final result = await InternetAddress.lookup('example.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {}
  } on SocketException catch (_) {
    print('not connetted');
    showDialog(
      context: context,
      builder: (_) => new AlertDialog(
          title: new Text(
            title == null ? "NESSUNA CONNESSIONE" : title,
            style: TextStyle(
              letterSpacing: 1,
            ),
          ),
          content: new Text(
            content == null ? "Per favore, connettersi ad internet!" : content,
            style: TextStyle(fontSize: 12),
          ),
          actions: <Widget>[
            new TextButton(
                onPressed: () {
                  Navigator.of(context).pushReplacementNamed("pages");
                },
                child: Text("Connesso"))
          ]),
    );
  }
}

void registerNotification(FirebaseMessaging _messaging) async {
  // Add the following line
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  // 3. On iOS, this helps to take the user permissions
  NotificationSettings settings = await _messaging.requestPermission(
    alert: true,
    badge: true,
    provisional: false,
    sound: true,
  );

  if (settings.authorizationStatus == AuthorizationStatus.authorized) {
    print('User granted permission');
    // For handling the received notifications
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      // Parse the message received
      PushNotification notification = PushNotification(
        title: message.notification?.title,
        body: message.notification?.body,
      );
    });
  } else {
    print('User declined or has not accepted permission');
  }
}

Future _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
}

nothing() {}

apiPost(url, body) async {
  print(body);
  var response = await http.post(url, body: body);
  return response;
}

//READ(GET)
authenticatedGet(url) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var _token = sharedPreferences.getString("token");
  var response = await http.get(url, headers: {
    HttpHeaders.authorizationHeader: 'Bearer $_token',
  });
  return response;
}

apiGet(url) async {
  var response = await http.get(
    url,
  );
  return response;
}

//UPDATE(PUT)
apiPut(url, body) async {
  var response = await http.put(
    url,
    body: body,
  );
  return response;
}

authenticatedPut(url, body) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var _token = sharedPreferences.getString("token");
  var response = await http.put(url, body: body, headers: {
    HttpHeaders.authorizationHeader: 'Bearer $_token',
  });
  return response;
}

//DELETE(DELETE)

apiDelete(url) async {
  var response = http.delete(url);
  return response;
}

authenticatedDelete(url) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var _token = sharedPreferences.getString("token");
  var response = http.delete(url, headers: {
    HttpHeaders.authorizationHeader: 'Bearer $_token',
  });
  return response;
}

List<String> getListFromJson(List jsonList, String key) {
  List<String> stringList = [];
  for (var item in jsonList) {
    stringList.add(item[key]);
  }
  return stringList;
}

getElementFromJson(List jsonList, String searchKey, String usedKey, String element) {
  for (var item in jsonList) {
    if (item[searchKey] == element) {
      return item[usedKey];
    }
  }
}
