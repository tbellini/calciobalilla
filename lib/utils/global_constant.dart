import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

const double generalPadding = 30.0;

//API CONSTANT
final String API_BASE = "http://console.calciobalilla.coricloud01a.coriweb.it/api/v1/";

//account
final Uri API_LOGIN = Uri.parse(API_BASE + "account/login/");
final Uri API_REGISTRAZIONE = Uri.parse(API_BASE + "account/registration/");
final Uri API_FORGOT_PASSWORD = Uri.parse(API_BASE + "account/forgot-password/");
//Friends
final Uri API_SEND_REQUEST_FRIEND = Uri.parse(API_BASE + "friends/send-request/");
final Uri API_RESPONSE_REQUEST_FRIEND = Uri.parse(API_BASE + "friends/response-request/");
final Uri API_REMOVE_FRIEND = Uri.parse(API_BASE + "friends/remove-friend/");
//Tournament
final Uri API_CREATE_TOURNAMENT = Uri.parse(API_BASE + "tournament/create/");
final Uri API_UPDATE_TOURNAMENT = Uri.parse(API_BASE + "tournament/update/");
final Uri API_GET_TOURNAMENTS_LIST = Uri.parse(API_BASE + "tournament/list/");
final Uri API_TOURNAMENTS_START = Uri.parse(API_BASE + "tournament/start/");

//Inscription
final Uri API_SEND_TOURNAMENT_REQUEST = Uri.parse(API_BASE + "inscription/send-request/");
//Location
final Uri API_CREATE_LOCATION = Uri.parse(API_BASE + "location/create/");
final Uri API_UPDATE_LOCATION = Uri.parse(API_BASE + "location/update/");
//Notification
final Uri API_SEND_NOTIFICATION = Uri.parse(API_BASE + "push/send-notification");

//dynamic constant

//Account
Uri API_GET_USER(String id) {
  return Uri.parse(API_BASE + "account/get-profile-" + id);
}

Uri API_UPDATE_USER(String id) {
  return Uri.parse(API_BASE + "account/update-profile-" + id);
}

//Friends
Uri API_GET_REQUESTS_LIST(String id) {
  return Uri.parse(API_BASE + "friends/get-requestList-" + id);
}

Uri API_SEARCH_FRIENDS(int pagesNumber, int skipPagesNumber) {
  return Uri.parse(API_BASE + "friends/search-user-" + pagesNumber.toString() + "-" + skipPagesNumber.toString());
}

Uri API_GET_FRIENDS(String id) {
  return Uri.parse(API_BASE + "friends/get-friendsList-" + id);
}

//Tournament
Uri API_GET_TOURNAMENT(String id) {
  return Uri.parse(API_BASE + "tournament/" + id);
}

Uri API_DELETE_TOURNAMENT(String id, String idUser) {
  return Uri.parse(API_BASE + "tournament/delete-" + id + "/" + idUser);
}

Uri API_GET_TOURNAMENT_LIST_BY_ID(String idUser) {
  return Uri.parse(API_BASE + "tournament/list/" + idUser);
}

Uri API_GET_PARTECIPANTS_LIST(int tournamentId) {
  return Uri.parse(API_BASE + "tournament/partecipant/" + tournamentId.toString());
}

//Inscription

Uri API_GET_ENROLL_TOURNAMENT_LIST(String idUser) {
  return Uri.parse(API_BASE + "inscription/list-request/" + idUser);
}

Uri API_REPLY_ENROLL_TOURNAMENT(int idRequest, int idState) {
  return Uri.parse(API_BASE + "inscription/reply-request/" + idRequest.toString() + "/" + idState.toString());
}

Uri API_GET_ENROLL_SCHEDULED(String idUser) {
  return Uri.parse(API_BASE + "inscription/scheduled/" + idUser);
}

Uri API_GET_ENROLL_ENDED(String idUser) {
  return Uri.parse(API_BASE + "inscription/ended/" + idUser);
}

Uri API_GET_ENROLL_KILLED(String idUser) {
  return Uri.parse(API_BASE + "inscription/killed/" + idUser);
}

Uri API_GET_ENROLL_WAITING(String idUser) {
  return Uri.parse(API_BASE + "inscription/waiting/" + idUser);
}

Uri API_DELETE_ENROLL_TORUNAMENT(int idEnroll) {
  return Uri.parse(API_BASE + "inscription/delete-enroll/" + idEnroll.toString());
}

//Location
Uri API_GET_LOCATION_LIST(String id) {
  return Uri.parse(API_BASE + "location/list/" + id);
}

Uri API_DELETE_LOCATION(String id, String idUser) {
  return Uri.parse(API_BASE + "location/delete-" + id + "/" + idUser);
}

Uri API_GET_LOCATION_DETAIL(int id) {
  return Uri.parse(API_BASE + "location/" + id.toString());
}

final String API_KEY_GOOGLE = "AIzaSyC_ro1wJPbfP1LYANhrdZnYYYR8EgEykTc";

//LIST constant
List genderList = [
  {"code": "m", "name": "Maschio"},
  {"code": "f", "name": "Femmina"}
];

//Style constants
final double BOTTOM_BAR_HEIGHT = 65;
final double APP_BAR_HEIGHT = 85;
final double PROGRESS_INDICATOR_HEIGHT = 6;

final double STANDARD_HEAD_HEIGHT = 280;
final double AUTH_HEIGHT = 235;
final double MAIN_TITLE_SIZE = 22;
final FontWeight MAIN_TITLE_WEIGHT = FontWeight.bold;
final double DESCRIPTION_SIZE = 15;
final FontWeight DESCRIPTION_WEIGHT = FontWeight.w300;
final double SECONDARY_TITLE_SIZE = 25;
final FontWeight SECONDARY_TITLE_WEIGHT = FontWeight.bold;
final EdgeInsets TILE_CONTENT_PADDING = EdgeInsets.symmetric(vertical: 15, horizontal: 20);
final double RADIUS_STANDARD_CONTAINER_BORDER = 30;

//Text constant
final ALPHANUMERIC_VALIDATOR = RegExp(r'^[a-zA-Z0-9]+$');
final NUMBERS_VALIDATOR = RegExp(r'^[0-9]{10}$');
final format = DateFormat("dd-MM-yyyy");
final formatHour = DateFormat("dd-MM-yyyy HH:mm");

//Object constant
final String URL_AVATAR_F = "https://www.kindpng.com/picc/m/163-1636340_user-avatar-icon-avatar-transparent-user-icon-png.png";
final String URL_AVATAR_M = "https://www.kindpng.com/picc/m/78-786207_user-avatar-png-user-avatar-icon-png-transparent.png";
final int PAGINATOR_LIST_LENGHT = 10;
final int PHISICS_STANDARD_LIST_LIMIT = 4;

//Colors costants

final Color PRIMARY_LIGHT_COLOR = Colors.grey[100]!;
final Color SECONDARY_DARK_COLOR = Color(0xFF7E7E7E);
