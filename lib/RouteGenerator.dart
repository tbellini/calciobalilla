import 'package:calciobalilla24/screens/Admin/edit_location.dart';
import 'package:calciobalilla24/screens/Admin/edit_profile_admin.dart';
import 'package:calciobalilla24/screens/Admin/edit_tournament.dart';
import 'package:calciobalilla24/screens/Admin/new_location.dart';
import 'package:calciobalilla24/screens/Admin/new_tournament.dart';
import 'package:calciobalilla24/screens/Admin/user_detail_admin.dart';
import 'package:calciobalilla24/screens/Tournament/admin/dashboard_sub_pages/dashboard_current.dart';
import 'package:calciobalilla24/screens/Tournament/user/eliminatory_phase_detail.dart';
import 'package:calciobalilla24/screens/Tournament/user/eliminatory_phase_ranking.dart';
import 'package:calciobalilla24/screens/Tournament/user/group_stage_detail.dart';
import 'package:calciobalilla24/screens/Tournament/user/group_stage_ranking.dart';
import 'package:calciobalilla24/screens/Tournament/user/scoreboard.dart';
import 'package:calciobalilla24/screens/Tournament/user/tournament_dashboard.dart';
import 'package:calciobalilla24/screens/User/dashboard.dart';
import 'package:calciobalilla24/screens/User/edit_profile.dart';
import 'package:calciobalilla24/screens/User/map.dart';
import 'package:calciobalilla24/screens/User/tournament_detail.dart';
import 'package:calciobalilla24/screens/User/tournaments.dart';
import 'package:calciobalilla24/screens/User/tournaments_partecipations.dart';
import 'package:calciobalilla24/screens/User/user_detail.dart';
import 'package:calciobalilla24/screens/auth/forgot_password/forgot_password.dart';
import 'package:calciobalilla24/screens/auth/privacy/privacy.dart';
import 'package:calciobalilla24/screens/auth/register/register.dart';
import 'package:calciobalilla24/screens/test-widget.dart';
import 'package:flutter/material.dart';
import 'screens/auth/login/login.dart';
import 'package:calciobalilla24/widgets/pages.dart';

class RouteGenerator {
  static Route<dynamic>? generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    switch (settings.name) {
      case 'login':
        return MaterialPageRoute(builder: (_) => Login());
      case 'test-widget':
        return MaterialPageRoute(builder: (_) => ShowWidgetPage());
      case 'pages':
        return MaterialPageRoute(builder: (_) => PagesBottomTabbar(currentTab: 0));
      case 'signin':
        return MaterialPageRoute(builder: (_) => Signin());
      case 'dashboard':
        return MaterialPageRoute(builder: (_) => Dashboard());
      case 'map':
        return MaterialPageRoute(builder: (_) => MapPage());
      case 'user-detail':
        return MaterialPageRoute(builder: (_) => UserDetail());
      case 'user-detail-admin':
        return MaterialPageRoute(builder: (_) => UserDetailAdmin());
      case 'edit-profile':
        return MaterialPageRoute(builder: (_) => EditProfile());
      case 'tournaments-partecipations':
        return MaterialPageRoute(builder: (_) => TournamentsPartecipations());
      case 'new-tournament':
        return MaterialPageRoute(builder: (_) => NewTournament());
      case 'edit-tournament':
        return MaterialPageRoute(builder: (_) => EditTournament());
      case 'edit-profile-admin':
        return MaterialPageRoute(builder: (_) => EditProfileAdmin());
      case 'edit-place':
        return MaterialPageRoute(builder: (_) => EditLocation());
      case 'tournaments':
        return MaterialPageRoute(builder: (_) => Tournaments());
      case 'tournament-dashboard':
        return MaterialPageRoute(builder: (_) => TournamentDashboard());
      case 'scoreboard':
        return MaterialPageRoute(builder: (_) => Scoreboard());
      case 'group-stage-detail':
        return MaterialPageRoute(builder: (_) => GroupSatgeDetail());
      case 'group-stage-ranking':
        return MaterialPageRoute(builder: (_) => GroupStageRanking());
      case 'eliminatory-detail':
        return MaterialPageRoute(builder: (_) => EliminatoryPhaseDetail());
      case 'eliminatory-ranking':
        return MaterialPageRoute(builder: (_) => EliminatoryPhaseRanking());
      case 'dashboard-current':
        return MaterialPageRoute(builder: (_) => DashboardCurrent());
      case 'forgot-password':
        return MaterialPageRoute(builder: (_) => ForgotPassword());
      case 'privacy':
        return MaterialPageRoute(builder: (_) => Privacy());
      case 'new-location':
        return MaterialPageRoute(builder: (_) => NewLocation());
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
