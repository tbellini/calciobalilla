class Mode {
  Mode(
      {this.id='', this.name='', this.description=''});
  String id;
  String name;
  String description;

  Mode fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return Mode(
        id: dynamicData['id'],
        name: dynamicData['name'],
        description: dynamicData['description'],
      );
    } else {
      return Mode();
    }
  }

  Map<String, dynamic> toMap(Mode object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['id'] = object.id;
      data['name'] = object.name;
      data['description'] = object.description;
      return data;
    } else {
      return <String, dynamic>{};
    }
  }

  List<Mode> fromMapList(List<dynamic> dynamicDataList) {
    final List<Mode> subUserList = <Mode>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subUserList.add(fromMap(dynamicData));
        }
      }
    }
    return subUserList;
  }

  List<Map<String, dynamic>> toMapList(List<Mode> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Mode data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }

    return mapList;
  }
}
