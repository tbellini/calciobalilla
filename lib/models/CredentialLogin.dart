class CredentialLogin {
  String username = "";
  String password = "";
  String deviceID = "";
  // String deviceName = "";

  CredentialLogin.fromJSON(Map<String, dynamic> jsonMap) {
    username = jsonMap["username"];
    password = jsonMap["password"];
    deviceID = jsonMap["DeviceToken"];
    // deviceName = jsonMap["DeviceName"];
  }

  CredentialLogin();

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = username;
    map["password"] = password;
    map["DeviceToken"] = deviceID;
    // map["DeviceName"] = deviceName;
    return map;
  }
}
