import 'package:calciobalilla24/models/Team.dart';
import 'package:calciobalilla24/models/Match.dart' as match;

class Round {
  Round(
      {this.id='', this.name='', this.teams, this.tournamentId='', this.matches});
  String id;
  String name;
  List<Team>? teams;
  String tournamentId;
  List<match.Match>? matches;

  Round fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return Round(
        id: dynamicData['id'],
        name: dynamicData['name'],
        teams: Team().fromMapList(dynamicData['teams']),
        matches: match.Match().fromMapList(dynamicData['matches']),
        tournamentId: dynamicData['tournamentId'],
      );
    } else {
      return Round();
    }
  }

  Map<String, dynamic> toMap(Round object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['id'] = object.id;
      data['name'] = object.name;
      data['teams'] = Team().toMapList(object.teams);
      data['matches'] = match.Match().toMapList(object.matches);
      data['tournamentId'] = object.tournamentId;
      return data;
    } else {
      return <String, dynamic>{};
    }
  }

  List<Round> fromMapList(List<dynamic> dynamicDataList) {
    final List<Round> subUserList = <Round>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subUserList.add(fromMap(dynamicData));
        }
      }
    }
    return subUserList;
  }

  List<Map<String, dynamic>> toMapList(List<Round> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Round data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }

    return mapList;
  }
}
