class User {
  String firstname = "";
  String lastname = "";
  String email = "";
  String password = "";
  String phoneNumber = "";
  //questo campo sarà vuoto e si riempirà quando verrà effettuato il get user profile
  String username = "";
  String id = "";
  String appRole = "";
  String gender = "";
  DateTime birthday = DateTime.now();
  DateTime dateExpire = DateTime.now();

  User();

  User.fromJSON(Map<String, dynamic> jsonMap) {
    id = jsonMap["id"] == null ? "" : jsonMap["id"];
    firstname = jsonMap["firstname"];
    lastname = jsonMap["lastname"];
    email = jsonMap["email"];
    password = jsonMap["password"] == null ? "" : jsonMap["password"];
    birthday = DateTime.parse(jsonMap["birthday"]);
    dateExpire = jsonMap["dateExpire"] == null ? DateTime(0) : DateTime.parse(jsonMap["dateExpire"]);
    appRole = jsonMap["appRole"] == null ? "" : jsonMap["appRole"];
    phoneNumber = jsonMap["phoneNumber"] == null ? "" : jsonMap["phoneNumber"];
    username = jsonMap["username"];
    gender = jsonMap["gender"];
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["firstname"] = firstname;
    map["lastname"] = lastname;
    map["email"] = email;
    map["password"] = password;
    map["appRole"] = appRole.toString();
    map["birthday"] = birthday.toString();
    map["dateExpire"] = dateExpire.toString();
    map["phoneNumber"] = phoneNumber;
    map["username"] = username;
    map["gender"] = gender;
    return map;
  }

  List<User> fromMapList(List<dynamic> dynamicDataList) {
    final List<User> subUserList = <User>[];
    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subUserList.add(User.fromJSON(dynamicData));
        }
      }
    }
    return subUserList;
  }

  // static const empty = User(id: '-');

  // const User(
  //     {this.id = '',
  //     this.username = '',
  //     this.email = '',
  //     this.role,
  //     this.avatar,
  //     this.firstName = '',
  //     this.lastName = '',
  //     this.address,
  //     this.active = true,
  //     this.friends});
  // final String id;
  // final String username;
  // final String email;
  // final Role? role;
  // final Avatar? avatar;
  // final String firstName;
  // final String lastName;
  // final Address? address;
  // final bool active;
  // final List<User?>? friends;

  // User fromMap(dynamic dynamicData) {
  //   if (dynamicData != null) {
  //     return User(
  //       id: dynamicData['id'],
  //       username: dynamicData['username'],
  //       email: dynamicData['email'],
  //       firstName: dynamicData['firstName'],
  //       lastName: dynamicData['lastName'],
  //       active: dynamicData['active'],
  //       role: Role().fromMap(dynamicData['role']),
  //       avatar: Avatar().fromMap(dynamicData['avatar']),
  //       address: Address().fromMap(dynamicData['address']),
  //       friends: User().fromMapList(dynamicData['friends']),
  //     );
  //   } else {
  //     return User();
  //   }
  // }

  // Map<String, dynamic> toMap(User object) {
  //   if (object != null) {
  //     final Map<String, dynamic> data = <String, dynamic>{};
  //     data['id'] = object.id;
  //     data['username'] = object.username;
  //     data['email'] = object.email;
  //     data['role'] =  Role().toMap(object.role);
  //     data['avatar'] = Avatar().toMap(object.avatar);
  //     data['firstName'] = object.firstName;
  //     data['lastName'] = object.lastName;
  //     data['address'] = Address().toMap(object.address);
  //     data['active'] = object.active;
  //     data['friends'] = User().toMapList(object.friends);
  //     return data;
  //   } else {
  //     return <String, dynamic>{};
  //   }
  // }

  // List<Map<String, dynamic>> toMapList(List<User?>? objectList) {
  //   final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
  //   if (objectList != null) {
  //     for (User? data in objectList) {
  //       if (data != null) {
  //         mapList.add(toMap(data));
  //       }
  //     }
  //   }

  //   return mapList;
  // }
}
