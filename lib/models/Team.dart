import 'package:calciobalilla24/models/User.dart';

class Team {
  Team(
      {this.id='', this.name='', this.members});
  String id;
  String name;
  List<User>? members;

  Team fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return Team(
        id: dynamicData['id'],
        name: dynamicData['name'],
        members: dynamicData['members'],
      );
    } else {
      return Team();
    }
  }

  Map<String, dynamic> toMap(Team object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['id'] = object.id;
      data['name'] = object.name;
      data['members'] = object.members;
      return data;
    } else {
      return <String, dynamic>{};
    }
  }

  List<Team> fromMapList(List<dynamic> dynamicDataList) {
    final List<Team> subUserList = <Team>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subUserList.add(fromMap(dynamicData));
        }
      }
    }
    return subUserList;
  }

  List<Map<String, dynamic>> toMapList(List<Team>? objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Team data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }

    return mapList;
  }
}
