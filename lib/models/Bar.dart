import 'package:calciobalilla24/models/Address.dart';

class Bar {
  Bar(
      {this.id='', this.name='', this.address, this.adminId='', this.managerId=''});
  String id;
  String name;
  String adminId;
  String managerId;
  Address? address;

  Bar fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return Bar(
        id: dynamicData['id'],
        name: dynamicData['name'],
        address: dynamicData['address'],
        adminId: dynamicData['adminId'],
        managerId: dynamicData['managerId'],
      );
    } else {
      return Bar();
    }
  }

  Map<String, dynamic> toMap(Bar object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['id'] = object.id;
      data['name'] = object.name;
      data['address'] = object.address;
      data['adminId'] = object.adminId;
      data['managerId'] = object.managerId;
      return data;
    } else {
      return <String, dynamic>{};
    }
  }

  List<Bar> fromMapList(List<dynamic> dynamicDataList) {
    final List<Bar> subUserList = <Bar>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subUserList.add(fromMap(dynamicData));
        }
      }
    }
    return subUserList;
  }

  List<Map<String, dynamic>> toMapList(List<Bar> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Bar data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }

    return mapList;
  }
}
