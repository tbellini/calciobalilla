class FriendRequest {
  int? id;
  String senderId = "";
  String firstname = "";
  String lastname = "";
  String email = "";
  String gender = "";
  int? state;

  FriendRequest();

  FriendRequest.fromJSON(Map<String, dynamic> jsonMap) {
    id = jsonMap["Id"];
    senderId = jsonMap["SenderId"] == null ? "" : jsonMap["SenderId"];
    firstname = jsonMap["Firstname"] == null ? "" : jsonMap["Firstname"];
    lastname = jsonMap["Lastname"] == null ? "" : jsonMap["Lastname"];
    email = jsonMap["Email"] == null ? "" : jsonMap["Email"];
    gender = jsonMap["Gender"] == null ? "" : jsonMap["Gender"];
    state = jsonMap["State"];
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["Id"] = id;
    map["SenderId"] = senderId;
    map["Firstname"] = firstname;
    map["Lastname"] = lastname;
    map["Email"] = email;
    map["Gender"] = gender;
    map["State"] = state;
    return map;
  }

  List<FriendRequest> fromMapList(List<dynamic> dynamicDataList) {
    final List<FriendRequest> subFriendRequestList = <FriendRequest>[];
    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subFriendRequestList.add(FriendRequest.fromJSON(dynamicData));
        }
      }
    }
    return subFriendRequestList;
  }
}
