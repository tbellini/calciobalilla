import 'package:calciobalilla24/models/Location.dart';
import 'package:calciobalilla24/models/Tournament.dart';

class EnrollTournament {
  int? id;
  int? tournamentId;
  String playerOne = "";
  String playerOneId = "";
  int? inscriptionState;
  DateTime deleteDate = DateTime.now();
  Location? location;
  Tournament? tournament;

  EnrollTournament();

  EnrollTournament.fromJSON(Map<String, dynamic> jsonMap) {
    id = jsonMap["Id"];
    tournamentId = jsonMap["TournamentId"];
    inscriptionState = jsonMap["InscriptionState"];
    if (jsonMap["Location"] != null) location = Location.fromJSON(jsonMap["Location"]);
    if (jsonMap["Tournament"] != null) tournament = Tournament.fromJSON(jsonMap["Tournament"]);
    playerOne = jsonMap["PlayerOne"] == null ? "" : jsonMap["PlayerOne"];
    playerOneId = jsonMap["PlayerTwo"] == null ? "" : jsonMap["PlayerTwo"];
    deleteDate = DateTime.parse(jsonMap["DeleteDate"]);
  }

  List<EnrollTournament> fromMapList(List<dynamic> dynamicDataList) {
    final List<EnrollTournament> subEnrollTournamentList = <EnrollTournament>[];
    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subEnrollTournamentList.add(EnrollTournament.fromJSON(dynamicData));
        }
      }
    }
    return subEnrollTournamentList;
  }
}
