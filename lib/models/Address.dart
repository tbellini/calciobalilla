class Address {
  Address(
      {this.address = '',
      this.city = '',
      this.zip='',
      this.province='',
      this.country='',
      this.lon='',
      this.lng=''
      });
  String address;
  String city;
  String zip;
  String province;
  String country;
  String lon;
  String lng;

  Address fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return Address(
        address: dynamicData['address'],
        city: dynamicData['city'],
        zip: dynamicData['zip'],
        province: dynamicData['province'],
        country: dynamicData['country'],
        lon: dynamicData['lon'],
        lng: dynamicData['lng'],
      );
    } else {
      return Address();
    }
  }

  Map<String, dynamic> toMap(Address? object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['address'] = object.address;
      data['city'] = object.city;
      data['zip'] = object.zip;
      data['province'] = object.province;
      data['country'] = object.country;
      data['lon'] = object.lon;
      data['lng'] = object.lng;
      return data;
    } else {
      return <String, dynamic>{};
    }
  }

  List<Address> fromMapList(List<dynamic> dynamicDataList) {
    final List<Address> subUserList = <Address>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subUserList.add(fromMap(dynamicData));
        }
      }
    }
    return subUserList;
  }

  List<Map<String, dynamic>> toMapList(List<Address> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Address data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }

    return mapList;
  }
}
