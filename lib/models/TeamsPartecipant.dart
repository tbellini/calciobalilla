class TeamsPartecipant {
  int? id;
  int? state;
  int? tournamentId;
  String playerOneId = "";
  String playerOneName = "";
  String playerTwoId = "";
  String playerTwoName = "";
  bool isChecked = false;

  TeamsPartecipant();

  TeamsPartecipant.fromJSON(Map<String, dynamic> jsonMap) {
    id = jsonMap["Id"];
    state = jsonMap["State"];
    tournamentId = jsonMap["TournamentId"];
    playerOneId = jsonMap["PlayerOneId"];
    playerOneName = jsonMap["PlayerOneName"];
    playerTwoId = jsonMap["PlayerTwoId"];
    playerTwoName = jsonMap["PlayerTwoName"];
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["Id"] = id.toString();
    map["State"] = state.toString();
    map["TournamentId"] = tournamentId.toString();
    map["PlayerOneId"] = playerOneId;
    map["PlayerOneName"] = playerOneName;
    map["PlayerTwoId"] = playerTwoId;
    map["PlayerTwoName"] = playerTwoName;
    return map;
  }

  List<TeamsPartecipant> fromMapList(List<dynamic> dynamicDataList) {
    final List<TeamsPartecipant> subTeamsPartecipantList = <TeamsPartecipant>[];
    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subTeamsPartecipantList.add(TeamsPartecipant.fromJSON(dynamicData));
        }
      }
    }
    return subTeamsPartecipantList;
  }
}
