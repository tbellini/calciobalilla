class Location {
  int? id;
  String title = "";
  DateTime deleteDate = DateTime.now();
  String address = "";
  String city = "";
  double? latitude;
  double? longitude;
  String userId = "";
  String imageCover = "";

  Location();

  Location.fromJSON(Map<String, dynamic> jsonMap) {
    id = jsonMap["Id"];
    title = jsonMap["Title"] == null ? "" : jsonMap["Title"];
    address = jsonMap["Address"] == null ? "" : jsonMap["Address"];
    city = jsonMap["City"] == null ? "" : jsonMap["City"];
    userId = jsonMap["UserId"] == null ? "" : jsonMap["UserId"];
    deleteDate = jsonMap["DeleteDate"] == null ? DateTime.now() : DateTime.parse(jsonMap["DeleteDate"]);
    latitude = jsonMap["Latitude"];
    longitude = jsonMap["Longitude"];
    //Url for network image
    imageCover = jsonMap["ImageCover"] == null ? "" : jsonMap["ImageCover"];
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["Id"] = id.toString();
    map["Title"] = title;
    map["Address"] = address;
    map["City"] = city;
    map["UserId"] = userId;
    map["Latitude"] = latitude.toString();
    map["Longitude"] = longitude.toString();
    //send in base 64
    map["ImageCover"] = imageCover;
    return map;
  }

  List<Location> fromMapList(List<dynamic> dynamicDataList) {
    final List<Location> subLocationList = <Location>[];
    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subLocationList.add(Location.fromJSON(dynamicData));
        }
      }
    }
    return subLocationList;
  }
}
