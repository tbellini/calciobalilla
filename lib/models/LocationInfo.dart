class LocationInfo {
  int? id;
  String title = "";
  String address = "";
  String city = "";
  String postalCode = "";
  String imageCover = "";

  LocationInfo();

  LocationInfo.fromJSON(Map<String, dynamic> jsonMap) {
    id = jsonMap["Id"];
    title = jsonMap["Title"] == null ? "" : jsonMap["Title"];
    address = jsonMap["Address"] == null ? "" : jsonMap["Address"];
    city = jsonMap["City"] == null ? "" : jsonMap["City"];
    postalCode = jsonMap["PostalCode"] == null ? "" : jsonMap["PostalCode"];
    imageCover = jsonMap["ImageCover"] == null ? "" : jsonMap["ImageCover"];
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["Id"] = id;
    map["Title"] = title;
    map["Address"] = address;
    map["City"] = city;
    map["PostalCode"] = postalCode;
    map["ImageCover"] = imageCover;
    return map;
  }

  List<LocationInfo> fromMapList(List<dynamic> dynamicDataList) {
    final List<LocationInfo> subLocationInfoList = <LocationInfo>[];
    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subLocationInfoList.add(LocationInfo.fromJSON(dynamicData));
        }
      }
    }
    return subLocationInfoList;
  }
}
