class Permission {
  Permission(
      {this.id = '',
      this.code = '',
      this.name = '',
      this.description = '',
      });
  String id;
  String code;
  String name;
  String description;

  Permission fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return Permission(
        id: dynamicData['id'],
        code: dynamicData['code'],
        name: dynamicData['name'],
        description: dynamicData['description'],
      );
    } else {
      return Permission();
    }
  }

  Map<String, dynamic> toMap(Permission object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['id'] = object.id;
      data['code'] = object.code;
      data['name'] = object.name;
      data['description'] = object.description;
      return data;
    } else {
       return <String, dynamic>{};
    }
  }

  List<Permission> fromMapList(List<dynamic>? dynamicDataList) {
    final List<Permission> subUserList = <Permission>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subUserList.add(fromMap(dynamicData));
        }
      }
    }
    return subUserList;
  }

  List<Map<String, dynamic>> toMapList(List<Permission>? objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Permission data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }

    return mapList;
  }
}
