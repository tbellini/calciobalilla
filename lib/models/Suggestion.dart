// For storing our result
class Suggestion {
  String placeId = "";
  String description = "";
  String mainText = "";
  String secondaryText = "";

  Suggestion();

  Suggestion.fromJSON(Map<String, dynamic> jsonMap) {
    placeId = jsonMap["place_id"];
    description = jsonMap["description"];
    mainText = jsonMap["structured_formatting"]["main_text"];
    secondaryText = jsonMap["structured_formatting"]["secondary_text"];
  }

  List<Suggestion> fromMapList(List<dynamic> dynamicDataList) {
    final List<Suggestion> subSuggestionList = <Suggestion>[];
    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subSuggestionList.add(Suggestion.fromJSON(dynamicData));
        }
      }
    }
    return subSuggestionList;
  }

  @override
  String toString() {
    return 'Suggestion(description: $description, placeId: $placeId, mainText: $mainText, secondarytext: $secondaryText)';
  }
}
