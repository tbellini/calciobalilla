import 'dart:convert';

class Notification {
  String title = "";
  String body = "";
  String clickAction = "";
  String channelId = "";
  String collapseKey = "";
  String pushType = "";
  String instantExpiration = "";
  String payload = "";
  dynamic idUsers = [];

  Notification();

  Notification.fromJSON(Map<String, dynamic> jsonMap) {
    title = jsonMap["title"] == null ? "" : jsonMap["title"];
    //subtitle or description
    body = jsonMap["body"];
    clickAction = jsonMap["clickAction"];
    channelId = jsonMap["channelId"];
    collapseKey = jsonMap["collapseKey"];
    pushType = jsonMap["pushType"];
    instantExpiration = jsonMap["instantExpiration"];
    payload = jsonMap["payload"];
    idUsers = jsonMap["userList"];
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["title"] = title;
    map["body"] = body;
    map["clickAction"] = clickAction;
    map["channelId"] = channelId;
    map["collapseKey"] = collapseKey;
    map["pushType"] = pushType;
    map["instantExpiration"] = instantExpiration;
    map["payload"] = payload;
    map["userList"] = jsonEncode(idUsers);
    return map;
  }
}
