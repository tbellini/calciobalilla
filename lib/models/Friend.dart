class Friend {
  String id = "";
  String firstname = "";
  String lastname = "";
  String email = "";
  String gender = "";

  Friend();

  Friend.fromJSON(Map<String, dynamic> jsonMap) {
    id = jsonMap["Id"] == null ? "" : jsonMap["Id"];
    firstname = jsonMap["Firstname"] == null ? "" : jsonMap["Firstname"];
    lastname = jsonMap["Lastname"] == null ? "" : jsonMap["Lastname"];
    email = jsonMap["Email"] == null ? "" : jsonMap["Email"];
    gender = jsonMap["Gender"] == null ? "" : jsonMap["Gender"];
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["Id"] = id;
    map["Firstname"] = firstname;
    map["Lastname"] = lastname;
    map["Email"] = email;
    map["Gender"] = gender;
    return map;
  }

  List<Friend> fromMapList(List<dynamic> dynamicDataList) {
    final List<Friend> subFriendList = <Friend>[];
    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subFriendList.add(Friend.fromJSON(dynamicData));
        }
      }
    }
    return subFriendList;
  }
}
