import 'package:calciobalilla24/models/Location.dart';
import 'package:calciobalilla24/utils/global_constant.dart';

class TournamentInfo {
  int? id;
  String title = "";
  DateTime dateScheduled = DateTime.now();
  Location location = Location();

  TournamentInfo();

  TournamentInfo.fromJSON(Map<String, dynamic> jsonMap) {
    id = jsonMap["Id"];
    if (jsonMap["Location"] != null) location = Location.fromJSON(jsonMap["Location"]);
    title = jsonMap["Title"] == null ? "" : jsonMap["Title"];
    dateScheduled = DateTime.parse(jsonMap["DateScheduled"]);
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["Id"] = id.toString();
    map["Title"] = title;
    map["DateScheduled"] = dateScheduled.toString();
    return map;
  }

  List<TournamentInfo> fromMapList(List<dynamic> dynamicDataList) {
    final List<TournamentInfo> subTournamentInfoList = <TournamentInfo>[];
    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subTournamentInfoList.add(TournamentInfo.fromJSON(dynamicData));
        }
      }
    }
    return subTournamentInfoList;
  }
}
