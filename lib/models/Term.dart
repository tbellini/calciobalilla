import 'dart:io';
import 'package:calciobalilla24/models/Team.dart';

class Term {
  Term(
      {this.id='', this.name='', this.description='', this.termFile});
  String id;
  String name;
  String description;
  File? termFile;

  Term fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return Term(
        id: dynamicData['id'],
        name: dynamicData['name'],
        description: dynamicData['description'],
        termFile: dynamicData['termFile'],
      );
    } else {
      return Term();
    }
  }

  Map<String, dynamic> toMap(Term object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['id'] = object.id;
      data['name'] = object.name;
      data['description'] = object.description;
      data['termFile'] = object.termFile;
      return data;
    } else {
      return  <String, dynamic>{};
    }
  }

  List<Term> fromMapList(List<dynamic> dynamicDataList) {
    final List<Term> subUserList = <Term>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subUserList.add(fromMap(dynamicData));
        }
      }
    }
    return subUserList;
  }

  List<Map<String, dynamic>> toMapList(List<Term> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Term data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }

    return mapList;
  }
}
