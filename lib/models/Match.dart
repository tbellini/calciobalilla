import 'package:calciobalilla24/models/Team.dart';

class Match {
  Match(
      {this.id='', this.team1, this.team2, this.points1=0, this.points2=0, this.status='', this.roundId=''});
  String id;
  Team? team1;
  Team? team2;
  int points1;
  int points2;
  String status;
  String roundId;

  Match fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return Match(
        id: dynamicData['id'],
        team1: dynamicData['team1'],
        team2: dynamicData['team2'],
        points1: dynamicData['points1'],
        points2: dynamicData['points2'],
        status: dynamicData['status'],
        roundId: dynamicData['roundId'],
      );
    } else {
      return Match();
    }
  }

  Map<String, dynamic> toMap(Match object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['id'] = object.id;
      data['team1'] = object.team1;
      data['team2'] = object.team2;
      data['points1'] = object.points1;
      data['points2'] = object.points2;
      data['status'] = object.status;
      data['roundId'] = object.roundId;
      return data;
    } else {
      return <String, dynamic>{};
    }
  }

  List<Match> fromMapList(List<dynamic> dynamicDataList) {
    final List<Match> subUserList = <Match>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subUserList.add(fromMap(dynamicData));
        }
      }
    }
    return subUserList;
  }

  List<Map<String, dynamic>> toMapList(List<Match>?  objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Match data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }

    return mapList;
  }
}
