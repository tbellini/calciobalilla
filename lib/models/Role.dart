import 'package:calciobalilla24/models/Permission.dart';

class Role {
  Role(
      {this.id = '',
      this.name = '',
      this.description = '',
      this.permissions
      });
  String id;
  String name;
  String description;
  List<Permission>? permissions;

  Role fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return Role(
        id: dynamicData['id'],
        name: dynamicData['name'],
        description: dynamicData['description'],
        permissions: Permission().fromMapList(dynamicData['permissions']),
      );
    } else {
      return Role();
    }
  }

  Map<String, dynamic> toMap(Role? object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['id'] = object.id;
      data['name'] = object.name;
      data['description'] = object.description;
      data['permissions'] = Permission().toMapList(object.permissions);
      return data;
    } else {
      return <String, dynamic>{};
    }
  }

  List<Role> fromMapList(List<dynamic> dynamicDataList) {
    final List<Role> subUserList = <Role>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subUserList.add(fromMap(dynamicData));
        }
      }
    }
    return subUserList;
  }

  List<Map<String, dynamic>> toMapList(List<Role>? objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Role? data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }

    return mapList;
  }
}
