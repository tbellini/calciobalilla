import 'package:calciobalilla24/models/Location.dart';
import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:geolocator/geolocator.dart';

class Tournament {
  int? id;
  String title = "";
  DateTime dateScheduled = DateTime.now();
  DateTime? dateEnd;
  int? foosballField;
  int? teams;
  int? locationId;
  Location location = new Location();
  String rules = "";
  String notes = "";
  String userId = "";
  DateTime? dateStart;
  Tournament();

  Tournament.fromJSON(Map<String, dynamic> jsonMap) {
    id = jsonMap["Id"];
    foosballField = jsonMap["FoosballField"];
    teams = jsonMap["Teams"];
    if (jsonMap["LocationId"] != null) {
      locationId = jsonMap["LocationId"];
    }
    if (jsonMap["Location"] != null) {
      location = Location.fromJSON(jsonMap["Location"]);
    }
    title = jsonMap["Title"] == null ? "" : jsonMap["Title"];
    rules = jsonMap["Rules"] == null ? "" : jsonMap["Rules"];
    notes = jsonMap["Notes"] == null ? "" : jsonMap["Notes"];
    userId = jsonMap["UserId"] == null ? "" : jsonMap["UserId"];
    dateScheduled = DateTime.parse(jsonMap["DateScheduled"]);
    dateEnd = jsonMap["DateEnd"] == null ? null : DateTime.parse(jsonMap["DateEnd"]);
    dateStart = jsonMap["DateStart"] == null ? null : DateTime.parse(jsonMap["DateStart"]);
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["Id"] = id.toString();
    map["FoosballField"] = foosballField.toString();
    map["Teams"] = teams.toString();
    if (locationId != null) {
      map["LocationId"] = locationId.toString();
    }
    map["Title"] = title;
    map["Rules"] = rules;
    map["UserId"] = userId;
    map["DateScheduled"] = dateScheduled.toString();
    return map;
  }

  List<Tournament> fromMapList(List<dynamic> dynamicDataList) {
    final List<Tournament> subTournamentList = <Tournament>[];
    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subTournamentList.add(Tournament.fromJSON(dynamicData));
        }
      }
    }
    return subTournamentList;
  }
}
