class Avatar {
  Avatar(
      {this.link = ''});
  String link;

  Avatar fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return Avatar(
        link: dynamicData['link'],
      );
    } else {
      return Avatar();
    }
  }

  Map<String, dynamic> toMap(Avatar? object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['link'] = object.link;
      return data;
    } else {
      return <String, dynamic>{};
    }
  }

  List<Avatar> fromMapList(List<dynamic> dynamicDataList) {
    final List<Avatar> subUserList = <Avatar>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subUserList.add(fromMap(dynamicData));
        }
      }
    }
    return subUserList;
  }

  List<Map<String, dynamic>> toMapList(List<Avatar> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Avatar data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }

    return mapList;
  }
}
