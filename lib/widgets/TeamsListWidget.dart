import 'package:calciobalilla24/models/TeamsPartecipant.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:flutter/material.dart';

class TeamsListWidget extends StatelessWidget {
  final List<TeamsPartecipant>? partecipantsList;
  const TeamsListWidget({
    Key? key,
    @required this.partecipantsList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isSelected = false;
    return ListView.separated(
      shrinkWrap: true,
      padding: EdgeInsets.only(top: 10, bottom: 10),
      itemBuilder: (BuildContext context, int index) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
            color: Theme.of(context).backgroundColor,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                    backgroundColor: PRIMARY_LIGHT_COLOR,
                    foregroundImage: NetworkImage(URL_AVATAR_M),
                    radius: 35,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    partecipantsList![index].playerOneName,
                    style: TextStyle(
                      fontSize: 12,
                      color: Theme.of(context).secondaryHeaderColor,
                    ),
                  )
                ],
              ),
              Icon(
                Icons.link_rounded,
                size: 30,
                color: Theme.of(context).secondaryHeaderColor,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                    backgroundColor: PRIMARY_LIGHT_COLOR,
                    foregroundImage: NetworkImage(URL_AVATAR_M),
                    radius: 35,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    partecipantsList![index].playerTwoName,
                    style: TextStyle(
                      color: Theme.of(context).secondaryHeaderColor,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) => SizedBox(
        height: 20,
      ),
      itemCount: partecipantsList!.length,
    );
  }
}
