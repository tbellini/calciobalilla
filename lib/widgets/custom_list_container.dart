import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:flutter/material.dart';

class CustomListContainer extends StatelessWidget {
  final Color? color;
  final String? title;
  final Color? textColor;
  final String? subtitle;
  final Widget? listWidget;
  final int? listLength;
  final void Function()? onPressedIcon;

  const CustomListContainer({
    Key? key,
    @required this.title,
    this.subtitle,
    this.color,
    this.textColor,
    this.onPressedIcon,
    @required this.listWidget,
    this.listLength,
  }) : super(key: key);

  final Color standardTextColor = Colors.black54;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: color != null ? color : PRIMARY_LIGHT_COLOR,
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: Padding(
        padding: EdgeInsets.all(25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                title!,
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700, color: textColor != null ? textColor : standardTextColor),
              ),
              if (subtitle != null)
                Text(
                  subtitle!,
                  style: TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: textColor != null ? textColor : standardTextColor),
                )
            ]),
            Divider(
              thickness: 0.5,
              color: textColor != null ? textColor : standardTextColor,
            ),
            listWidget!,
            if (listLength != null)
              if (listLength! > 3)
                Center(
                  child: Container(
                    child: IconButton(
                      enableFeedback: true,
                      onPressed: this.onPressedIcon,
                      icon: Icon(Icons.arrow_circle_down_rounded),
                      iconSize: 50,
                      color: textColor != null ? textColor : Theme.of(context).hoverColor,
                    ),
                  ),
                )
          ],
        ),
      ),
    );
  }
}
