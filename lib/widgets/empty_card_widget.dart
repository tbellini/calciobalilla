import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/empty_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class EmptyCardWidget extends StatelessWidget {
  final Color? color;
  final String? titleCard;
  final String? titleWidget;
  final double? height;
  final Color? textColor;
  final String? subtitle;
  final Color? firstColor;
  final Color? secondaryColor;
  final Color? textColorEmptyListWidget;
  final Color? iconColor;
  final IconData? icon;
  const EmptyCardWidget(
      {Key? key,
      this.height,
      this.titleCard,
      this.titleWidget,
      this.color,
      this.textColor,
      this.subtitle,
      this.firstColor,
      this.secondaryColor,
      this.textColorEmptyListWidget,
      this.iconColor,
      @required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height != null ? height! : 350,
      decoration: BoxDecoration(
        color: color != null ? color : SECONDARY_DARK_COLOR,
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: Padding(
        padding: EdgeInsets.all(25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                titleCard!,
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700, color: textColor != null ? textColor : Theme.of(context).hoverColor),
              ),
              if (subtitle != null)
                Text(
                  subtitle!,
                  style: TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: textColor != null ? textColor : Theme.of(context).hoverColor),
                )
            ]),
            Divider(
              thickness: 0.5,
              color: textColor != null ? textColor : Theme.of(context).hoverColor,
            ),
            // 150 altezza
            EmptyListWidget(
              containerHeight: height != null ? (height! - 100) : 350,
              icon: icon!,
              title: titleWidget!,
              firstColor: this.firstColor,
              secondaryColor: this.secondaryColor,
              textColor: this.textColorEmptyListWidget,
              iconColor: iconColor,
            )
          ],
        ),
      ),
    );
  }
}
