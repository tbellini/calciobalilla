import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:flutter/material.dart';

class CustomProgressIndicator extends StatelessWidget {
  final double? topSpace;
  final Color? color;
  const CustomProgressIndicator({Key? key, this.color, this.topSpace}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: this.topSpace == null ? MediaQuery.of(context).padding.top : 0,
          ),
          LinearProgressIndicator(
            minHeight: PROGRESS_INDICATOR_HEIGHT,
            color: color != null ? color : Theme.of(context).hintColor,
          ),
        ],
      ),
    );
  }
}
