import 'package:calciobalilla24/screens/Tournament/admin/dashboard_sub_pages/dashboard_current.dart';
import 'package:calciobalilla24/screens/Tournament/user/tournament_dashboard.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

typedef void IdTapCallback(int id);
typedef void IdPressCallback(int index);

class CustomListItem extends StatelessWidget {
  final Color? color;
  final String? title;
  //Must not be null
  final List? list;
  final Function? navigator;
  final int? selector;
  final IdPressCallback? onLongPressed;
  final IdTapCallback? onTap;
  final Color? textColor;
  final String? subtitle;

  CustomListItem(
      {this.color,
      @required this.title,
      @required this.list,
      this.selector,
      this.onLongPressed,
      this.textColor,
      this.subtitle,
      this.navigator,
      this.onTap});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: color != null ? color : SECONDARY_DARK_COLOR,
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: Padding(
        padding: EdgeInsets.all(25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                title!,
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700, color: textColor != null ? textColor : Theme.of(context).hoverColor),
              ),
              if (subtitle != null)
                Text(
                  subtitle!,
                  style: TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: textColor != null ? textColor : Theme.of(context).hoverColor),
                )
            ]),
            Divider(
              thickness: 0.5,
              color: textColor != null ? textColor : Theme.of(context).hoverColor,
            ),
            ListView.separated(
              padding: EdgeInsets.only(top: 5),
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: list!.length,
              separatorBuilder: (BuildContext context, int index) => SizedBox(
                height: 12,
              ),
              itemBuilder: (BuildContext context, int index) {
                switch (selector) {
                  //Rank match;
                  case 0:
                    return Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {},
                        onLongPress: () {},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            //ONLY FOR TEST
                            Text(list![index]["s1"],
                                style: TextStyle(
                                    color: textColor != null ? textColor : Theme.of(context).hoverColor, fontSize: 18, fontWeight: FontWeight.w700)),
                            Text(list![index]["points"],
                                style: TextStyle(
                                    color: textColor != null ? textColor : Theme.of(context).hoverColor, fontSize: 18, fontWeight: FontWeight.w300)),
                            Text(list![index]["s2"],
                                style: TextStyle(
                                    color: textColor != null ? textColor : Theme.of(context).hoverColor, fontSize: 18, fontWeight: FontWeight.w700)),
                          ],
                        ),
                      ),
                    );

                  case 1:
                    return Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {},
                        onLongPress: () {},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            //ONLY FOR TEST
                            Text((index + 1).toString(),
                                style: TextStyle(
                                    color: textColor != null ? textColor : Theme.of(context).hoverColor, fontSize: 18, fontWeight: FontWeight.w300)),
                            SizedBox(
                              width: 20,
                            ),
                            Text(list![index]["s"],
                                style: TextStyle(
                                    color: textColor != null ? textColor : Theme.of(context).hoverColor, fontSize: 18, fontWeight: FontWeight.w700)),
                          ],
                        ),
                      ),
                    );

                  // break;
                  default:
                    return Material(
                        color: Colors.transparent,
                        child: InkWell(
                            onTap: () {
                              onTap!(list![index].id);
                            },
                            onLongPress: () {
                              onLongPressed!(index);
                            },
                            child: Container(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                //ONLY FOR TEST
                                Text(list![index].title,
                                    style: TextStyle(
                                        color: textColor != null ? textColor : Theme.of(context).hoverColor,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700)),
                                Text(formatHour.format(list![index].dateScheduled),
                                    style: TextStyle(
                                        color: textColor != null ? textColor : Theme.of(context).hoverColor,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w300))
                              ],
                            ))));
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
