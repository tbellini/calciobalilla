import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:flutter/material.dart';

class CustomFriendListTile extends StatelessWidget {
  final List? list;
  final int? index;
  const CustomFriendListTile({Key? key, @required this.list, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        backgroundImage: this.list![this.index!].gender == 'm' ? NetworkImage(URL_AVATAR_M) : NetworkImage(URL_AVATAR_F),
        radius: MediaQuery.of(context).size.width / 9,
        backgroundColor: PRIMARY_LIGHT_COLOR,
      ),
      title: Text(
        this.list![this.index!].firstname + " " + this.list![this.index!].lastname,
        style: TextStyle(fontWeight: FontWeight.w700, fontSize: 15),
      ),
      subtitle: Text(
        this.list![this.index!].email,
        style: TextStyle(fontWeight: FontWeight.w300, fontSize: 10),
      ),
    );
  }
}
