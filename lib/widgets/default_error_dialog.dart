import 'package:flutter/material.dart';

class DefaultErrorDialog extends StatelessWidget {
  final String? title;
  final String? subtitle;
  final String? buttonText;
  const DefaultErrorDialog({Key? key, this.title, this.subtitle, this.buttonText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: new Text(title==null ? "Qualcosa è andato storto" : title!),
      content: new Text(subtitle==null ? "per favore, verifica che i dati inseriti siano corretti e che la tua connessione sia stabile." : subtitle!),
      actions: <Widget>[
        TextButton(
          child: Text(buttonText == null ? 'Ok' : buttonText!),
          onPressed: () {
            //sempre pop
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
