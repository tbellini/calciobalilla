import 'package:flutter/material.dart';

class ShimmerCarousel extends StatelessWidget {
  final double? height;
  final double? bottomPadding;
  final EdgeInsetsGeometry? carouselPadding;
  final Color? backgroundColor;

  ShimmerCarousel({
    @required this.height,
    this.carouselPadding,
    this.bottomPadding,
    this.backgroundColor,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(
          bottom: bottomPadding == null ? 30 : bottomPadding!,
        ),
        child: Container(
          height: height,
          child: ListView.separated(
            // default EdgeInsets.only(left: 40, right: 20)
            padding: this.carouselPadding == null ? EdgeInsets.only(left: 40, right: 20) : carouselPadding,
            itemCount: 3,
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                ),
                width: 200,
              );
            },
            separatorBuilder: (context, index) => SizedBox(
              width: 20,
            ),
          ),
        ));
  }
}
