import 'package:flutter/material.dart';

class ShimmerCircle extends StatelessWidget {
  final double? radius;
  const ShimmerCircle({Key? key, @required this.radius}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: (radius!*2),
      height: (radius!*2),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
      ),
    );
  }
}
