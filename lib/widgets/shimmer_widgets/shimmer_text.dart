import 'package:flutter/material.dart';

class ShimmerText extends StatelessWidget {
  final double? mainHeight;
  final double? secondaryHeight;
  final MainAxisAlignment? mainAxis;
  final EdgeInsetsGeometry? padding;
  final double? borderRadius;
  final int? lines;

  const ShimmerText(
    this.lines, {
    Key? key,
    this.mainHeight,
    this.secondaryHeight,
    this.padding,
    this.mainAxis,
    this.borderRadius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding!,
      child: Column(
        mainAxisAlignment: mainAxis != null ? mainAxis! : MainAxisAlignment.center,
        children: [
          Container(
            width: double.infinity,
            height: mainHeight != null ? mainHeight : 25,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(this.borderRadius != null ? this.borderRadius! : 100)),
            ),
          ),
          for (int i = 0; i < lines!; i++)
            Container(
              margin: EdgeInsets.only(top: 10),
              width: double.infinity,
              height: secondaryHeight != null ? secondaryHeight : 15,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(100)),
              ),
            ),
        ],
      ),
    );
  }
}
