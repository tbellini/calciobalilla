import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:flutter/material.dart';

class CustomContainer extends StatelessWidget {
  final Widget? child;
  final Color? color;
  final Color? backgroundColor;
  final EdgeInsetsGeometry? margin;
  final double? height;
  CustomContainer({@required this.child, @required this.color, this.margin, this.backgroundColor, this.height});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(width: 0, color: this.backgroundColor != null ? backgroundColor! : Colors.transparent),
          left: BorderSide(width: 0, color: Colors.transparent),
          right: BorderSide(width: 0, color: Colors.transparent),
          bottom: BorderSide(width: 0, color: color!),
        ),
        color: backgroundColor != null ? backgroundColor : Colors.transparent,
      ),
      margin: margin,
      width: MediaQuery.of(context).size.width,
      child: new Container(
        height: height != null ? height : null,
        decoration: new BoxDecoration(
            // border: Border(bottom: BorderSide(width: 0, color: color!) ),
            border: Border.all(width: 0, color: color!),
            color: color,
            borderRadius: new BorderRadius.only(
              topLeft: Radius.circular(RADIUS_STANDARD_CONTAINER_BORDER),
              topRight: Radius.circular(RADIUS_STANDARD_CONTAINER_BORDER),
            )),
        child: child,
      ),
    );
  }
}
