import 'package:calciobalilla24/RouteGenerator.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/pages.dart';
import 'package:flutter/material.dart';

class EmptyListWidget extends StatelessWidget {
  final double? containerHeight;
  final IconData? icon;
  final String? title;
  final Color? firstColor;
  final Color? secondaryColor;
  final Color? textColor;
  final Color? iconColor;
  final String? textButton;
  final Color? textButtonColor;
  const EmptyListWidget(
      {Key? key,
      @required this.containerHeight,
      @required this.icon,
      @required this.title,
      this.textButton,
      this.textButtonColor,
      this.firstColor,
      this.secondaryColor,
      this.textColor,
      this.iconColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        alignment: AlignmentDirectional.center,
        padding: EdgeInsets.symmetric(horizontal: 30),
        height: this.containerHeight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ClipOval(
              child: Stack(
                children: <Widget>[
                  Container(
                    width: 140,
                    height: 140,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: LinearGradient(begin: Alignment.bottomLeft, end: Alignment.topRight, colors: [
                          this.firstColor != null ? this.firstColor! : Theme.of(context).focusColor.withOpacity(0.3),
                          this.secondaryColor != null ? this.secondaryColor! : Theme.of(context).focusColor.withOpacity(0.05),
                        ])),
                    child: Icon(
                      icon!,
                      color: this.iconColor != null ? this.iconColor : Theme.of(context).scaffoldBackgroundColor,
                      size: 65,
                    ),
                  ),
                  Positioned(
                    right: -30,
                    bottom: -50,
                    child: Container(
                      width: 90,
                      height: 90,
                      decoration: BoxDecoration(
                        color: Colors.grey[100]!.withOpacity(.2),
                        borderRadius: BorderRadius.circular(150),
                      ),
                    ),
                  ),
                  Positioned(
                    left: -20,
                    top: -50,
                    child: Container(
                      width: 110,
                      height: 110,
                      decoration: BoxDecoration(
                        color: Colors.grey[100]!.withOpacity(.2),
                        borderRadius: BorderRadius.circular(150),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 15),
             Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      this.title!,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 17, fontWeight: FontWeight.w700, color: this.textColor != null ? this.textColor : SECONDARY_DARK_COLOR),
                    ),
                    if (this.textButton != null)
                      TextButton(
                          style: ButtonStyle(
                            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(horizontal: 0)),
                          ),
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PagesBottomTabbar(
                                          currentTab: 3,
                                        )));
                          },
                          child: Text(
                            this.textButton!,
                            style: TextStyle(
                                fontSize: 17,
                            fontWeight: FontWeight.w700,
                            color: this.textButtonColor != null ? this.textButtonColor : SECONDARY_DARK_COLOR),
                          ))
                  ],
                ),
          ],
        ),
      ),
    );
  }
}
