import 'package:calciobalilla24/screens/Admin/admin_dashboard.dart';
import 'package:calciobalilla24/screens/Admin/edit_location.dart';
import 'package:calciobalilla24/screens/Admin/new_tournament.dart';
import 'package:calciobalilla24/screens/Admin/switch_admin_page.dart';
import 'package:calciobalilla24/screens/Admin/user_detail_admin.dart';
import 'package:calciobalilla24/screens/User/switch_page.dart';
import 'package:calciobalilla24/screens/User/tournaments_partecipations.dart';
import 'package:calciobalilla24/screens/User/user_detail.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:flutter/material.dart';
import 'package:calciobalilla24/screens/User/dashboard.dart';
import 'package:calciobalilla24/screens/User/map.dart';
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PagesBottomTabbar extends StatefulWidget {
  int? currentTab;
  Widget currentPage = Dashboard();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  PagesBottomTabbar({
    Key? key,
    this.currentTab,
  }) {
    currentTab = currentTab != null ? currentTab : 0;
  }

  @override
  _PagesBottomTabbarState createState() {
    return _PagesBottomTabbarState();
  }
}

class _PagesBottomTabbarState extends State<PagesBottomTabbar> {
  int _pageIndex = 0;
  String role = "";
  PageController? _pageController;

  initState() {
      setState(() {
      _pageIndex = this.widget.currentTab!;
    });
    _pageController = PageController(initialPage: _pageIndex, keepPage: true);
    getRole().then((value) {
      setState(() {
        role = value;
      });
    });
    super.initState();
  }

  Future<String> getRole() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? role = sharedPreferences.getString('role');
    if (role != null) {
      return role;
    } else
      return 'user';
  }

  @override
  void didUpdateWidget(PagesBottomTabbar oldWidget) {
    // _selectTab(oldWidget.currentTab!);
    super.didUpdateWidget(oldWidget);
  }

  void onTabTapped(int index) {
    this._pageController!.animateToPage(index, duration: const Duration(milliseconds: 200), curve: Curves.easeInOut);
  }

  void onPageChanged(int page) {
    setState(() {
      this._pageIndex = page;
    });
  }

  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  List<Widget> usersPages = [Dashboard(), MapPage(), TournamentsPartecipations(), UserDetail(), SwitchPage()];
  List<Widget> adminPages = [AdminDashboard(), NewTournament(), UserDetailAdmin(), SwitchAdminPage()];

  List<Widget> checkPages() {
    // print(role);
    return role == 'user' ? usersPages : adminPages;
  }

  @override
  void dispose() {
    _pageController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            key: widget.scaffoldKey,
            body: PageView(
              physics: (_pageIndex == 1 && role == 'user') ? NeverScrollableScrollPhysics() : null,
              children: checkPages(),
              onPageChanged: onPageChanged,
              controller: _pageController,
            ),
            extendBody: true,
            bottomNavigationBar: Container(
              height: BOTTOM_BAR_HEIGHT,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topRight: Radius.circular(35), topLeft: Radius.circular(35)),
                boxShadow: [
                  BoxShadow(color: Theme.of(context).hoverColor, spreadRadius: 3),
                ],
              ),
              child: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,
                selectedItemColor: Theme.of(context).accentColor,
                selectedFontSize: 0,
                unselectedFontSize: 0,
                iconSize: 22,
                elevation: 0,
                backgroundColor: Colors.transparent,
                selectedIconTheme: IconThemeData(size: 28),
                unselectedItemColor: Theme.of(context).focusColor.withOpacity(1),
                currentIndex: widget.currentTab!,
                onTap: onTabTapped,
                // this will be set when a new tab is tapped
                items: [
                  BottomNavigationBarItem(
                    icon: Icon(Icons.home_outlined, size: 30, color: _pageIndex == 0 ? Colors.red : Colors.grey),
                    title: new Container(height: 0.0),
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.add_circle_outline_rounded, size: 30, color: _pageIndex == 1 ? Colors.red : Colors.grey),
                    title: new Container(height: 0.0),
                  ),
                  if (role != "admin")
                    BottomNavigationBarItem(
                      icon: Icon(Icons.play_circle_outline_outlined, size: 30, color: _pageIndex == 2 ? Colors.red : Colors.grey),
                      title: new Container(height: 0.0),
                    ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.person_outline, size: 30, color: getColorFirst(role, _pageIndex)),
                    title: new Container(height: 0.0),
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.repeat, size: 30, color: getColorSecond(role, _pageIndex)),
                    title: new Container(height: 0.0),
                  ),
                ],
              ),
            )));
  }

  Color getColorFirst(String role, int currentTab) {
    if (role != 'admin') {
      if (currentTab == 3)
        return Colors.red;
      else
        return Colors.grey;
    } else {
      if (currentTab == 2)
        return Colors.red;
      else
        return Colors.grey;
    }
  }

  Color getColorSecond(String role, int currentTab) {
    if (role != 'admin') {
      if (currentTab == 4)
        return Colors.red;
      else
        return Colors.grey;
    } else {
      if (currentTab == 3)
        return Colors.red;
      else
        return Colors.grey;
    }
  }
}
