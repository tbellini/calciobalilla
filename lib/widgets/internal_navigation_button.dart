import 'dart:ffi';

import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class InternalNavigationButton extends StatefulWidget {
  final int? tabNumber;
  final int? tabSelected;
  final String? title;
  final Function()? onPressed;

  const InternalNavigationButton({Key? key, @required this.tabSelected, @required this.title, @required this.tabNumber, @required this.onPressed})
      : super(key: key);

  @override
  _InternalNavigationButtonState createState() => _InternalNavigationButtonState();
}

class _InternalNavigationButtonState extends State<InternalNavigationButton> {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: this.widget.onPressed,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: Text(
            this.widget.title!,
            //default text size = 14
            style: TextStyle(color: getNumber() ? Theme.of(context).hoverColor : Colors.black),
          ),
        ),
        style: ButtonStyle(
            elevation: MaterialStateProperty.all<double>(0.1),
            backgroundColor: MaterialStateProperty.all<Color>(getNumber() ? SECONDARY_DARK_COLOR : PRIMARY_LIGHT_COLOR),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(
              borderRadius: BorderRadius.zero,
            ))));
  }

  getNumber() {
    if (this.widget.tabNumber! == this.widget.tabSelected!) {
      return true;
    } else
      return false;
  }
}
