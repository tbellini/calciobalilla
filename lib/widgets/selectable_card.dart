import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmer_text.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class SelectableCard extends StatelessWidget {
  final List? list;
  final int? index;
  final double? width;
  final double? carouselHeight;
  final bool? isLocation;
  final Color? backgroundColor;
  final bool? isSelected;
  final String? imageUrl;
  SelectableCard(
      {@required this.list,
      @required this.index,
      @required this.carouselHeight,
      @required this.imageUrl,
      this.width,
      this.isLocation,
      this.backgroundColor,
      this.isSelected});

  @override
  Widget build(BuildContext context) {
    return Container(
        //default 200
        width: width != null ? width : 200,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          color: backgroundColor != null ? backgroundColor : Colors.white,
        ),
        child: Stack(
          alignment: AlignmentDirectional.topStart,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
                  child: Image.network(
                    this.imageUrl!,
                    height: (carouselHeight! / 2) + (carouselHeight! / 7),
                    width: width != null ? width : 200,
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.center, children: [
                    Text(
                      list![index!].title.length >= 16 ? list![index!].title.substring(0, 16) + "..." : list![index!].title,
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      this.isLocation! ? list![index!].address + ", " + list![index!].city : formatHour.format(list![index!].dateScheduled),
                      style: TextStyle(fontSize: 10, fontWeight: FontWeight.w300),
                    ),
                  ]),
                )
              ],
            ),
            if (isSelected != null && isSelected!)
              ClipPath(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    color: Colors.black54,
                  ),
                  child: Center(
                      child: Icon(
                    Icons.check,
                    color: Colors.white,
                    size: 70,
                  )),
                ),
              ),
          ],
        ));
  }
}
