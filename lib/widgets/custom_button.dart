import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final GestureTapCallback? onPressed;
  final IconData? icon;
  final String? text;
  final Color? color;
  final double? width;
  final double? vertical;
  final double? horizontal;

  CustomButton({@required this.onPressed, @required this.text, @required this.color, this.width, this.icon, this.vertical, this.horizontal});

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      fillColor: color,
      splashColor: color,
      child: Padding(
        // default vertical 15, horizontal 20
        padding: EdgeInsets.symmetric(vertical: vertical != null ? vertical!:15, horizontal: horizontal !=null? horizontal! : 20),
        child: icon != null
            ? Container(
                width: width ?? 150,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Icon(
                      icon,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: 40,
                    ),
                    Text(
                      text!,
                      maxLines: 1,
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ))
            : Container(
                width: width ?? 150,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      text!,
                      maxLines: 1,
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                  ],
                )),
      ),
      onPressed: onPressed,
      shape: const StadiumBorder(),
    );
  }
}
