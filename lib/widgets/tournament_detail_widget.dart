import 'package:calciobalilla24/models/Location.dart';
import 'package:calciobalilla24/models/Tournament.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmer_text.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class TournamentDetailWidget extends StatelessWidget {
  final Tournament? tournament;
  final Location? location;
  final Widget? lastWidget;

  const TournamentDetailWidget({Key? key, @required this.tournament, @required this.location, this.lastWidget}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: lastWidget != null ? MediaQuery.of(context).size.height : MediaQuery.of(context).size.height / 1.5,
      decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: Radius.circular(RADIUS_STANDARD_CONTAINER_BORDER),
            topRight: Radius.circular(RADIUS_STANDARD_CONTAINER_BORDER),
          )),
      child: Column(
        children: [
          ClipRRect(
            borderRadius: new BorderRadius.only(
              topLeft: Radius.circular(RADIUS_STANDARD_CONTAINER_BORDER),
              topRight: Radius.circular(RADIUS_STANDARD_CONTAINER_BORDER),
            ),
            child: Image(
              height: 220,
              width: MediaQuery.of(context).size.width,
              image: NetworkImage(
                location!.imageCover,
              ),
              fit: BoxFit.cover,
            ),
          ),
          Container(
            padding: EdgeInsets.all(generalPadding),
            child: tournament!.title == ""
                ? Shimmer.fromColors(
                    baseColor: Theme.of(context).dividerColor,
                    highlightColor: PRIMARY_LIGHT_COLOR,
                    enabled: true,
                    child: ShimmerText(
                      3,
                      mainAxis: MainAxisAlignment.center,
                      padding: EdgeInsets.only(right: 15),
                    ))
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            tournament!.title,
                            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            location!.title.toUpperCase() + " - " + location!.address + " " + location!.city + " ",
                            style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
                          ),
                          Divider(
                            thickness: 0.5,
                          ),
                          RichText(
                              text: TextSpan(
                                  text: 'Data inizio torneo: ',
                                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
                                  children: [
                                TextSpan(
                                    text: formatHour.format(tournament!.dateScheduled), style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300))
                              ])),
                          RichText(
                              text: TextSpan(
                                  text: 'Numero di tavoli: ',
                                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
                                  children: [
                                TextSpan(text: tournament!.foosballField.toString(), style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300))
                              ])),
                          RichText(
                              text: TextSpan(
                                  text: 'Numero di squadre: ',
                                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
                                  children: [
                                TextSpan(text: tournament!.teams.toString(), style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300))
                              ])),
                          if (this.tournament!.notes != "")
                            RichText(
                              text: TextSpan(
                                text: 'Note: ',
                                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
                                children: [
                                  TextSpan(
                                    text: this.tournament!.notes,
                                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
                                  ),
                                ],
                              ),
                            ),
                          if (this.tournament!.rules != "")
                            RichText(
                              text: TextSpan(
                                text: 'Regole: ',
                                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
                                children: [
                                  TextSpan(
                                    text: this.tournament!.notes,
                                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
                                  ),
                                ],
                              ),
                            )
                        ],
                      ),
                    ],
                  ),
          ),
          if (lastWidget != null)
            Expanded(
              child: lastWidget!,
            ),
        ],
      ),
    );
  }
}
