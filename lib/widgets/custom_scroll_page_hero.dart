import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:calciobalilla24/widgets/custom_appbar.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/pages.dart';
import 'package:flutter/material.dart';

class CustomScrollPageHero extends StatelessWidget {
  final Widget? custom_bar;
  final Widget? custom_body;
  final double? height;
  final String? imageUrl;
  final String? heroTag;
  final double? imageDimension;
  final bool? backArrow;
  final Widget? navigateWidget;
  final String? base64Url;
  final bool? canScroll;
  final bool? fillRemaining;

  CustomScrollPageHero({
    @required this.custom_bar,
    @required this.custom_body,
    @required this.height,
    @required this.imageUrl,
    this.base64Url,
    this.heroTag,
    this.imageDimension,
    this.backArrow,
    this.navigateWidget,
    this.canScroll,
    this.fillRemaining,
  });

  goBack(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => this.navigateWidget == null ? PagesBottomTabbar() : this.navigateWidget!),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: CustomScrollView(
            physics: this.canScroll != null || this.canScroll == false ? NeverScrollableScrollPhysics() : null,
            shrinkWrap: true,
            slivers: <Widget>[
              SliverAppBar(
                automaticallyImplyLeading: false,
                backgroundColor: Theme.of(context).backgroundColor,
                expandedHeight: height,
                elevation: 0,
                leading: this.backArrow == null || this.backArrow == false
                    ? null
                    : IconButton(
                        icon: Icon(Icons.arrow_back),
                        onPressed: () {
                          goBack(context);
                        },
                      ),
                flexibleSpace: FlexibleSpaceBar(
                  collapseMode: CollapseMode.parallax,
                  background: Stack(alignment: Alignment.topCenter, children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      //mantiene altezza immagine sempre inferiore al margine minimo
                      height: MediaQuery.of(context).size.height / (imageDimension == null ? 3.9 : imageDimension! - 1.1),
                      child: Hero(
                        //INSERIRE LO STESSO TAG NELLA PARTE DELLA RICERCA TORNEI (MAP)
                        //default "tournament"
                        tag: heroTag != null ? heroTag! : 'tImage',
                        child: getHeroWidget(base64Url, imageUrl),
                      ),
                    ),
                    CustomContainer(
                      child: custom_bar!,
                      color: Colors.white,
                      //media query --> DEFAULT 5
                      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height / (imageDimension == null ? 5 : imageDimension!)),
                    ),
                  ]),
                ),
              ),
              fillRemaining != null || fillRemaining == true
                  ? SliverFillRemaining(
                      child: custom_body!,
                    )
                  :
              SliverToBoxAdapter(
                child: custom_body!,
              ),
              
            ],
          ),
        ),
      ],
    );
  }

  Widget getHeroWidget(String? base64Url, String? imageUrl) {
    if (imageUrl! == "") {
      return Center(child: CircularProgressIndicator());
    } else if (base64Url != null) {
      if (base64Url != "") {
        return Image.memory(
          base64Decode(base64Url),
          fit: BoxFit.cover,
        );
      } else {
        return CachedNetworkImage(
            fit: BoxFit.cover,
            imageUrl: imageUrl,
            placeholder: (context, url) => Image.asset(
                  'assets/img/loading.gif',
                  fit: BoxFit.cover,
                ),
            errorWidget: (context, url, error) => Icon(
                  Icons.error,
                  size: 40,
                ));
      }
    } else {
      return CachedNetworkImage(
          fit: BoxFit.cover,
          imageUrl: imageUrl,
          placeholder: (context, url) => Image.asset(
                'assets/img/loading.gif',
                fit: BoxFit.cover,
              ),
          errorWidget: (context, url, error) => Icon(
                Icons.error,
                size: 40,
              ));
    }
  }
}
