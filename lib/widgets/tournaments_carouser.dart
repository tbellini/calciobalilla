import 'package:calciobalilla24/screens/User/tournament_detail.dart';
import 'package:calciobalilla24/widgets/tournament_card.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

typedef void IdTapCallback(int id);

class TouramentsCarousel extends StatelessWidget {
  final List? list;
  final double? height;
  final double? bottomPadding;
  final EdgeInsetsGeometry? carouselPadding;
  final bool? isLocation;
  final IdTapCallback? onTap;
  final Color? backgroundColor;

  TouramentsCarousel(
      {@required this.list,
      @required this.height,
      @required this.onTap,
      this.carouselPadding,
      this.bottomPadding,
      this.isLocation,
      this.backgroundColor});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(
          //default: 80
          bottom: bottomPadding == null ? 80 : bottomPadding!,
        ),
        child: Container(
          height: height,
          child: ListView.separated(
            // default EdgeInsets.only(left: 40, right: 20)
            padding: this.carouselPadding == null ? EdgeInsets.only(left: 40, right: 20) : carouselPadding,
            itemCount: list!.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                  onTap: () {
                    onTap!(list![index].id);
                  },
                  child: TournamentCard(
                    isLocation: this.isLocation,
                    carouselHeight: height,
                    list: list,
                    index: index,
                    imageUrl: list![index].location.imageCover,
                    backgroundColor: backgroundColor,
                  ));
            },
            separatorBuilder: (context, index) => SizedBox(
              width: 20,
            ),
          ),
        ));
  }
}
