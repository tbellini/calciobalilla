import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:flutter/material.dart';

class CustomListTile extends StatelessWidget {
  final int index;
  final String? textButton;
  final String? title;
  final String? subtitle;
  final String? urlImage;
  final double? radius;
  final Widget? trailingWidget;
  final void Function()? onPressedButton;
  final void Function()? onLongPressedButton;
  final void Function()? onPressedTile;
  final int? id;

  const CustomListTile(
    this.index, {
    Key? key,
    this.onPressedButton,
    this.onLongPressedButton,
    this.textButton,
    @required this.title,
    @required this.subtitle,
    this.onPressedTile,
    this.urlImage,
    this.radius,
    this.trailingWidget,
    this.id
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: ListTile(
        onTap: this.onPressedTile != null ? this.onPressedTile : null,
        contentPadding: TILE_CONTENT_PADDING,
        title: Text(
          this.title!,
          style: TextStyle(fontSize: 13.5),
        ),
        subtitle: Text(
          this.subtitle!,
          style: TextStyle(
            fontWeight: FontWeight.w300,
            fontSize: 12.5,
          ),
        ),
        onLongPress: onLongPressedButton != null ? onLongPressedButton : null,
        enableFeedback: true,
        leading: this.urlImage != null
            ? this.id != null
                ? Hero(
                    tag: 'tImage' + id.toString(),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(this.urlImage!),
                      backgroundColor: PRIMARY_LIGHT_COLOR,
                      radius: this.radius != null ? this.radius : null,
                    ))
                : CircleAvatar(
                    backgroundImage: NetworkImage(this.urlImage!),
                backgroundColor: PRIMARY_LIGHT_COLOR,
                radius: this.radius != null ? this.radius : null,
              )
            : null,
        trailing: this.textButton != null && this.onPressedButton != null
            ? ElevatedButton(
                child: Text(
                  this.textButton!,
                  style: TextStyle(fontSize: 10.5),
                ),
                onPressed: onPressedButton,
              )
            : this.trailingWidget != null
                ? this.trailingWidget
                : null,
      ),
    );
  }
}
