import 'package:calciobalilla24/widgets/rule_card.dart';
import 'package:flutter/material.dart';

class CustomRulesCarousel extends StatelessWidget {
  final List? list;
  final double? carouselHeight;
  final double? initialLeftPadding;
  final Function()? onTap;

  CustomRulesCarousel({@required this.list, @required this.carouselHeight, @required this.onTap, this.initialLeftPadding});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: carouselHeight,
      child: ListView.separated(
          padding: EdgeInsets.only(left: initialLeftPadding ?? 10, right: 10),
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: onTap,
              child: RuleCard(
                list: list,
                index: index,
                carouselHeight: carouselHeight,
              ),
            );
          },
          separatorBuilder: (BuildContext context, int index) => SizedBox(
                width: 10,
              ),
          itemCount: list!.length),
    );
  }
}
