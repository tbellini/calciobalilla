import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PhaseCard extends StatelessWidget {
  final double? width;
  final double? height;
  final Color? color;
  final String? title;
  final String? desc;

  PhaseCard({@required this.height, @required this.color, @required this.title, @required this.desc, this.width});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(20),
        //default 300
        // width: width != null ? width : 200,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          color: color,
        ),
        child: Center(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              title!,
              style: TextStyle(
                fontWeight: FontWeight.w600, 
                fontSize: 14, 
                color: Colors.white,
                ),
                textAlign: TextAlign.center,
            ),
            SizedBox(height: 2,),
            Text(
              desc!,
              style: TextStyle(
                color: Colors.white,
                fontSize: 10,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        )));
  }
}
