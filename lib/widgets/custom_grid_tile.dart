import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:flutter/material.dart';

class CustomGridTile extends StatelessWidget {
  final Widget? body;
  final String? title;
  final String? subtitle;
  final String? button_text;
  final GestureTapCallback? onPressed;
  final Color? color;
  final Color? textColor;

  CustomGridTile({@required this.body, @required this.title, @required this.button_text, @required this.onPressed, this.subtitle, this.color, this.textColor});
  @override
  Widget build(BuildContext context) {
    return
     Container(
        decoration: BoxDecoration(
        color: color != null ? color : SECONDARY_DARK_COLOR,
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: Padding(
        padding: EdgeInsets.all(30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
            children: [
            Text(
              title!,
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700, color: textColor!=null ? textColor:Theme.of(context).hoverColor),
            ),
            subtitle != null
                ? Text(
                    title!,
                    style: TextStyle(fontSize: 10, fontWeight: FontWeight.w300, color: textColor!=null ? textColor:Theme.of(context).hoverColor),
                  )
                : SizedBox(height: 0,),
            Divider(
              thickness: 0.5,
              color: textColor,
            ),
              SizedBox(height: 5,),
            body!,
              SizedBox(height: 15,),
          ]),
            CustomButton(onPressed: (){}, text: button_text, color: Theme.of(context).hintColor, horizontal: 40,)
          ],
        ),
      ),
    );
  }
}
