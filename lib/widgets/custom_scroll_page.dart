import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_appbar.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:flutter/material.dart';

class CustomScrollPage extends StatelessWidget {
  final Widget? custom_bar;
  final Widget? custom_body;
  final double? height;
  final double? margin;
  final String? title_bar;
  final Color? color_title_bar;
  final Color? color_bar;
  final bool? backArrow;
  final IconData? icon_right_bar;
  final GestureTapCallback? iconRightTap;
  final IconData? icon_left_bar;
  final GestureTapCallback? iconLeftTap;


  CustomScrollPage(
      {@required this.custom_bar,
      @required this.custom_body,
      @required this.height,
      this.margin,
      this.title_bar,
      this.color_title_bar,
      this.color_bar,
      this.backArrow,
      this.icon_right_bar,
      this.iconRightTap,
      this.icon_left_bar,
      this.iconLeftTap,});

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          color: Colors.transparent,
          child: CustomScrollView(
            primary: true,
            shrinkWrap: false,
            slivers: <Widget>[
              SliverAppBar(
                // default false
                automaticallyImplyLeading: backArrow != null ? backArrow! : false,
                leading: IconButton(
                    icon: Icon(
                      icon_left_bar,
                      color: color_title_bar,
                    ),
                    onPressed: iconLeftTap),
                backgroundColor: Theme.of(context).backgroundColor,
                expandedHeight: height,
                title: title_bar != null
                    ? Text(
                        title_bar!.toUpperCase(),
                        style: TextStyle(color: color_title_bar, fontSize: 20, fontWeight: FontWeight.bold),
                      )
                    : Container(),
                centerTitle: true,
                actions: [
                  if (icon_right_bar != null)
                    Padding(
                      padding: EdgeInsets.only(right: 4),
                      child: IconButton(
                        icon: Icon(icon_right_bar),
                        onPressed: iconRightTap,
                      ),
                    )
                ],
                elevation: 0,
                // iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
                flexibleSpace: FlexibleSpaceBar(
                  collapseMode: CollapseMode.parallax,
                  background: AnimatedContainer(
                      duration: const Duration(seconds: 1),
                      margin: EdgeInsets.all(margin == null ? 0 : margin!),
                      height: height,
                      child: title_bar != null
                          ? Column(
                              children: [
                                Container(
                                  height: APP_BAR_HEIGHT,
                                  color: this.color_bar!,
                                ),
                                Expanded(
                                  child: Container(
                                    //VEDERE SE TOGLIERE EXPANDED SE NON FUNZIONANO I COLUMN...
                                    decoration: BoxDecoration(
                                        color: color_bar,
                                        border:
                                            Border(top: BorderSide(width: 0, color: this.color_bar != null ? this.color_bar! : Colors.transparent))),
                                    child: CustomContainer(child: custom_bar!, color: Colors.white),
                                  ),
                                ),
                              ],
                            )
                          : custom_bar!),
                ),
              ),
              
              SliverToBoxAdapter(
                child: custom_body,
              ),
              
            ],
          ),
        ),
      ],
    );
  }
}
