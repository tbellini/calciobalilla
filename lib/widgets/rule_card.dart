import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class RuleCard extends StatelessWidget {
  final List? list;
  final int? index;
  final double? width;
  final double? carouselHeight;

  RuleCard({@required this.list, @required this.index, @required this.carouselHeight, this.width});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
        //default 300
        width: width != null ? width : 200,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(25)),
          color: index!.isEven ? Colors.blueAccent[200] : Colors.blue.shade400,
        ),
           child: Center(
          child: Row(
            children: [
              Icon(
                FontAwesomeIcons.fileAlt,
                color: Colors.white,
              ),
              SizedBox(width: 10,),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    list![index!]["title"],
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 15,
                      color: Colors.white
                    ),

                  ),
                  Text(
                    list![index!]["desc"],
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 11,
                    ),
                  ),
                ],
              )
            ],
        ),
        ),
    );
    
  }
}
