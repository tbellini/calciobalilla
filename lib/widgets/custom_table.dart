import 'package:flutter/material.dart';

class CustomTable extends StatelessWidget {
  final GestureTapCallback? onPressed;
  final IconData? icon;
  final String? text;
  final Color? color;
  final List<Map>? tournamentsList;

  CustomTable(
      {@required this.onPressed,
      @required this.text,
      @required this.color,
      @required this.tournamentsList,
      this.icon});
      
  Widget itemDetail(itemDetail) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        children: [
          Column(
            children: [Text(itemDetail['name'])],
          ),
          SizedBox(
            width: 50,
          ),
          Column(
            children: [Text(itemDetail['due_date'])],
          ),
        ],
      ),
    );
  }

  Widget itemList(itemList) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: Card(
        color: Colors.grey[800],
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Container(
                child: Text('TITOLO'),
              ),
              Row(
                  children: itemList.map((p) {
                return itemDetail(p);
              }).toList())
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(children: [itemList(tournamentsList!)]));
  }
}
