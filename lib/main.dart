import 'dart:io';

import 'package:calciobalilla24/RouteGenerator.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // TOLTO PER IL WEB
  await Permission.location.request();
  await Permission.locationWhenInUse.request();
  final bool isLoggedPar = await isLogged();
  await Firebase.initializeApp();
  //ERROR: certificate
  // HttpOverrides.global = new MyHttpOverrides();
  runApp(MyApp(
    initalRoute: isLoggedPar ? 'pages' : 'login',
  ));
}

class MyApp extends StatefulWidget {
  final String? initalRoute;
  const MyApp({Key? key, this.initalRoute}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late final FirebaseMessaging _fcm = FirebaseMessaging.instance;

  @override
  void initState() {
    registerNotification(_fcm);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      localizationsDelegates: [GlobalMaterialLocalizations.delegate],
      supportedLocales: [const Locale('it', 'IT'), const Locale('en', 'EN')],
      title: 'Calciobalilla24',
      theme: ThemeData(
        fontFamily: 'Roboto',
        primarySwatch: Colors.blue,
        hintColor: Color(0xFF1AA7F1),
        accentColor: Color(0xFF585858),
        primaryColorLight: Colors.grey[100],
        dividerColor: Colors.grey[300],
        secondaryHeaderColor: Colors.grey[400],
        hoverColor: Colors.white,
        backgroundColor: Colors.white,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      onGenerateRoute: RouteGenerator.generateRoute,
      initialRoute: this.widget.initalRoute,
    );
  }
}


// class MyApp extends StatelessWidget {
//   final String? initalRoute;
//   MyApp({this.initalRoute});
//   @override
//   Widget build(BuildContext context) {
//     SystemChrome.setPreferredOrientations([
//       DeviceOrientation.portraitUp,
//       DeviceOrientation.portraitDown,
//     ]);
//     return MaterialApp(
//       localizationsDelegates: [GlobalMaterialLocalizations.delegate],
//       supportedLocales: [const Locale('it', 'IT'), const Locale('en', 'EN')],
//       title: 'Calciobalilla24',
//       theme: ThemeData(
//         fontFamily: 'Roboto',
//         primarySwatch: Colors.blue,
//         hintColor: Color(0xFF1AA7F1),
//         accentColor: Color(0xFF585858),
//         cardColor: Color(0xFF7E7E7E),
//         buttonColor: Colors.grey[100],
//         dividerColor: Colors.grey[300],
//         hoverColor: Colors.white,
//         backgroundColor: Colors.white,
//         visualDensity: VisualDensity.adaptivePlatformDensity,
//       ),
//       onGenerateRoute: RouteGenerator.generateRoute,
//       initialRoute: this.initalRoute,
//     );
//   }
// }

  //ERROR: certificate
//  class MyHttpOverrides extends HttpOverrides{
//   @override
//   HttpClient createHttpClient(SecurityContext? context){
//     return super.createHttpClient(context)
//       ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
//   }
// }
