import 'package:calciobalilla24/models/Tournament.dart';
import 'package:calciobalilla24/screens/Tournament/admin/dashboard_sub_pages/dashboard_current.dart';
import 'package:calciobalilla24/screens/Tournament/admin/dashboard_sub_pages/dashboard_ended.dart';
import 'package:calciobalilla24/screens/Tournament/admin/dashboard_sub_pages/dashboard_pre_start.dart';
import 'package:flutter/material.dart';

class DashboardPaginator extends StatefulWidget {
  final Tournament? tournament;
  const DashboardPaginator({Key? key, @required this.tournament}) : super(key: key);

  @override
  _DashboardPaginatorState createState() => _DashboardPaginatorState();
}

class _DashboardPaginatorState extends State<DashboardPaginator> {
  Widget getDashboardPage(Tournament tournament) {
    if (tournament.dateStart != null && tournament.dateEnd == null)
      return DashboardCurrent(tournament: tournament);
    else if (tournament.dateEnd != null)
      return DashboardEnded();
    else
      return DashboardPreStart(tournament: tournament);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: getDashboardPage(this.widget.tournament!),
    );
  }
}
