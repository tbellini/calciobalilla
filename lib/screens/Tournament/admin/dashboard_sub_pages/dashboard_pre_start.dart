import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/TeamsPartecipant.dart';
import 'package:calciobalilla24/models/Tournament.dart';
import 'package:calciobalilla24/repositories/notification_repo.dart';
import 'package:calciobalilla24/screens/Tournament/admin/dashboard_paginator.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/TeamsListWidget.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page_hero.dart';
import 'package:calciobalilla24/widgets/default_error_dialog.dart';
import 'package:calciobalilla24/widgets/empty_list_widget.dart';
import 'package:calciobalilla24/widgets/pages.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class DashboardPreStart extends StatefulWidget {
  final Tournament? tournament;
  const DashboardPreStart({Key? key, @required this.tournament}) : super(key: key);

  @override
  _DashboardPreStartState createState() => _DashboardPreStartState();
}

class _DashboardPreStartState extends StateMVC<DashboardPreStart> {
  TournamentsController? _con;
  List<TeamsPartecipant> partecipantsList = [];

  _DashboardPreStartState() : super(TournamentsController()) {
    _con = controller as TournamentsController;
  }

  @override
  void initState() {
    _con!.getPartecipantTeams(this.widget.tournament!.id!).then((value) {
      setState(() {
        partecipantsList = value;
      });
    });
    super.initState();
  }

  getAbsent(List<TeamsPartecipant> partecipantsList) {
    List<int> absentList = [];
    for (TeamsPartecipant partecipant in partecipantsList) {
      if (partecipant.isChecked) {
        absentList.add(partecipant.id!);
      }
    }
    return absentList;
  }

  //list<int> id delle iscrizioni per gli assenti --> id iscrizioni = id partecipante

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: CustomScrollPageHero(
          heroTag: "tImage" + this.widget.tournament!.id!.toString(),
          backArrow: true,
          navigateWidget: PagesBottomTabbar(),
          imageUrl: this.widget.tournament!.location.imageCover,
          custom_bar: CustomContainer(
            child: Container(
              padding: EdgeInsets.only(top: 15, bottom: 5),
              child: Stack(
                alignment: Alignment.topCenter,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 55),
                    child: _con!.isLoading
                        ? Image(
                            image: AssetImage('assets/img/custom_loading.gif'),
                            width: 200,
                            height: 200,
                          )
                        : InkWell(
                            child: Image(
                              image: AssetImage('assets/img/start.gif'),
                              width: 200,
                              height: 200,
                            ),
                            onTap: () {
                              List<int> absentListID = getAbsent(partecipantsList);
                              showDialog(
                                context: context,
                                builder: (_) => AlertDialog(
                                  title: Text("Iniziare il torneo? "),
                                  content: RichText(
                                    text: TextSpan(
                                      text: "Hai selezionato: ",
                                      style: TextStyle(fontSize: 15, color: Colors.black),
                                      children: [
                                        TextSpan(
                                          text: absentListID.length == 1
                                              ? absentListID.length.toString() + " assente.\n"
                                              : absentListID.length.toString() + " assenti.\n",
                                          style: TextStyle(fontWeight: FontWeight.w700),
                                        ),
                                        TextSpan(text: "Iniziando il torneo, "),
                                        TextSpan(
                                          text: "non sarà più possibile aggiungerli.",
                                          style: TextStyle(fontWeight: FontWeight.w700),
                                        ),
                                      ],
                                    ),
                                  ),
                                  actions: [
                                    TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text("No"),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                        _con!.sendAbsentTeams(absentListID, this.widget.tournament!.id, partecipantsList).then((value) {
                                          if (value) {
                                            sendNotification(
                                              "Torneo iniziato",
                                              "Il torneo: " +
                                                  this.widget.tournament!.title +
                                                  " è iniziato alle ore: " +
                                                  DateFormat.Hm().format(DateTime.now()),
                                              getPartecipantsUsersID(partecipantsList),
                                            );

                                            //REINDIRIZZAMENTO VERSO PAGINA DASHBOARD TORNEO INIZIATO
                                            _con!.getTournamentDetail(this.widget.tournament!.id!).then((value) {
                                              setState(() {
                                                _con!.isLoading = false;
                                              });
                                              goToWidget(
                                                context,
                                                DashboardPaginator(tournament: this.widget.tournament),
                                              );
                                            });
                                          } else
                                            showDialog(
                                              context: context,
                                              builder: (_) => DefaultErrorDialog(
                                                subtitle:
                                                    "Verificare che la connessione sia stabile e che tutte le procedure preliminari siano state eseguite correttamente.",
                                              ),
                                            );
                                        });
                                      },
                                      child: Text("Si"),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                  ),
                  Column(
                    children: [
                      Text(
                        this.widget.tournament!.title.toString(),
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        formatHour.format(this.widget.tournament!.dateScheduled) +
                            " - " +
                            this.widget.tournament!.foosballField.toString() +
                            " tavoli".toUpperCase(),
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            color: Theme.of(context).backgroundColor,
          ),
          custom_body: CustomContainer(
            backgroundColor: Theme.of(context).backgroundColor,
            color: PRIMARY_LIGHT_COLOR,
            child: Padding(
              padding: EdgeInsets.only(left: 25, right: 25, top: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    RichText(
                        text: TextSpan(
                            text: "Seleziona ",
                            style: TextStyle(fontSize: 25, fontWeight: FontWeight.w300, color: SECONDARY_DARK_COLOR),
                            children: [
                          TextSpan(
                            text: "assenti",
                            style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700, color: SECONDARY_DARK_COLOR),
                          )
                        ])),
                    Text(
                      "Elenco delle squadre iscritte a questo torneo.",
                      style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.w400,
                        color: SECONDARY_DARK_COLOR,
                      ),
                    )
                  ]),
                  Divider(
                    thickness: 0.5,
                    color: SECONDARY_DARK_COLOR,
                  ),
                  Container(
                    height: partecipantsList.length >= PHISICS_STANDARD_LIST_LIMIT ? null : MediaQuery.of(context).size.height / 2,
                    child: partecipantsList.isEmpty
                        ? EmptyListWidget(
                            containerHeight: MediaQuery.of(context).size.height / 2,
                            icon: Icons.data_usage_sharp,
                            title: "Ancora nessun partecipante.",
                          )
                        :
                     ListView.separated(
                            shrinkWrap: true,
                      physics: partecipantsList.length >= PHISICS_STANDARD_LIST_LIMIT ? NeverScrollableScrollPhysics() : null,
                            padding: EdgeInsets.only(top: 20, bottom: 20),
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                decoration: BoxDecoration(
                                  color: Theme.of(context).backgroundColor,
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20),
                                  ),
                                ),
                                child: CheckboxListTile(
                                  contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                  title: Text(partecipantsList[index].playerOneName),
                                  subtitle: Text(partecipantsList[index].playerTwoName),
                                  value: partecipantsList[index].isChecked,
                                  onChanged: (value) {
                                    setState(() {
                                      partecipantsList[index].isChecked = value!;
                                    });
                                  },
                                ),
                              );
                            },
                            separatorBuilder: (BuildContext context, int index) => SizedBox(
                              height: 20,
                            ),
                            itemCount: partecipantsList.length,
                    ),
                  ),
                ],
              ),
            ),
          ),
          height: 380,
        ),
      ),
      onWillPop: () => goToWidget(context, PagesBottomTabbar()),
    );
  }
}
