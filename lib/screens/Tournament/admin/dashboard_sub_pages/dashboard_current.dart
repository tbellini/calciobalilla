import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/Tournament.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_grid_tile.dart';
import 'package:calciobalilla24/widgets/custom_list_tile.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:calciobalilla24/widgets/pages.dart';
import 'package:calciobalilla24/widgets/phase_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class DashboardCurrent extends StatefulWidget {
  final Tournament? tournament;
  DashboardCurrent({@required this.tournament});

  @override
  _DashboardCurrentState createState() => _DashboardCurrentState();
}

class _DashboardCurrentState extends StateMVC<DashboardCurrent> {
  TournamentsController? _con;
  _DashboardCurrentState() : super(TournamentsController()) {
    _con = controller as TournamentsController;
  }

  List listPart = [
    {'title': 'Torneo', 'subtitle': "bar nuraghi", 's1': 'Squadra1', 's2': 'Squadra2'},
    {'title': 'Torneo', 'subtitle': "bar nuraghi", 's1': 'Squadra1', 's2': 'Squadra2'},
    {'title': 'Torneo', 'subtitle': "bar nuraghi", 's1': 'Squadra1', 's2': 'Squadra2'},
    {'title': 'Torneo', 'subtitle': "bar nuraghi", 's1': 'Squadra1', 's2': 'Squadra2'},
    {'title': 'Torneo', 'subtitle': "bar nuraghi", 's1': 'Squadra1', 's2': 'Squadra2'},
    {'title': 'Torneo', 'subtitle': "bar nuraghi", 's1': 'Squadra1', 's2': 'Squadra2'},
    {'title': 'Torneo', 'subtitle': "bar nuraghi", 's1': 'Squadra1', 's2': 'Squadra2'},
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: CustomScrollPage(
          icon_right_bar: Icons.home,
          iconRightTap: () {
            Navigator.of(context).pushReplacementNamed('pages');
          },
          custom_bar: Container(
            padding: EdgeInsets.symmetric(vertical: 25, horizontal: 40),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  this.widget.tournament!.title,
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  children: [
                    Expanded(
                        child: InkWell(
                      child: PhaseCard(
                        title: "Fase a gironi",
                        desc: "Verifica partite e classifica parziale",
                        color: Colors.blue,
                        height: 190,
                      ),
                      onTap: () {
                        Navigator.of(context).pushReplacementNamed('group-stage-detail');
                      },
                    )),
                    SizedBox(
                      width: 15,
                    ),
                    Expanded(
                      child: InkWell(
                        child: PhaseCard(
                          title: "Fase eliminatoria",
                          desc: "Verifica partite e classifica parziale",
                          color: Colors.blueAccent.shade400,
                          height: 160,
                        ),
                        onTap: () {
                          Future.delayed(Duration.zero, () {
                            Navigator.of(context).pushReplacementNamed('eliminatory-detail');
                          });
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          height: 270,
          custom_body: CustomContainer(
            backgroundColor: Theme.of(context).backgroundColor,
            color: PRIMARY_LIGHT_COLOR,
            child: Padding(
              padding: EdgeInsets.only(left: 25, right: 25, top: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    RichText(
                        text: TextSpan(
                            text: "Elenco ",
                            style: TextStyle(fontSize: 25, fontWeight: FontWeight.w300, color: SECONDARY_DARK_COLOR),
                            children: [
                          TextSpan(
                            text: "contestazioni",
                            style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700, color: SECONDARY_DARK_COLOR),
                          )
                        ])),
                    Text(
                      "Elenco delle contestazioni presenti",
                      style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.w400,
                        color: SECONDARY_DARK_COLOR,
                      ),
                    )
                  ]),
                  Divider(
                    thickness: 0.5,
                    color: SECONDARY_DARK_COLOR,
                  ),
                  Container(
                    height: listPart.length >= PHISICS_STANDARD_LIST_LIMIT ? null : MediaQuery.of(context).size.height / 2,
                    child: ListView.separated(
                      shrinkWrap: true,
                      physics: listPart.length >= PHISICS_STANDARD_LIST_LIMIT ? NeverScrollableScrollPhysics() : null,
                      padding: EdgeInsets.only(top: 20, bottom: 20),
                      itemBuilder: (BuildContext context, int index) {
                        return CustomListTile(
                          index,
                          title: "nome contestazione",
                          subtitle: listPart[index]["s1"] + ' - ' + listPart[index]["s2"],
                          textButton: "Conferma",
                          onPressedButton: () {},
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) => SizedBox(
                        height: 20,
                      ),
                      itemCount: listPart.length,
                    ),
                  ),
                ],
              ),
            ),
          ),
          title_bar: "tornei",
          color_title_bar: Theme.of(context).hoverColor,
          color_bar: SECONDARY_DARK_COLOR,
        ),
      ),
      onWillPop: () => goToWidget(context, PagesBottomTabbar()),
    );
  }

  goBack() {
    Navigator.of(context).deactivate();
  }
}
