import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/Tournament.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_list_item.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class TournamentDashboard extends StatefulWidget {
  final int? id;
  TournamentDashboard({this.id});
  @override
  _TournamentDashboardState createState() => _TournamentDashboardState();
}

class _TournamentDashboardState extends StateMVC<TournamentDashboard> {
  TournamentsController? _con;
  Tournament tournament = new Tournament();
  final List groupList = [
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"}
  ];
  final List eliminatoryList = [
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"}
  ];

  _TournamentDashboardState() : super(TournamentsController()) {
    _con = controller as TournamentsController;
  }

  @override
  void initState() {
    _con!.getTournamentDetail(this.widget.id!).then((value) => {
          setState(() {
            tournament = value;
          })
        });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
          backgroundColor: Theme.of(context).accentColor,
          body: CustomScrollPage(
            custom_bar: CustomContainer(
                color: Theme.of(context).backgroundColor,
                child: Container(
                    padding: EdgeInsets.all(generalPadding),
                    child: Column(
                      children: [
                        Text(
                          "Partita corrente",
                          style: TextStyle(fontWeight: FontWeight.w700, fontSize: 35),
                          textAlign: TextAlign.start,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Squadra 1", style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700)),
                            Text("vs",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w300,
                                )),
                            Text("Squadra 2", style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700)),
                          ],
                        ),
                        SizedBox(height: 10),
                        Divider(
                          thickness: 0.5,
                          color: Colors.black38,
                        ),
                        SizedBox(height: 15),
                        Text(
                          "Tavolo 3",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                        SizedBox(height: 15),
                        CustomButton(
                            onPressed: () {
                              Navigator.of(context).pushReplacementNamed('scoreboard');
                            },
                            text: "Segnapunti",
                            width: 300,
                            color: Theme.of(context).hintColor),
                      ],
                    ))),
            custom_body: CustomContainer(
              backgroundColor: Theme.of(context).backgroundColor,
              color: Theme.of(context).accentColor,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: generalPadding + 10, horizontal: generalPadding - 10),
                child: Column(
                  children: [
                    if (groupList.isNotEmpty)
                      CustomListItem(
                          selector: 0,
                          title: "Girone attuale",
                          list: groupList,
                          ),
                    SizedBox(
                      height: 20,
                    ),
                    if (eliminatoryList.isNotEmpty)
                      CustomListItem(
                          selector: 0,
                          title: "Fase eliminatoria",
                          list: eliminatoryList,
                          // onPressed: () {
                        //   SchedulerBinding.instance!.addPostFrameCallback((_) {
                        //     Navigator.of(context).pushReplacementNamed('eliminatory-detail');
                        //   });
                        // }
                      ),
                  ],
                ),
              ),
            ),
            height: 350,
            title_bar: "Tornei",
            color_title_bar: Theme.of(context).hoverColor,
            color_bar: Theme.of(context).hintColor,
            icon_right_bar: Icons.home,
            iconRightTap: () {
              Navigator.of(context).pushReplacementNamed('pages');
            },
          )),
      onWillPop: () => goBack(),
    );
  }

  goBack() {
    Navigator.of(context).deactivate();
  }
}
