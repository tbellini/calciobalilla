import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_list_item.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class GroupStageRanking extends StatefulWidget {
  @override
  _GroupStageRankingState createState() => _GroupStageRankingState();
}

class _GroupStageRankingState extends State<GroupStageRanking> {

  final String assetPath = "assets/img/cup.svg";

  List tournamentsProgramma = [
    {"s": "Squadra1", },
    {"s": "Squadra2", },
    {"s": "Squadra3", },
    {"s": "Squadra4", }
  ];

  List stageGroup = [
    {"title": "girone1"},
    {"title": "girone2"},
    {"title": "girone3"},
    {"title": "girone4"},
    {"title": "girone5"},
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(child:  
    Scaffold(
      body: CustomScrollPage(
        title_bar: "Classifica gironi",
        icon_left_bar: Icons.arrow_back,
        iconLeftTap: () {
          Navigator.of(context).pushReplacementNamed('group-stage-detail');
        },
        color_bar: SECONDARY_DARK_COLOR,
        color_title_bar: Theme.of(context).hoverColor,
        height: 350,
        custom_bar: Container(
          padding: EdgeInsets.all(generalPadding),
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Classifica",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800),
                  ),
                  Text(
                    "fase a gironi",
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.w300),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  RichText(
                    text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      children: [
                      TextSpan(
                        text: "Gironi totali: ",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextSpan(
                        text: "5",
                        style: TextStyle(fontWeight: FontWeight.w400),
                      ),
                    ]),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  RichText(
                    
                    text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      children: [
                      TextSpan(
                        text: "Regole partite: ",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextSpan(
                        text: "ISO",
                        style: TextStyle(fontWeight: FontWeight.w400),
                      ),
                    ]),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  RichText(
                    text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      children: [
                      TextSpan(
                        text: "Numero partite: ",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextSpan(
                        text: "25",
                        style: TextStyle(fontWeight: FontWeight.w400),
                      ),
                    ]),
                  ),
                  
                ],
              ),
              SizedBox(width: 30,),
              Expanded(
                child: SvgPicture.asset(
                  assetPath,
                  semanticsLabel: 'label_1',
                ),
              )
            ],
          ),
        ),
        custom_body: CustomContainer(
            child: Container(
              margin: EdgeInsets.only(bottom: generalPadding, left: generalPadding - 10, right: generalPadding - 10, top: generalPadding- 10),
              child: ListView.separated(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    
                    return CustomListItem(
                        selector: 1,
                        title: stageGroup[index]["title"],
                        list: tournamentsProgramma,
                        );
                  },
                  separatorBuilder: (BuildContext context, int index) => SizedBox(
                        height: 20,
                      ),
                  itemCount: stageGroup.length),
            ),
            color: Theme.of(context).accentColor),
      ),
    ),
    onWillPop: () => goBack(),
    );
  }
  goBack(){
    Navigator.of(context).pushReplacementNamed('group-stage-detail');
  }
}
