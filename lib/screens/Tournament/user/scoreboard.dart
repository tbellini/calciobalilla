import 'dart:async';

import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class Scoreboard extends StatefulWidget {
  @override
  _ScoreboardState createState() => _ScoreboardState();
}

class _ScoreboardState extends State<Scoreboard> {
  Stopwatch _stopwatch = Stopwatch();
  Timer _timer = new Timer.periodic(Duration(milliseconds: 1), (timer) {});

  String rule1 =
      "La partita inizierà con il lancio della monetina. La scelta del campo spetterà alla squadra che vincerà il sorteggio e potrà partire con il servizio difensivo.";
  String rule2 =
      "Tenendo ferma la pallina, si può dichiarare il VIA e dopo aver sentito il VAI dell'avversario, la pallina dovrà raggiungere il bordo di fondo del campo e calciare o continuare a palleggiare sul bordo o tra i calciatori delle due stecche per 10 secondi massimo compreso il tempo di tiro.";
  String rule3 = "Qualsiasi violazione potrà essere penalizzata fermando il gioco e passando la pallina all'avversario.";

  @override
  void initState() {
    _timer = new Timer.periodic(Duration(milliseconds: 50), (timer) {
      setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void handleStartStop() {
    if (_stopwatch.isRunning) {
      _stopwatch.stop();
    } else {
      _stopwatch.start();
    }
    setState(() {}); // re-render the page
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomScrollPage(
      
      icon_left_bar: Icons.arrow_back,
      iconLeftTap: () {
        Navigator.of(context).pushReplacementNamed('tournament-dashboard');
      },
      title_bar: "Segnapunti",
      color_title_bar: Theme.of(context).hoverColor,
      color_bar: SECONDARY_DARK_COLOR,
      height: 480,
      custom_bar: CustomContainer(
        color: Theme.of(context).backgroundColor,
        child: Container(
          padding: EdgeInsets.all(generalPadding),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    children: [
                      Text(
                        "Squadra1",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      CircularPercentIndicator(
                        radius: 120,
                        lineWidth: 12,
                        percent: 0.6,
                        center: new Text(
                          "6",
                          style: TextStyle(fontSize: 50, fontWeight: FontWeight.w800, color: Colors.blue),
                        ),
                        progressColor: Colors.blue,
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Text(
                        "Squadra2",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      CircularPercentIndicator(
                        radius: 120,
                        lineWidth: 12,
                        percent: 0.4,
                        center: new Text(
                          "4",
                          style: TextStyle(fontSize: 50, fontWeight: FontWeight.w800, color: Colors.red),
                        ),
                        progressColor: Colors.red,
                      )
                    ],
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Divider(
                thickness: 0.3,
                color: Colors.grey,
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "Tempo partita".toUpperCase(),
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 20),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                formatTime(_stopwatch.elapsedMilliseconds),
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w300),
              ),
              SizedBox(
                height: 10,
              ),
              CustomButton(
                  onPressed: () {
                    handleStartStop();
                  },
                  text: _stopwatch.isRunning ? "stop" : "start",
                  color: Theme.of(context).hintColor)
            ],
          ),
        ),
      ),
      custom_body: CustomContainer(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: generalPadding + 10, horizontal: generalPadding + 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Regolamento",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Theme.of(context).hoverColor),
              ),
              SizedBox(
                height: 25,
              ),
              Text(
                rule1 + "\n" + rule2 + '\n' + rule3,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Theme.of(context).hoverColor, height: 1.2),
              ),
              SizedBox(
                height: 25,
              ),
              Align(
                child: CustomButton(onPressed: () {}, text: "Conferma", color: Theme.of(context).hintColor),
              )
            ],
          ),
        ),
        color: Theme.of(context).accentColor,
        // backgroundColor: Theme.of(context).backgroundColor,
      ),
    ));
  }
}
