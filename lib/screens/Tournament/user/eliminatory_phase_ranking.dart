import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_list_item.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:flutter/material.dart';

class EliminatoryPhaseRanking extends StatefulWidget {
  @override
  _EliminatoryPhaseRankingState createState() => _EliminatoryPhaseRankingState();
}

class _EliminatoryPhaseRankingState extends State<EliminatoryPhaseRanking> {

  List tournamentsProgramma = [
    {"s": "Squadra1", },
    {"s": "Squadra2", },
    {"s": "Squadra3", },
    {"s": "Squadra4", }
  ];


  @override
  Widget build(BuildContext context) {
    return WillPopScope(child:  
    Scaffold(
      backgroundColor: Theme.of(context).accentColor,
      body: CustomScrollPage(
        title_bar: "Classifica eliminatoria",
        // backArrow: true,
        icon_left_bar: Icons.arrow_back,
        iconLeftTap: () {
          Navigator.of(context).pushReplacementNamed('eliminatory-detail');
        },
        color_bar: SECONDARY_DARK_COLOR,
        color_title_bar: Theme.of(context).hoverColor,
        height: 320,
        custom_bar: Container(
          padding: EdgeInsets.all(generalPadding),
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Classifica",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800),
                  ),
                  Text(
                    "fase eliminatoria",
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.w300),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  RichText(
                    text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      children: [
                      TextSpan(
                        text: "Regole partite: ",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextSpan(
                        text: "ISO",
                        style: TextStyle(fontWeight: FontWeight.w400),
                      ),
                    ]),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  RichText(
                    text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      children: [
                      TextSpan(
                        text: "Numero partite: ",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextSpan(
                        text: "25",
                        style: TextStyle(fontWeight: FontWeight.w400),
                      ),
                    ]),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ],
          ),
        ),
        custom_body: CustomContainer(
          backgroundColor: Theme.of(context).backgroundColor,
            child: Container(
              margin: EdgeInsets.only(bottom: generalPadding * 2, left: generalPadding - 10, right: generalPadding - 10, top: generalPadding + 15),
              child:  CustomListItem(
                        selector: 1,
                        title: "Fase eliminatoria",
                        list: tournamentsProgramma,
                        ),
            ),
            color: Theme.of(context).accentColor
            ),
      ),
    ),
    onWillPop: () => goBack(),
    );
  }
  goBack(){
    Navigator.of(context).pushReplacementNamed('eliminatory-detail');
  }
}