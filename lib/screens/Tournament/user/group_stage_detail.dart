import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_list_item.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GroupSatgeDetail extends StatefulWidget {
  @override
  _GroupSatgeDetailState createState() => _GroupSatgeDetailState();
}

class _GroupSatgeDetailState extends State<GroupSatgeDetail> {
  final String assetPath = "assets/img/cup.svg";
  String role = "";

  getRole() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      role = sharedPreferences.getString('role')!;
    });
  }

  @override
  void initState() {
    getRole();
    super.initState();
  }

  List tournamentsProgramma = [
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"},
    {"s1": "Squadra1", "points": "2/1", "s2": "Squadra2"}
  ];

  List stageGroup = [
    {"title": "girone1"},
    {"title": "girone2"},
    {"title": "girone3"},
    {"title": "girone4"},
    {"title": "girone5"},
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: CustomScrollPage(
          title_bar: "tornei",
          icon_right_bar: Icons.home_filled,
          iconRightTap: () {
            if (role == 'admin') {
              Navigator.of(context).pushReplacementNamed('dashboard-current');
            } else
              Navigator.of(context).pushReplacementNamed('tournament-dashboard');
          },
          color_bar: SECONDARY_DARK_COLOR,
          color_title_bar: Theme.of(context).hoverColor,
          height: 320,
          custom_bar: Container(
            padding: EdgeInsets.all(generalPadding),
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Fase a gironi",
                      style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    RichText(
                      text: TextSpan(style: TextStyle(color: Colors.black), children: [
                        TextSpan(
                          text: "Gironi totali: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: "5",
                          style: TextStyle(fontWeight: FontWeight.w400),
                        ),
                      ]),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    RichText(
                      text: TextSpan(style: TextStyle(color: Colors.black), children: [
                        TextSpan(
                          text: "Regole partite: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: "ISO",
                          style: TextStyle(fontWeight: FontWeight.w400),
                        ),
                      ]),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    RichText(
                      text: TextSpan(style: TextStyle(color: Colors.black), children: [
                        TextSpan(
                          text: "Numero partite: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: "25",
                          style: TextStyle(fontWeight: FontWeight.w400),
                        ),
                      ]),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    CustomButton(
                        onPressed: () {
                          Navigator.of(context).pushReplacementNamed('group-stage-ranking');
                        },
                        text: "Classifica",
                        color: Theme.of(context).hintColor)
                  ],
                ),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: SvgPicture.asset(
                    assetPath,
                    semanticsLabel: 'label_1',
                  ),
                )
              ],
            ),
          ),
          custom_body: CustomContainer(
              child: Container(
                margin: EdgeInsets.only(bottom: generalPadding * 2, left: generalPadding - 10, right: generalPadding - 10, top: generalPadding + 15),
                child: ListView.separated(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      return CustomListItem(
                        selector: 0,
                        title: stageGroup[index]["title"],
                        list: tournamentsProgramma,
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) => SizedBox(
                          height: 20,
                        ),
                    itemCount: stageGroup.length),
              ),
              color: Theme.of(context).accentColor),
        ),
      ),
      onWillPop: () => goBack(),
    );
  }

  goBack() {
    if (role == 'admin') {
      Navigator.of(context).pushReplacementNamed('dashboard-current');
    } else
      Navigator.of(context).pushReplacementNamed('tournament-dashboard');
  }
}
