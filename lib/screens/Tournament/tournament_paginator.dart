import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/EnrollTournament.dart';
import 'package:calciobalilla24/models/TournamentInfo.dart';
import 'package:calciobalilla24/screens/Admin/edit_tournament.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_list_container.dart';
import 'package:calciobalilla24/widgets/custom_list_tile.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page_hero.dart';
import 'package:calciobalilla24/widgets/pages.dart';
import 'package:calciobalilla24/widgets/tournament_detail_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class TournamentPaginator extends StatefulWidget {
  final List<EnrollTournament>? enrollList;
  final List<TournamentInfo>? tournamentInfoList;
  const TournamentPaginator({this.enrollList, this.tournamentInfoList, Key? key}) : super(key: key);

  @override
  _TournamentPaginatorState createState() => _TournamentPaginatorState();
}

class _TournamentPaginatorState extends StateMVC<TournamentPaginator> {
  TournamentsController? _con;

  _TournamentPaginatorState() : super(TournamentsController()) {
    _con = controller as TournamentsController;
  }

  int getListLegnth(List<TournamentInfo>? tInfoList, List<EnrollTournament>? enrollList) {
    if (tInfoList != null) {
      return tInfoList.length;
    } else {
      return enrollList!.length;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollPage(
        iconLeftTap: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => PagesBottomTabbar(),
            ),
          );
        },
        icon_left_bar: Icons.arrow_back,
        title_bar: "lista completa",
        color_bar: SECONDARY_DARK_COLOR,
        custom_bar: CustomContainer(
            color: Theme.of(context).backgroundColor,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  RichText(
                      text:
                          TextSpan(text: "Tornei in\n", style: TextStyle(fontWeight: FontWeight.w300, fontSize: 30, color: Colors.black), children: [
                    TextSpan(
                      text: "Programma",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                    )
                  ])),
                  Icon(
                    FontAwesomeIcons.calendarAlt,
                    size: 100,
                  )
                ],
              ),
            )),
        custom_body: CustomContainer(
          backgroundColor: Theme.of(context).backgroundColor,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 30),
            child: CustomListContainer(
              title: "Tornei in programma",
              listWidget: Padding(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  child: ListView.separated(
                    padding: EdgeInsets.only(top: 5),
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: getListLegnth(this.widget.tournamentInfoList, this.widget.enrollList),
                    separatorBuilder: (BuildContext context, int index) => SizedBox(
                      height: 12,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      if (this.widget.tournamentInfoList != null) {
                        return CustomListTile(
                          index,
                          title: this.widget.tournamentInfoList![index].title,
                          subtitle: formatHour.format(this.widget.tournamentInfoList![index].dateScheduled),
                          urlImage: this.widget.tournamentInfoList![index].location.imageCover,
                          onLongPressedButton: () {},
                          radius: 30,
                          textButton: "Modifica",
                          onPressedButton: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => EditTournament(id: this.widget.tournamentInfoList![index].id),
                              ),
                            );
                          },
                        );
                      } else {
                        return CustomListTile(
                          index,
                          title: this.widget.enrollList![index].tournament!.title,
                          subtitle: formatHour.format(this.widget.enrollList![index].tournament!.dateScheduled),
                          urlImage: this.widget.enrollList![index].location!.imageCover,
                          onLongPressedButton: () {},
                          radius: 30,
                          textButton: "Dettaglio",
                          onPressedButton: () {
                            showModalBottomSheet<void>(
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(RADIUS_STANDARD_CONTAINER_BORDER)),
                              context: context,
                              builder: (BuildContext context) {
                                return TournamentDetailWidget(
                                  tournament: this.widget.enrollList![index].tournament,
                                  location: this.widget.enrollList![index].location,
                                );
                              },
                            );
                          },
                        );
                      }
                    },
                  )),
            ),
          ),
          color: Theme.of(context).secondaryHeaderColor,
        ),
        height: 300,
      ),
    );
  }
}
