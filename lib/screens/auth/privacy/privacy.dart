import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:flutter/material.dart';

class Privacy extends StatefulWidget {
  const Privacy({Key? key}) : super(key: key);

  @override
  _PrivacyState createState() => _PrivacyState();
}

class _PrivacyState extends State<Privacy> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(child: Scaffold(
      body: CustomScrollPage(
        height: AUTH_HEIGHT,
        icon_left_bar: Icons.arrow_back,
        iconLeftTap: () => Navigator.of(context).pop(),
        color_title_bar: Colors.black,
        custom_bar:Container(
          margin: EdgeInsets.all(15),
          width: 200,
          height: 200,
          child: Image(
          image: AssetImage('assets/img/logo.png')),
        ),

        custom_body: CustomContainer(
          color: SECONDARY_DARK_COLOR,
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
            child: Column(
            children: [
            
            Text(
              "privacy".toUpperCase(),
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Theme.of(context).hoverColor,
              ),
            ),

            Text(
              "dgpr",
              style:TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.w300,
                color: Theme.of(context).hoverColor
              )
            ),
          ],),),),
        ),
    ),
    onWillPop: () => goBack(),
    );
  }
  goBack(){
    Navigator.of(context).pop();
  }
}
