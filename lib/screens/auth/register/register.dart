import 'package:calciobalilla24/controller/auth_controller.dart';
import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_progress_indicator.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:calciobalilla24/widgets/default_error_dialog.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class Signin extends StatefulWidget {
  @override
  _SigninState createState() => _SigninState();
}

class _SigninState extends StateMVC<Signin> {
  bool _obscureText = true;
  User newUser = new User();
  final format = DateFormat("dd-MM-yyyy");
  final key = GlobalKey<FormState>();
  AuthController? _con;
  String password = "";
  String dropGender = "";
  bool privacy = false;
  //textEditingController
  TextEditingController nameCon = new TextEditingController();
  TextEditingController surnameCon = new TextEditingController();
  TextEditingController dateCon = new TextEditingController();
  TextEditingController emailCon = new TextEditingController();
  TextEditingController passCon = new TextEditingController();
  _SigninState() : super(AuthController()) {
    _con = controller as AuthController?;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: Form(
          key: key,
          child: CustomScrollPage(
            custom_bar: Column(
              children: [
                if (_con!.isLoading) CustomProgressIndicator(),
                Container(
                  margin: EdgeInsets.all(15),
                  width: 200,
                  height: 200,
                  child: Image(image: AssetImage('assets/img/logo.png')),
                ),
              ],
            ),
            custom_body: CustomContainer(
                child: Container(
                  margin: EdgeInsets.all(55),
                  child: Column(children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Registrazione",
                          style: TextStyle(fontSize: SECONDARY_TITLE_SIZE, fontWeight: FontWeight.bold, color: Theme.of(context).hoverColor),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        txtFirstname("Nome", Theme.of(context).hoverColor),
                        SizedBox(
                          height: 20,
                        ),
                        txtLastname("Cognome", Theme.of(context).hoverColor),
                        SizedBox(
                          height: 20,
                        ),
                        txtBirthday("Data di nascita", Theme.of(context).hoverColor, icon: Icons.calendar_today),
                        SizedBox(
                          height: 20,
                        ),
                        txtGender("Genere", Theme.of(context).hoverColor, icon: FontAwesomeIcons.user),
                        SizedBox(
                          height: 20,
                        ),
                        txtEmail("Email", Theme.of(context).hoverColor),
                        SizedBox(
                          height: 20,
                        ),
                        txtPassword("Password", Theme.of(context).hoverColor),
                        SizedBox(
                          height: 20,
                        ),
                        txtRepeatPassword("Ripeti password", Theme.of(context).hoverColor),
                        SizedBox(
                          height: 30,
                        ),
                        RichText(
                          text: TextSpan(children: [
                            TextSpan(text: 'Accetto '),
                            TextSpan(
                                text: 'i termini e le condizioni',
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.of(context).pushNamed('privacy');
                                  },
                                style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  decorationColor: Theme.of(context).hoverColor,
                                  decorationStyle: TextDecorationStyle.wavy,
                                )),
                            TextSpan(
                              text: " del servizio di Calciobalilla24",
                            )
                          ]),
                        ),
                        privacyWidget(),
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomButton(
                          onPressed: () {
                            if (key.currentState!.validate() && !privacy) {
                              showDialog(
                                  context: context,
                                  builder: (_) => DefaultErrorDialog(
                                        title: "Privacy",
                                        subtitle: "Per procedere è necessario accettare i termini e le condizioni.",
                                      ));
                            } else if (key.currentState!.validate() && privacy) {
                              key.currentState!.save();
                              _con!.isLoading = true;
                              _con!.signin(context, newUser);
                              setState(() {});
                            }
                          },
                          text: 'Registrati',
                          color: Theme.of(context).hintColor,
                          width: 200,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        CustomButton(
                            width: 200,
                            onPressed: () {
                              Navigator.of(context).pushNamed('login');
                            },
                            text: 'Login',
                            color: SECONDARY_DARK_COLOR)
                      ],
                    ),
                  ]),
                ),
                color: Theme.of(context).accentColor),
            height: AUTH_HEIGHT,
          ),
        ),
      ),
      onWillPop: () => nothing(),
    );
  }

  TextFormField txtEmail(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => newUser.email = getNotSpacedLowercaseString(value!),
      validator: (value) {
        if (!value!.contains("@") || value.length < 3) {
          return 'inserire un mail valida';
        } else
          return null;
      },
      controller: emailCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtFirstname(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => newUser.firstname = value!,
      validator: (value) => value!.length < 3 ? "Inserisci un nome valido." : null,
      controller: nameCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtLastname(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => newUser.lastname = value!,
      validator: (value) => value!.length < 3 ? "Inserisci un cognome valido." : null,
      controller: surnameCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  DateTimeField txtBirthday(String title, Color mainColor, {IconData? icon, Locale? locale}) {
    return DateTimeField(
      onSaved: (newValue) => newUser.birthday = newValue!,
      cursorColor: mainColor,
      controller: dateCon,
      validator: (value) => value == null ? "Inserisci una data valida." : null,
      onShowPicker: (context, currentValue) {
        return showDatePicker(
            context: context,
            locale: locale == null ? const Locale('it', 'IT') : locale,
            initialDate: currentValue ?? DateTime.now(),
            firstDate: DateTime(1940),
            lastDate: DateTime(2023));
      },
      style: TextStyle(color: mainColor),
      format: format,
      decoration: InputDecoration(
        labelText: title,
        labelStyle: TextStyle(color: mainColor),
        contentPadding: EdgeInsets.all(12),
        hintText: "1/1/1998",
        hintStyle: TextStyle(color: mainColor),
        prefixIcon: Icon(icon, color: mainColor),
        // border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
        // focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.5))),
        // enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
      ),
    );
  }

  DropdownButtonFormField txtGender(String title, Color color, {IconData? icon}) {
    return new DropdownButtonFormField<String>(
      validator: (value) => value == null ? "Seleziona un genere." : null,
      onSaved: (newValue) => newUser.gender = getElementFromJson(genderList, 'name', 'code', newValue!),
      hint: Text(
        title,
        style: TextStyle(color: color),
      ),
      dropdownColor: SECONDARY_DARK_COLOR,
      isExpanded: true,
      items: getListFromJson(genderList, 'name').map((String value) {
        return new DropdownMenuItem<String>(
          value: value,
          child: new Text(
            value,
            style: TextStyle(
              color: Theme.of(context).hoverColor,
            ),
          ),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          dropGender = newValue!;
        });
      },
    );
  }

  TextFormField txtPassword(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      cursorColor: mainColor,
      onSaved: (value) => newUser.password = value!,
      validator: (value) {
        if (value!.length < 3) {
          return "Inserisci una password valida.";
        } else if (ALPHANUMERIC_VALIDATOR.hasMatch(value)) {
          return "Inserisci una password alfanumerica";
        } else
          return null;
      },
      style: TextStyle(color: mainColor),
      obscureText: _obscureText,
      controller: passCon,
      decoration: InputDecoration(
        suffixIcon: IconButton(
            icon: Icon(
              _obscureText ? Icons.visibility : Icons.visibility_off,
              color: mainColor,
            ),
            onPressed: () {
              setState(() {
                _obscureText = !_obscureText;
              });
            }),
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtRepeatPassword(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      cursorColor: mainColor,
      onSaved: (value) {},
      validator: (value) {
        if (value!.length < 3) {
          return "Inserisci una password valida.";
        } else if (passCon.text != value) {
          return "le password non coincidono.";
        } else
          return null;
      },
      style: TextStyle(color: mainColor),
      obscureText: _obscureText,
      decoration: InputDecoration(
        suffixIcon: IconButton(
            icon: Icon(
              _obscureText ? Icons.visibility : Icons.visibility_off,
              color: mainColor,
            ),
            onPressed: () {
              setState(() {
                _obscureText = !_obscureText;
              });
            }),
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  Row privacyWidget() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Switch(
          value: privacy,
          onChanged: (value) {
            setState(() {
              privacy = value;
              print(privacy);
            });
          },
          activeColor: Theme.of(context).hoverColor,
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          "Accosento",
          style: TextStyle(
            fontWeight: FontWeight.w500,
            letterSpacing: .1,
            color: Theme.of(context).hoverColor,
          ),
        )
      ],
    );
  }
}
