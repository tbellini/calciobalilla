import 'package:calciobalilla24/controller/auth_controller.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_progress_indicator.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key? key}) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends StateMVC<ForgotPassword> {
  TextEditingController emailCon = new TextEditingController();
  String email = "";
  final _key = GlobalKey<FormState>();
  AuthController? _con;

  _ForgotPasswordState() : super(AuthController()) {
    _con = controller as AuthController?;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Theme.of(context).backgroundColor,
          body: Form(
            key: _key,
            child: Container(
              child: Column(
                children: [
                  if (_con!.isLoading) CustomProgressIndicator(),
                  Container(
                    margin: EdgeInsets.all(15),
                    width: 200,
                    height: AUTH_HEIGHT,
                    child: Image(image: AssetImage('assets/img/logo.png')),
                  ),
                  Expanded(
                      child: CustomContainer(
                          child: Container(
                            margin: EdgeInsets.all(55),
                            child: Column(
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Recupera password",
                                      style: TextStyle(fontSize: SECONDARY_TITLE_SIZE, fontWeight: FontWeight.bold, color: Theme.of(context).hoverColor),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    txtEmail("Email", Theme.of(context).hoverColor),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                CustomButton(
                                    width: 200,
                                    onPressed: () {
                                      if (_key.currentState!.validate()) {
                                        _key.currentState!.save();
                                        setState(() {
                                          _con!.isLoading = true;
                                        });
                                        _con!.forgotPassword(context, email);
                                      }
                                    },
                                    text: 'Invia',
                                    color: Theme.of(context).hintColor),
                              ],
                            ),
                          ),
                          color: Theme.of(context).accentColor)),
                ],
              ),
            ),
          ),
        ),
        onWillPop: () => backFunction(context, 'login'));
  }

  TextFormField txtEmail(String title, Color mainColor, {Icon? icon}) {
    return TextFormField(
      onSaved: (value) => email = value!,
      validator: (value) {
        if (!value!.contains("@") || value.length < 3) {
          return 'inserire un mail valida';
        } else
          return null;
      },
      style: TextStyle(color: mainColor),
      controller: emailCon,
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }
}
