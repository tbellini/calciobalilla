import 'package:calciobalilla24/controller/auth_controller.dart';
import 'package:calciobalilla24/models/CredentialLogin.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_progress_indicator.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends StateMVC<Login> {
  bool _obscureText = true;
  AuthController? _con;
  final _key = GlobalKey<FormState>();
  //late consente che userCredentialLogin sia nullo per ora, perchè il complatore si "fida" che noi andremo ad assegnargli qualcosa
  CredentialLogin _userCredentialLogin = new CredentialLogin();
  //editing controller
  TextEditingController userCon = new TextEditingController();
  TextEditingController passCon = new TextEditingController();

  _LoginState() : super(AuthController()) {
    _con = controller as AuthController?;
  }

  @override
  void dispose() {
    userCon.dispose();
    passCon.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Theme.of(context).backgroundColor,
          body: Form(
            key: _key,
            child: Container(
              child: Column(
                children: [
                  if (_con!.isLoading) CustomProgressIndicator(),
                  Container(
                    margin: EdgeInsets.all(15),
                    width: 200,
                    height: AUTH_HEIGHT,
                    child: Image(image: AssetImage('assets/img/logo.png')),
                  ),
                  Expanded(
                      child: CustomContainer(
                          child: Container(
                            margin: EdgeInsets.all(55),
                            child: SingleChildScrollView(
                              child: Column(children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Login",
                                      style:
                                          TextStyle(fontSize: SECONDARY_TITLE_SIZE, fontWeight: FontWeight.bold, color: Theme.of(context).hoverColor),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    txtEmail("Email", Theme.of(context).hoverColor),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    txtPassword("Password", Theme.of(context).hoverColor),
                                    SizedBox(
                                      height: 30,
                                    ),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    CustomButton(
                                        width: 200,
                                        onPressed: () {
                                          if (_key.currentState!.validate()) {
                                            _key.currentState!.save();
                                            print(_userCredentialLogin.username);

                                            setState(() {
                                              _con!.isLoading = true;
                                            });
                                            _con!.login(context, _userCredentialLogin);
                                          }
                                        },
                                        text: 'Login',
                                        color: Theme.of(context).hintColor),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    CustomButton(
                                        width: 200,
                                        onPressed: () {
                                          Navigator.of(context).pushNamed('signin');
                                        },
                                        text: 'Registrati',
                                        color: SECONDARY_DARK_COLOR),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    RichText(
                                      text: TextSpan(children: [
                                        TextSpan(text: 'password dimenticata? '),
                                        TextSpan(
                                            text: 'recuperala.',
                                            style: TextStyle(
                                              decoration: TextDecoration.underline,
                                              decorationStyle: TextDecorationStyle.wavy,
                                            ),
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                Navigator.of(context).pushNamed('forgot-password');
                                              })
                                      ]),
                                    ),
                                  ],
                                ),
                              ]),
                            ),
                          ),
                          color: Theme.of(context).accentColor)),
                ],
              ),
            ),
          )),
      onWillPop: () => nothing(),
    );
  }

  TextFormField txtEmail(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => _userCredentialLogin.username = getNotSpacedLowercaseString(value!),
      validator: (value) {
        if (value!.length < 3) {
          return 'inserire una mail o username validi';
        } else
          return null;
      },
      style: TextStyle(color: mainColor),
      controller: userCon,
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtPassword(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      cursorColor: mainColor,
      onSaved: (value) => _userCredentialLogin.password = value!,
      validator: (value) => value!.length < 3 ? "inserici una password valida" : null,
      controller: passCon,
      style: TextStyle(color: mainColor),
      obscureText: _obscureText,
      decoration: InputDecoration(
        suffixIcon: IconButton(
            icon: Icon(
              _obscureText ? Icons.visibility : Icons.visibility_off,
              color: mainColor,
            ),
            onPressed: () {
              setState(() {
                _obscureText = !_obscureText;
              });
            }),
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }
}
