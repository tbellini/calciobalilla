import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/Location.dart';
import 'package:calciobalilla24/models/LocationInfo.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page_hero.dart';
import 'package:calciobalilla24/widgets/pages.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class EditLocation extends StatefulWidget {
  final Location? location;
  EditLocation({this.location});
  @override
  _EditLocationState createState() => _EditLocationState();
}

class _EditLocationState extends StateMVC<EditLocation> {
  final format = DateFormat("dd-MM-yyyy");
  bool _isSelected = false;
  TournamentsController? _con;
  Location currentLocation = new Location();
  Location newLocation = new Location();
  var key = GlobalKey<FormState>();
  String? imageEncoded;

  final ImagePicker _picker = ImagePicker();

  TextEditingController titleCon = new TextEditingController();
  TextEditingController addressCon = new TextEditingController();
  TextEditingController cityCon = new TextEditingController();
  TextEditingController postalCodeCon = new TextEditingController();
  TextEditingController countryCon = new TextEditingController();

  _EditLocationState() : super(TournamentsController()) {
    _con = controller as TournamentsController;
  }

  @override
  void initState() {
    currentLocation = this.widget.location!;
    titleCon.text = this.widget.location!.title;
    addressCon.text = this.widget.location!.address;
    cityCon.text = this.widget.location!.city;
    setState(() {});

    super.initState();
  }

  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: WillPopScope(
      child: CustomScrollPageHero(
          //altezza immagine+parte bianca
          height: 380,
          // numero più basso = aumenta di dimensione e viceversa
          imageDimension: 4.5,
          backArrow: true,
          navigateWidget: PagesBottomTabbar(
            currentTab: 2,
          ),
          custom_bar: Container(
            padding: EdgeInsets.all(generalPadding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              this.widget.location!.title.length >= 16
                                  ? this.widget.location!.title.substring(0, 16) + "..."
                                  : this.widget.location!.title,
                              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              this.widget.location!.address + ", " + this.widget.location!.city,
                              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
                            ),
                          ],
                        ),
                        IconButton(
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (_) => AlertDialog(
                                        title: Text("Eliminare la location?"),
                                        content:
                                            Text("Sei sicuro di voler eliminare definitivamente la location: " + this.widget.location!.title + "?"),
                                        actions: [
                                          TextButton(
                                              onPressed: () {
                                                _con!.deleteLocation(this.widget.location!.id!, context);
                                                Navigator.pop(context);
                                              },
                                              child: Text("Si")),
                                          TextButton(
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: Text("No"))
                                        ],
                                      ));
                            },
                            icon: Icon(
                              Icons.delete,
                              size: 30,
                            )),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 6),
                      child: Divider(
                        thickness: 0.5,
                      ),
                    ),
                    Text(
                      "In questa sezione sarà possibile modificare il nome della location, aggiungere un immagine o eliminare la location stessa. Per cambiare nome e indirizzo, eliminare la location corrente e crearne una nuova.",
                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
                    ),
                    
                  ],
                ),
              ],
            ),
          ),
          imageUrl: this.widget.location!.imageCover,
          base64Url: newLocation.imageCover,
          fillRemaining: true,
          custom_body: CustomContainer(
              child: Form(
                key: key,
                child: Container(
                  margin: EdgeInsets.all(generalPadding + 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            "Modificare i dati",
                            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          txtTitle("Titolo", Theme.of(context).hoverColor),
                          SizedBox(
                            height: 20,
                          ),
                          txtAddress("Indirizzo (es. via giuseppe mazzini, 76)", Theme.of(context).hoverColor),
                          SizedBox(
                            height: 20,
                          ),
                          txtCity("Città", Theme.of(context).hoverColor),
                          
                        ],
                      ),

                      imageEncoded != null
                              ? Column(
                                  children: [
                                    SizedBox(
                                      height: 40,
                                    ),
                                    _con!.isLoading
                                        ? CircularProgressIndicator()
                                        : Icon(
                                            FontAwesomeIcons.check,
                                            color: Theme.of(context).hoverColor,
                                            size: 50,
                                          ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      _con!.isLoading ? "Caricamento immagine..." : "Immagine selezionata correttamente",
                                      style: TextStyle(color: Theme.of(context).hoverColor, fontWeight: FontWeight.w300),
                                    ),
                                    SizedBox(
                                      height: 40,
                                    ),
                                  ],
                                )
                              : Column(
                                  children: [
                                    SizedBox(
                                      height: 40,
                                    ),
                                    IconButton(
                                      onPressed: () async {
                                        final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
                                        if (image != null) {
                                          image.readAsBytes().then((value) {
                                            imageEncoded = base64Encode(value);
                                            newLocation.imageCover = imageEncoded!;
                                            setState(() {});
                                          });
                                        }
                                      },
                                      icon: Icon(FontAwesomeIcons.plus),
                                      color: Theme.of(context).hoverColor,
                                      iconSize: 50,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      "Premere per modificare l'immagine",
                                      style: TextStyle(color: Theme.of(context).hoverColor, fontWeight: FontWeight.w300),
                                    ),
                                    SizedBox(
                                      height: 40,
                                    ),
                                  ],
                                ),

                      CustomButton(
                        onPressed: () {
                          if (key.currentState!.validate()) {
                            key.currentState!.save();
                            newLocation.userId = currentLocation.userId;
                            newLocation.id = currentLocation.id;
                            //location address and city with longitude and latitude
                            newLocation.city = currentLocation.city;
                            newLocation.address = currentLocation.address;
                            newLocation.latitude = currentLocation.latitude;
                            newLocation.longitude = currentLocation.longitude;
                            _con!.isLoading = true;
                            _con!.editLocation(newLocation, context).then((value) {
                              showDialog(
                                  context: context,
                                  builder: (_) => AlertDialog(
                                        title: Text("Stato modifica:"),
                                        content: Text(value.toString()),
                                        actions: [
                                          TextButton(
                                              onPressed: () {
                                                Navigator.of(context).pushReplacementNamed('pages');
                                              },
                                              child: Text("Ok"))
                                        ],
                                      ));
                            });
                            setState(() {});
                          }
                        },
                        text: 'Modifica locale',
                        color: Theme.of(context).hintColor,
                        width: 200,
                      ),
                    ],
                  ),
                ),
              ),
              color: Theme.of(context).accentColor)),
      onWillPop: () => goBack(),
    ));
  }

  goBack() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => PagesBottomTabbar(
          currentTab: 2,
        ),
      ),
    );
  }

  TextFormField txtTitle(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => newLocation.title = value!,
      validator: (value) {
        if (value!.length < 3) {
          return 'inserire un titolo valido';
        } else
          return null;
      },
      controller: titleCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtAddress(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      enabled: false,
      controller: addressCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtCity(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      enabled: false,
      controller: cityCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }
}
