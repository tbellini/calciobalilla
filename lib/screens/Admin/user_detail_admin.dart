import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/Location.dart';
import 'package:calciobalilla24/models/LocationInfo.dart';
import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/repositories/user_repo.dart';
import 'package:calciobalilla24/screens/Admin/edit_location.dart';
import 'package:calciobalilla24/screens/Admin/edit_tournament.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_list_tile.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:calciobalilla24/widgets/empty_list_widget.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmerCircle.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmer_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shimmer/shimmer.dart';

class UserDetailAdmin extends StatefulWidget {
  @override
  _UserDetailAdminState createState() => _UserDetailAdminState();
}

class _UserDetailAdminState extends StateMVC<UserDetailAdmin> {
  User myUser = new User();
  List<LocationInfo> locationList = [];
  TournamentsController? _con;
  double standardHeight = 350;


  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  _UserDetailAdminState() : super(TournamentsController()) {
    _con = controller as TournamentsController;
  }

  @override
  void initState() {
    setState(() {
      _con!.isLoading = true;
    });

    getMyUser().then((value) {
      setState(() {
        myUser = value;
      });
    });

    _con!.getLocationList().then((value) {
      setState(() {
        locationList = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double containerLocationListHeight = MediaQuery.of(context).size.height - (standardHeight + 100 + BOTTOM_BAR_HEIGHT);

    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: CustomScrollPage(
            title_bar: "USER",
            color_title_bar: Colors.white,
            color_bar: SECONDARY_DARK_COLOR,
            height: standardHeight,
            custom_bar: CustomContainer(
              color: Theme.of(context).backgroundColor,
              child: Container(
                padding: EdgeInsets.all(generalPadding),
                child: Column(children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    myUser.gender == ""
                          ? Expanded(
                              child: Shimmer.fromColors(
                                  baseColor: Colors.grey[300]!,
                                  highlightColor: Colors.grey[100]!,
                                  enabled: true,
                                  child: ShimmerCircle(radius: MediaQuery.of(context).size.width / 6)),
                            )
                          : CircleAvatar(
                              backgroundImage: myUser.gender == 'm' ? NetworkImage(URL_AVATAR_M) : NetworkImage(URL_AVATAR_F),
                              radius: MediaQuery.of(context).size.width / 6,
                              backgroundColor: PRIMARY_LIGHT_COLOR,
                            ),
                    myUser.firstname == ""
                        ? Expanded(
                            child: Shimmer.fromColors(
                                baseColor: Colors.grey[300]!,
                                highlightColor: Colors.grey[100]!,
                                enabled: true,
                                child: ShimmerText(
                                  2,
                                  padding: EdgeInsets.only(left: 15),
                                )),
                          )
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                myUser.firstname + " " + myUser.lastname,
                                style: TextStyle(fontSize: MAIN_TITLE_SIZE, fontWeight: MAIN_TITLE_WEIGHT),
                                maxLines: 2,
                              ),
                              Text(
                                format.format(myUser.birthday),
                                style: TextStyle(fontSize: DESCRIPTION_SIZE, fontWeight: DESCRIPTION_WEIGHT),
                              ),
                              Text(
                                myUser.phoneNumber,
                                style: TextStyle(fontSize: DESCRIPTION_SIZE, fontWeight: DESCRIPTION_WEIGHT),
                              ),
                              // SizedBox(
                                //   height: 12,
                                // ),
                              ],
                            ),
                    ],
                  ),
                SizedBox(
                  height: 20,
                  ),
                  CustomButton(
                      onPressed: () {
                      Navigator.of(context).pushNamed('edit-profile');
                      },
                    width: 400,
                    text: "Modifica profilo",
                      color: Theme.of(context).hintColor)
              ]),
              ),
            ),
            custom_body: CustomContainer(
              height: locationList.length < 3 ? MediaQuery.of(context).size.height / 2 : null,
                color: PRIMARY_LIGHT_COLOR,
                child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RichText(
                                text: TextSpan(
                                    text: "Gestione ",
                                    style: TextStyle(fontSize: 19, fontWeight: FontWeight.w400, color: Colors.black),
                                    children: [
                                  TextSpan(
                                    text: "Location",
                                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800, color: Colors.black),
                                  )
                                ])),
                            IconButton(
                                onPressed: () {
                                  Navigator.of(context).pushReplacementNamed('new-location');
                                },
                                icon: Icon(FontAwesomeIcons.plus))
                          ],
                        ),
                        _con!.isLoading
                            ? Container(
                                height: containerLocationListHeight,
                                width: MediaQuery.of(context).size.width,
                                child: Center(
                                  child: CircularProgressIndicator(),
                                ),
                              )
                            : locationList.isNotEmpty
                                ? ListView.separated(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemBuilder: (BuildContext context, int index) {
                                      return CustomListTile(
                                        index,
                                        title: locationList[index].title,
                                        subtitle: locationList[index].address + ", " + locationList[index].city,
                                        urlImage: locationList[index].imageCover,
                                        radius: 30,
                                        onPressedTile: () {
                                          _con!.getLocationDetail(locationList[index].id!).then((value) {
                                            Navigator.pushReplacement(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => EditLocation(location: value),
                                              ),
                                            );
                                          });
                                        },
                                      );
                                    },
                                    separatorBuilder: (BuildContext context, int index) => SizedBox(
                                          height: 15,
                                        ),
                                    itemCount: locationList.length)
                                : EmptyListWidget(
                                    containerHeight: containerLocationListHeight, icon: FontAwesomeIcons.thumbtack, title: "Crea una nuova location!")
                      ],
                    )))));
  }
}
