import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/Tournament.dart';
import 'package:calciobalilla24/models/TournamentInfo.dart';
import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/repositories/notification_repo.dart';
import 'package:calciobalilla24/repositories/user_repo.dart';
import 'package:calciobalilla24/screens/Admin/edit_tournament.dart';
import 'package:calciobalilla24/screens/Tournament/admin/dashboard_paginator.dart';
import 'package:calciobalilla24/screens/Tournament/tournament_paginator.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/TeamsListWidget.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_list_container.dart';
import 'package:calciobalilla24/widgets/custom_list_tile.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:calciobalilla24/widgets/empty_card_widget.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmerCircle.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmer_text.dart';
import 'package:calciobalilla24/widgets/tournament_detail_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shimmer/shimmer.dart';

class AdminDashboard extends StatefulWidget {
  @override
  _AdminDashboardState createState() => _AdminDashboardState();
}

class _AdminDashboardState extends StateMVC<AdminDashboard> {
  User myUser = new User();
  TournamentsController? _con;
  List<TournamentInfo> scheduledTournament = [];
  List<TournamentInfo> endedTournament = [];
  int itemCount = 4;
  int? itemCountScheduled;
  _AdminDashboardState() : super(TournamentsController()) {
    _con = controller as TournamentsController;
  }
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void initState() {
    checkInternet(context);
    setState(() {
      _con!.isLoading = true;
    });
    getMyUser().then((value) {
      setState(() {
        myUser = value;
      });
    });
    getAllTournamentsList();
    _con!.getControllerRole();
    super.initState();
  }

  getAllTournamentsList() {
    _con!.getTournamentsListAdminID().then((value) {
      scheduledTournament = value;
      if (value.length < itemCount) {
        itemCountScheduled = value.length;
      } else {
        itemCountScheduled = itemCount;
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: CustomScrollPage(
        iconRightTap: () => Navigator.of(context).pushReplacementNamed('login'),
        icon_right_bar: Icons.logout,
        title_bar: "HOME",
        color_title_bar: Theme.of(context).hoverColor,
        color_bar: Theme.of(context).hintColor,
        custom_bar: Container(
            padding: EdgeInsets.all(generalPadding),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                myUser.gender == ""
                    ? Expanded(
                        child: Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            enabled: true,
                            child: ShimmerCircle(radius: MediaQuery.of(context).size.width / 6)),
                      )
                    : CircleAvatar(
                        backgroundImage: myUser.gender == 'm' ? NetworkImage(URL_AVATAR_M) : NetworkImage(URL_AVATAR_F),
                        radius: MediaQuery.of(context).size.width / 6,
                        backgroundColor: PRIMARY_LIGHT_COLOR,
                      ),
                myUser.firstname == ""
                    ? Expanded(
                        child: Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            enabled: true,
                            child: ShimmerText(
                              2,
                              mainAxis: MainAxisAlignment.center,
                              padding: EdgeInsets.only(right: 15),
                            )),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            myUser.firstname + " " + myUser.lastname,
                            style: TextStyle(fontSize: MAIN_TITLE_SIZE, fontWeight: MAIN_TITLE_WEIGHT),
                          ),
                          Text(
                            format.format(myUser.birthday),
                            style: TextStyle(fontSize: DESCRIPTION_SIZE, fontWeight: DESCRIPTION_WEIGHT),
                          ),
                          Text(
                            myUser.phoneNumber,
                            style: TextStyle(fontSize: DESCRIPTION_SIZE, fontWeight: DESCRIPTION_WEIGHT),
                          ),
                        ],
                      ),
              ],
            )),
        custom_body: CustomContainer(
            child: Container(
                margin: EdgeInsets.only(bottom: generalPadding * 2, left: generalPadding - 10, right: generalPadding - 10, top: generalPadding + 15),
                child: Column(
                  children: [
                    _con!.isLoading
                        ? Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            enabled: true,
                            child: ShimmerText(
                              0,
                              mainAxis: MainAxisAlignment.center,
                              padding: EdgeInsets.all(0),
                              borderRadius: 20,
                              mainHeight: 250,
                            ))
                        : scheduledTournament.isEmpty
                            ? EmptyCardWidget(
                                height: 350,
                                titleCard: "Tornei in programma",
                                titleWidget: "Nessun torneo, trovano uno!",
                                firstColor: Colors.grey[100],
                                secondaryColor: Colors.grey[400],
                                textColorEmptyListWidget: Colors.grey,
                                textColor: Colors.black54,
                                iconColor: PRIMARY_LIGHT_COLOR,
                                color: PRIMARY_LIGHT_COLOR,
                                icon: FontAwesomeIcons.calendarPlus,
                              )
                            : CustomListContainer(
                                listLength: scheduledTournament.length,
                                color: PRIMARY_LIGHT_COLOR,
                                textColor: Colors.black54,
                                title: "Tornei in programma",
                                subtitle: "Tieni premuto per eliminare l'iscrizione",
                                listWidget: Padding(
                                  padding: EdgeInsets.symmetric(vertical: 15),
                                  child: ListView.separated(
                                    padding: EdgeInsets.only(top: 5),
                                    shrinkWrap: true,
                                    physics: const NeverScrollableScrollPhysics(),
                                    itemCount: itemCountScheduled!,
                                    separatorBuilder: (BuildContext context, int index) => SizedBox(
                                      height: 12,
                                    ),
                                    itemBuilder: (BuildContext context, int index) {
                                      return CustomListTile(index,
                                          onPressedTile: () async {
                                            Tournament tournament = await _con!.getTournamentDetail(scheduledTournament[index].id!);
                                            Navigator.pushReplacement(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => DashboardPaginator(
                                                  tournament: tournament,
                                                ),
                                              ),
                                            );
                                          },
                                          id: scheduledTournament[index].id,
                                          title: scheduledTournament[index].title,
                                          subtitle: formatHour.format(scheduledTournament[index].dateScheduled),
                                          urlImage: scheduledTournament[index].location.imageCover,
                                          onLongPressedButton: () {
                                            showDialog(
                                              context: context,
                                              builder: (_) => AlertDialog(
                                                title: Text("Eliminare iscrizione torneo?"),
                                                content: RichText(
                                                  text: TextSpan(
                                                    text: "Procedere con l'eliminazione della squadra dal torneo: ",
                                                    style: TextStyle(color: Colors.black),
                                                    children: [
                                                      TextSpan(
                                                        text: scheduledTournament[index].title,
                                                        style: TextStyle(fontWeight: FontWeight.bold),
                                                      ),
                                                      TextSpan(
                                                        text: "?",
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                actions: [
                                                  TextButton(
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                        //DELETEEE TORUNAMENT TEST
                                                        _con!.getPartecipantTeams(scheduledTournament[index].id!).then((partecipantsList) {
                                                          sendNotification("Test notifica multipla", "Notifica multipla a più users",
                                                              getPartecipantsUsersID(partecipantsList));
                                                          print(getPartecipantsUsersID(partecipantsList));
                                                          // _con!
                                                          //     .deleteTournament(
                                                          //         scheduledTournament[index], context, getPartecipantsUsersID(partecipantsList))
                                                          //     .then((value) {
                                                          //   getAllTournamentsList();
                                                          // });
                                                        });
                                                      },
                                                      child: Text("Si"))
                                                ],
                                              ),
                                            );
                                          },
                                          radius: 25,
                                          trailingWidget: Container(
                                            width: 100,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                IconButton(
                                                    onPressed: () {
                                                      _con!.getTournamentDetail(scheduledTournament[index].id!).then((tournament) {
                                                        _con!.getPartecipantTeams(scheduledTournament[index].id!).then((partecipantsList) {
                                                          showModalBottomSheet<void>(
                                                            isScrollControlled: true,
                                                            shape: RoundedRectangleBorder(
                                                                borderRadius: BorderRadius.circular(RADIUS_STANDARD_CONTAINER_BORDER)),
                                                            context: context,
                                                            builder: (BuildContext context) {
                                                              return TournamentDetailWidget(
                                                                tournament: tournament,
                                                                location: tournament.location,
                                                                lastWidget: partecipantsList.isNotEmpty
                                                                    ? CustomContainer(
                                                                        color: PRIMARY_LIGHT_COLOR,
                                                                        child: Padding(
                                                                          padding: EdgeInsets.only(left: 25, right: 25, top: 30),
                                                                          child: Column(
                                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                            children: [
                                                                              Column(
                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                children: [
                                                                                Text(
                                                                                  "Elenco partecipanti",
                                                                                  style: TextStyle(
                                                                                      fontSize: 25,
                                                                                      fontWeight: FontWeight.w700,
                                                                                      color: SECONDARY_DARK_COLOR),
                                                                                ),
                                                                                Text(
                                                                                  "Elenco delle squadre iscritte a questo torneo.",
                                                                                  style: TextStyle(
                                                                                    fontSize: 10,
                                                                                    fontWeight: FontWeight.w400,
                                                                                    color: SECONDARY_DARK_COLOR,
                                                                                  ),
                                                                                )
                                                                              ],
                                                                              ),
                                                                              SizedBox(
                                                                                height: 20,
                                                                              ),
                                                                              Expanded(
                                                                                child: 
                                                                              TeamsListWidget(
                                                                                partecipantsList: partecipantsList,
                                                                              ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      )
                                                                    : Center(
                                                                        child: Column(
                                                                        children: [
                                                                          Image(
                                                                            image: AssetImage("assets/img/empty.gif"),
                                                                            width: MediaQuery.of(context).size.width,
                                                                          ),
                                                                          Text(
                                                                            "Nessun partecipante",
                                                                            style: TextStyle(color: SECONDARY_DARK_COLOR),
                                                                          )
                                                                        ],
                                                                      )),
                                                              );
                                                            },
                                                          );
                                                        });
                                                      });
                                                    },
                                                    icon: Icon(
                                                      Icons.remove_red_eye_outlined,
                                                    )),
                                                IconButton(
                                                  onPressed: () {
                                                    Navigator.pushReplacement(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) => EditTournament(
                                                          id: scheduledTournament[index].id,
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                  icon: Icon(Icons.edit),
                                                ),
                                              ],
                                            ),
                                          ));
                                    },
                                  ),
                                ),
                                onPressedIcon: () {
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => TournamentPaginator(
                                        tournamentInfoList: scheduledTournament,
                                      ),
                                    ),
                                  );
                                },
                              ),
                    SizedBox(
                      height: 20,
                    ),
                    _con!.isLoading
                        ? Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            enabled: true,
                            child: ShimmerText(
                              0,
                              mainAxis: MainAxisAlignment.center,
                              padding: EdgeInsets.all(0),
                              borderRadius: 20,
                              mainHeight: 250,
                            ))
                        : endedTournament.isEmpty
                            ? EmptyCardWidget(
                                height: 350,
                                titleCard: "Tornei terminati",
                                titleWidget: "Nessun torneo terminato.",
                                firstColor: Colors.grey[100],
                                secondaryColor: Colors.grey[400],
                                textColorEmptyListWidget: Theme.of(context).secondaryHeaderColor,
                                textColor: Colors.black54,
                                iconColor: PRIMARY_LIGHT_COLOR,
                                color: PRIMARY_LIGHT_COLOR,
                                icon: FontAwesomeIcons.calendarCheck,
                              )
                            : CustomListContainer(
                                listLength: endedTournament.length,
                                color: PRIMARY_LIGHT_COLOR,
                                textColor: Colors.black54,
                                title: "Tornei terminati",
                                listWidget: Padding(
                                    padding: EdgeInsets.symmetric(vertical: 15),
                                    child: ListView.separated(
                                      padding: EdgeInsets.only(top: 5),
                                      shrinkWrap: true,
                                      physics: const NeverScrollableScrollPhysics(),
                                      itemCount: itemCount,
                                      separatorBuilder: (BuildContext context, int index) => SizedBox(
                                        height: 12,
                                      ),
                                      itemBuilder: (BuildContext context, int index) {
                                        return CustomListTile(
                                          index,
                                          title: endedTournament[index].title,
                                          id: endedTournament[index].id,
                                          subtitle: formatHour.format(endedTournament[index].dateScheduled),
                                          urlImage: endedTournament[index].location.imageCover,
                                          onLongPressedButton: () {},
                                          radius: 30,
                                          textButton: "Dettaglio",
                                          onPressedButton: () {
                                            _con!.getTournamentDetail(scheduledTournament[index].id!).then((tournament) {
                                              showModalBottomSheet<void>(
                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(RADIUS_STANDARD_CONTAINER_BORDER)),
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return TournamentDetailWidget(
                                                    tournament: tournament,
                                                    location: endedTournament[index].location,
                                                  );
                                                },
                                              );
                                            });
                                          },
                                        );
                                      },
                                    )),
                              ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                )),
            color: Theme.of(context).secondaryHeaderColor),
        height: STANDARD_HEAD_HEIGHT,
      ),
    );
  }
}
