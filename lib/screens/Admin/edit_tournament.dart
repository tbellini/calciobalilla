import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/Location.dart';
import 'package:calciobalilla24/models/LocationInfo.dart';
import 'package:calciobalilla24/models/Tournament.dart';
import 'package:calciobalilla24/screens/Admin/new_location.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_rules_carousel.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:calciobalilla24/widgets/default_error_dialog.dart';
import 'package:calciobalilla24/widgets/pages.dart';
import 'package:calciobalilla24/widgets/selectable_carousel.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmer_carousel.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shimmer/shimmer.dart';

class EditTournament extends StatefulWidget {
  final int? id;
  EditTournament({this.id});
  @override
  _EditTournamentState createState() => _EditTournamentState();
}

class _EditTournamentState extends StateMVC<EditTournament> {
  Tournament currentTournament = Tournament();
  Tournament updatedTournament = Tournament();
  TournamentsController? _con;
  double tournamentCarouselHeight = 180;
  List<LocationInfo> locationList = [];
  int? locationId;
  var _key = GlobalKey<FormState>();

  TextEditingController titleCon = new TextEditingController();
  TextEditingController rulesCon = new TextEditingController();
  TextEditingController notesCon = new TextEditingController();
  TextEditingController dateScheduledCon = new TextEditingController();
  TextEditingController foosballCon = new TextEditingController();
  TextEditingController teamsCon = new TextEditingController();

  bool _obscureText = true;
  final format = DateFormat("dd-MM-yyyy");

  _EditTournamentState() : super(TournamentsController()) {
    _con = controller as TournamentsController;
  }

  @override
  void initState() {
    setState(() {
      _con!.isLoading = true;
    });
    _con!.getTournamentDetail(this.widget.id!).then((value) {
      setState(() {
        currentTournament = value;
        locationId = value.location.id;
        titleCon.text = value.title;
        if (value.rules != null) rulesCon.text = value.rules;
        if (value.notes != null) notesCon.text = value.notes;
        dateScheduledCon.text = formatHour.format(value.dateScheduled);
        foosballCon.text = value.foosballField.toString();
        teamsCon.text = value.teams.toString();
      });
    });
    _con!.getLocationList().then((value) {
      locationList = value;
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: WillPopScope(
      child: CustomScrollPage(
          icon_left_bar: Icons.arrow_back,
          iconLeftTap: () {
            Navigator.of(context).pushReplacementNamed('pages');
          },
          title_bar: "Modifica torneo",
          color_bar: SECONDARY_DARK_COLOR,
          color_title_bar: Theme.of(context).backgroundColor,
          height: 370,
          custom_bar: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(left: generalPadding, top: generalPadding),
                child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                  RichText(
                      text: TextSpan(text: "Modifica ", style: TextStyle(fontSize: 29, fontWeight: FontWeight.w400, color: Colors.black), children: [
                    TextSpan(
                      text: "Torneo",
                      style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800, color: Colors.black),
                    )
                  ])),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Seleziona la location che ospiterà il torneo.",
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ]),
              ),
              _con!.isLoading
                  ? Expanded(
                      child: Shimmer.fromColors(
                        baseColor: Colors.grey[300]!,
                        highlightColor: Colors.grey[100]!,
                        enabled: true,
                        child: ShimmerCarousel(
                          height: tournamentCarouselHeight,
                        ),
                      ),
                    )
                  : locationList.isEmpty
                      ? Container(
                          height: tournamentCarouselHeight,
                          child: Center(
                              child: Stack(
                            alignment: Alignment.bottomCenter,
                            children: [
                              InkWell(
                                onTap: () async {
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => NewLocation(),
                                    ),
                                  );
                                },
                                child: Image(
                                  image: AssetImage("assets/img/plus_green.gif"),
                                  height: 200,
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.only(bottom: 25),
                                  child: Text(
                                    "Crea nuova location",
                                    // style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w400),
                                  ))
                            ],
                          )))
                      : SelectableCarousel(
                          backgroundColor: Colors.grey[100],
                          list: locationList,
                          isLocation: true,
                          idCardSelected: locationId,
                          bottomPadding: 0,
                          carouselPadding: EdgeInsets.only(left: generalPadding, right: 10),
                          height: tournamentCarouselHeight,
                          onTap: (id) {
                            print(id);
                            setState(() {
                              locationId = id;
                            });
                          })
            ],
          ),
          custom_body: CustomContainer(
              child: Form(
                key: _key,
                child: Container(
                  margin: EdgeInsets.all(generalPadding + 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Modificare i dati",
                            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          txtTitle("Titolo", Theme.of(context).hoverColor),
                          SizedBox(
                            height: 20,
                          ),
                          txtdateScheduled("Data d'inizio", Theme.of(context).hoverColor, icon: FontAwesomeIcons.calendar),
                          SizedBox(
                            height: 20,
                          ),
                          txtFoosbalField("Numero di tavoli", Theme.of(context).hoverColor),
                          SizedBox(
                            height: 20,
                          ),
                          txtTeams("Numero di squadre", Theme.of(context).hoverColor),
                          SizedBox(
                            height: 20,
                          ),
                          txtNotes("Note", Theme.of(context).hoverColor),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                      CustomButton(
                          onPressed: () {
                            if (_key.currentState!.validate()) {
                              _key.currentState!.save();
                              updatedTournament.locationId = locationId!;
                              updatedTournament.id = currentTournament.id;
                              _con!.editTournament(updatedTournament, context);
                            }
                          },
                          text: 'Modifica torneo',
                          color: Theme.of(context).hintColor),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
              color: Theme.of(context).accentColor)),
      onWillPop: () => goToCreateTournament(),
    ));
  }

  goToCreateTournament() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => PagesBottomTabbar(),
      ),
    );
  }

  TextFormField txtTitle(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => updatedTournament.title = value!,
      validator: (value) {
        if (value!.length < 3) {
          return 'inserire un titolo valido';
        } else
          return null;
      },
      controller: titleCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtRules(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => updatedTournament.rules = value!,
      validator: (value) => value!.length < 3 ? "Inserisci una regola valida." : null,
      controller: rulesCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtNotes(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => updatedTournament.notes = value!,
      controller: notesCon,
      style: TextStyle(color: mainColor),
      maxLines: 3,
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtFoosbalField(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (value) => updatedTournament.foosballField = int.parse(value!),
      validator: (value) => value!.length < 1 ? "Inserisci un numero di tavoli sufficiente." : null,
      controller: foosballCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtTeams(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (value) => updatedTournament.teams = int.parse(value!),
      validator: (value) => value!.length < 1 ? "Inserisci una disposizione di squadre valida" : null,
      controller: teamsCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  txtdateScheduled(String title, Color mainColor, {IconData? icon}) {
    return DateTimePicker(
      type: DateTimePickerType.dateTime,
      locale: Locale("it", "IT"),
      onSaved: (newValue) {
        updatedTournament.dateScheduled = DateTime.parse(newValue!);
      },
      // dateMask: 'd/MM/yyyy HH:mm',
      controller: dateScheduledCon,
      cursorColor: mainColor,
      style: TextStyle(color: mainColor),
      validator: (val) {
        if (val == "") {
          return "inserisci un'ora valida";
        } else
          return null;
      },
      firstDate: DateTime(1940),
      lastDate: DateTime(2040),
      decoration: InputDecoration(
        labelText: title,
        labelStyle: TextStyle(color: mainColor),
        contentPadding: EdgeInsets.all(12),
        hintText: "1/1/1998",
        hintStyle: TextStyle(color: mainColor),
        prefixIcon: Icon(
          Icons.calendar_today,
          color: mainColor,
        ),
      ),
    );
  }
}
