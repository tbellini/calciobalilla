import 'package:calciobalilla24/screens/User/switch_page_sub_widgets/redirect_widget.dart';
import 'package:flutter/material.dart';

class SwitchAdminPage extends StatefulWidget {
  const SwitchAdminPage({Key? key}) : super(key: key);

  @override
  _SwitchAdminPageState createState() => _SwitchAdminPageState();
}

class _SwitchAdminPageState extends State<SwitchAdminPage> {

  void setState(fn) {
    if (mounted) super.setState(fn);
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[50],
        elevation: 0,
        title: Text(
          "Switch".toUpperCase(),
          style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: RedirectWidget('user'),
    );
  }
}
