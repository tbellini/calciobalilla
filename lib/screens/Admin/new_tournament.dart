import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/LocationInfo.dart';
import 'package:calciobalilla24/models/Tournament.dart';
import 'package:calciobalilla24/screens/Admin/new_location.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_rules_carousel.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:calciobalilla24/widgets/empty_list_widget.dart';
import 'package:calciobalilla24/widgets/selectable_carousel.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmer_carousel.dart';
import 'package:calciobalilla24/widgets/tournaments_carouser.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shimmer/shimmer.dart';

class NewTournament extends StatefulWidget {
  @override
  _NewTournamentState createState() => _NewTournamentState();
}

class _NewTournamentState extends StateMVC<NewTournament> {
  TournamentsController? _con;
  double tournamentCarouselHeight = 180;
  _NewTournamentState() : super(TournamentsController()) {
    _con = controller as TournamentsController?;
  }

  List<LocationInfo> locationList = [];
  Tournament newTournament = new Tournament();
  int? locationId;
  var key = GlobalKey<FormState>();
  bool _obscureText = true;
  DateTime selectedDate = DateTime.now();

  TextEditingController titleCon = new TextEditingController();
  TextEditingController rulesCon = new TextEditingController();
  TextEditingController notesCon = new TextEditingController();
  TextEditingController dateScheduledCon = new TextEditingController();
  TextEditingController hourStartCon = new TextEditingController();
  TextEditingController foosballCon = new TextEditingController();
  TextEditingController teamsCon = new TextEditingController();

  @override
  void initState() {
    setState(() {
      _con!.isLoading = true;
    });
    _con!.getLocationList().then((value) {
      locationList = value;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollPage(
          backArrow: true,
          title_bar: "Nuovo torneo",
          color_bar: SECONDARY_DARK_COLOR,
          color_title_bar: Theme.of(context).backgroundColor,
          height: 370,
          icon_right_bar: Icons.home,
          iconRightTap: () => Navigator.of(context).pushReplacementNamed('pages'),
          custom_bar: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(left: generalPadding, top: generalPadding),
                child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                  RichText(
                      text: TextSpan(
                          text: "Crea un nuovo ",
                          style: TextStyle(fontSize: 29, fontWeight: FontWeight.w400, color: Colors.black),
                          children: [
                        TextSpan(
                          text: "Torneo",
                          style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800, color: Colors.black),
                        )
                      ])),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Seleziona la location che ospiterà il torneo.",
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ]),
              ),
              _con!.isLoading
                  ? Expanded(
                      child: Shimmer.fromColors(
                        baseColor: Colors.grey[300]!,
                        highlightColor: Colors.grey[100]!,
                        enabled: true,
                        child: ShimmerCarousel(
                          height: tournamentCarouselHeight,
                        ),
                      ),
                    )
                  : locationList.isEmpty
                      ? Container(
                          height: tournamentCarouselHeight,
                          child: Center(
                              child: Stack(
                            alignment: Alignment.bottomCenter,
                            children: [
                              InkWell(
                                onTap: () async {
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => NewLocation(),
                                    ),
                                  );
                                },
                                child: Image(
                                  image: AssetImage("assets/img/plus_green.gif"),
                                  height: 200,
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.only(bottom: 25),
                                  child: Text(
                                    "Crea nuova location",
                                    // style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w400),
                                  ))
                            ],
                          )))
                      : SelectableCarousel(
                          backgroundColor: Colors.grey[100],
                          list: locationList,
                          isLocation: true,
                          idCardSelected: locationId,
                          bottomPadding: 0,
                          carouselPadding: EdgeInsets.only(left: generalPadding, right: 10),
                          height: tournamentCarouselHeight,
                          onTap: (id) {
                            print(id);
                            setState(() {
                              locationId = id;
                            });
                          })
            ],
          ),
          custom_body: CustomContainer(
              child: Form(
                  key: key,
                  child: Container(
                    margin: EdgeInsets.all(generalPadding + 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Inserire i dati",
                              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            txtTitle("Titolo", Theme.of(context).hoverColor),
                            SizedBox(
                              height: 20,
                            ),
                            txtdateScheduled("Data d'inizio", Theme.of(context).hoverColor, icon: FontAwesomeIcons.calendar),
                            SizedBox(
                              height: 20,
                            ),
                            txtFoosbalField("Numero di tavoli", Theme.of(context).hoverColor),
                            SizedBox(
                              height: 20,
                            ),
                            txtTeams("Numero di squadre", Theme.of(context).hoverColor),
                            SizedBox(
                              height: 20,
                            ),
                            txtNotes("Note", Theme.of(context).hoverColor),
                            SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                        CustomButton(
                            onPressed: () {
                              if (key.currentState!.validate()) {
                                key.currentState!.save();
                                if (locationId != null) {
                                  newTournament.locationId = locationId;
                                  print(locationId);
                                  _con?.createTournament(newTournament, context);
                                } else {
                                  showDialog(
                                      context: context,
                                      builder: (_) => AlertDialog(
                                            title: Text("Seleziona una location!"),
                                            content: Text(
                                                "Per poter procedere con la creazione del torneo, è necessario selezionare una location. Se non ne hai, creane uno!"),
                                            actions: [
                                              TextButton(
                                                onPressed: () {
                                                  Navigator.of(context).pushReplacementNamed("new-location");
                                                },
                                                child: Text("Crea location"),
                                              ),
                                              TextButton(
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                },
                                                child: Text("Seleziona location"),
                                              )
                                            ],
                                          ));
                                }
                              }
                            },
                            text: 'Crea torneo',
                            color: Theme.of(context).hintColor),
                        SizedBox(
                          height: 25,
                        )
                      ],
                    ),
                  )),
              color: Theme.of(context).accentColor)),
    );
  }

  TextFormField txtTitle(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => newTournament.title = value!,
      validator: (value) {
        if (value!.length < 3) {
          return 'inserire un titolo valido';
        } else
          return null;
      },
      controller: titleCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtRules(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => newTournament.rules = value!,
      validator: (value) => value!.length < 3 ? "Inserisci una regola valida." : null,
      controller: rulesCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtNotes(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => newTournament.notes = value!,
      controller: notesCon,
      style: TextStyle(color: mainColor),
      maxLines: 3,
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtFoosbalField(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (value) => newTournament.foosballField = int.parse(value!),
      validator: (value) => value!.length < 1 ? "Inserisci un numero di tavoli sufficiente." : null,
      controller: foosballCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtTeams(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (value) => newTournament.teams = int.parse(value!),
      validator: (value) => value!.length < 1 ? "Inserisci una disposizione di squadre valida" : null,
      controller: teamsCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

// user_ID --> proprietario del locale

  txtdateScheduled(String title, Color mainColor, {IconData? icon}) {
    return DateTimePicker(
      type: DateTimePickerType.dateTime,
      locale: Locale("it", "IT"),
      onSaved: (newValue) {
        newTournament.dateScheduled = DateTime.parse(newValue!);
        print(newTournament.dateScheduled);
      },
      // dateMask: 'd/MM/yyyy HH:mm',
      controller: hourStartCon,
      cursorColor: mainColor,
      style: TextStyle(color: mainColor),
      validator: (val) {
        if (val == "") {
          return "inserisci un'ora valida";
        } else
          return null;
      },
      firstDate: DateTime(1940),
      lastDate: DateTime(2040),
      decoration: InputDecoration(
        labelText: title,
        labelStyle: TextStyle(color: mainColor),
        contentPadding: EdgeInsets.all(12),
        hintText: "1/1/1998",
        hintStyle: TextStyle(color: mainColor),
        prefixIcon: Icon(
          Icons.calendar_today,
          color: mainColor,
        ),
      ),
    );
  }
}
