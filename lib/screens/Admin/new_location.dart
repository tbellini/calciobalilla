import 'dart:convert';

import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/Suggestion.dart';
import 'package:calciobalilla24/models/Tournament.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/models/Location.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_rules_carousel.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:calciobalilla24/widgets/default_error_dialog.dart';
import 'package:calciobalilla24/widgets/pages.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class NewLocation extends StatefulWidget {
  @override
  _NewLocationState createState() => _NewLocationState();
}

class _NewLocationState extends StateMVC<NewLocation> {
  TournamentsController? _con;
  List<Suggestion> addressList = [];

  _NewLocationState() : super(TournamentsController()) {
    _con = controller as TournamentsController?;
  }
  final ImagePicker _picker = ImagePicker();
  LatLng? selectedPlace;
  Location newLocation = new Location();
  var key = GlobalKey<FormState>();
  bool _obscureText = true;
  String assetPath = "assets/img/location.gif";
  String? imageEncoded;

  TextEditingController titleCon = new TextEditingController();
  TextEditingController addressCon = new TextEditingController();
  TextEditingController cityCon = new TextEditingController();
  TextEditingController countryCon = new TextEditingController();
  TextEditingController civicNumberCon = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        child: CustomScrollPage(
            backArrow: true,
            title_bar: "Nuova Location",
            color_bar: SECONDARY_DARK_COLOR,
            color_title_bar: Theme.of(context).backgroundColor,
            height: 300,
            icon_right_bar: Icons.home,
            iconRightTap: () => goBack(),
            custom_bar: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(left: generalPadding, top: generalPadding),
                  child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RichText(
                            text: TextSpan(
                                text: "Crea una nuova",
                                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w400, color: Colors.black),
                                children: [
                              TextSpan(
                                text: "\nLocation",
                                style: TextStyle(fontSize: 26, fontWeight: FontWeight.w800, color: Colors.black),
                              )
                            ])),
                        Image.asset(
                          assetPath,
                          height: 180,
                        )
                      ],
                    )
                  ]),
                ),
              ],
            ),
            custom_body: CustomContainer(
                child: Form(
                    key: key,
                    child: Container(
                      margin: EdgeInsets.all(generalPadding + 20),
                      height: selectedPlace == null ? null : MediaQuery.of(context).size.height / 1.7,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Text(
                                      "Inserire i dati",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
                                    ),
                                  ),
                                  IconButton(
                                      onPressed: () {
                                        addressList.clear();
                                        selectedPlace = null;
                                        imageEncoded = null;
                                        setState(() {});
                                      },
                                      icon: Icon(
                                        Icons.refresh,
                                        color: Theme.of(context).hoverColor,
                                        size: 32,
                                      ))
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              txtTitle("Titolo", Theme.of(context).hoverColor),
                              SizedBox(
                                height: 20,
                              ),
                              txtAddress("Indirizzo (es. via giuseppe mazzini, 76)", Theme.of(context).hoverColor),
                              if (addressList.isEmpty && selectedPlace == null)
                                Column(
                                  children: [
                                    SizedBox(
                                      height: 20,
                                    ),
                                    txtCivicNumber("Numero civico", Theme.of(context).hoverColor),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    txtCity("Città", Theme.of(context).hoverColor),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    txtCountry("Provincia", Theme.of(context).hoverColor),
                                    SizedBox(
                                      height: 25,
                                    ),
                                  ],
                                ),
                              if (addressList.isNotEmpty)
                                AnimatedContainer(
                                  duration: new Duration(microseconds: 500),
                                  height: addressList.isEmpty ? 0 : 300,
                                  child: ListView.builder(
                                      itemCount: addressList.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        print(addressList);
                                        return ListTile(
                                          leading: Icon(
                                            Icons.gps_fixed,
                                            color: Theme.of(context).hoverColor,
                                          ),
                                          title: Text(
                                            addressList[index].mainText,
                                            style: TextStyle(color: Theme.of(context).hoverColor),
                                          ),
                                          subtitle: Text(
                                            addressList[index].secondaryText,
                                            style: TextStyle(color: Theme.of(context).dividerColor),
                                          ),
                                          onTap: () async {
                                            _con!.getLatLangFromPlaceId(addressList[index].placeId).then((value) {
                                              selectedPlace = value;
                                              addressCon.text = addressList[index].description;
                                              newLocation.address = addressList[index].mainText;
                                              newLocation.city = addressList[index].secondaryText;
                                              addressList.clear();
                                              setState(() {});
                                            });
                                          },
                                        );
                                      }),
                                ),
                              if (addressList.isEmpty && selectedPlace == null)
                                CustomButton(
                                  onPressed: () {
                                    if (key.currentState!.validate()) {
                                      _con!
                                          .getListGoogleAddress(
                                              addressCon.text + ", " + civicNumberCon.text + ", " + cityCon.text + ", " + countryCon.text, 'it')
                                          .then((value) {
                                        if (value.isEmpty) {
                                          showDialog(
                                              context: context,
                                              builder: (_) => AlertDialog(
                                                    title: Text("Attenzione!"),
                                                    content: Text("Nessun risultato per: " +
                                                        addressCon.text +
                                                        ", " +
                                                        civicNumberCon.text +
                                                        ", " +
                                                        cityCon.text +
                                                        ", " +
                                                        countryCon.text),
                                                  ));
                                        } else {
                                          setState(() {
                                            addressList = value;
                                          });
                                        }
                                      });
                                    }
                                  },
                                  text: "Cerca indirizzi",
                                  color: SECONDARY_DARK_COLOR,
                                ),
                            ],
                          ),
                          if (selectedPlace != null)
                            Column(
                              children: [
                                imageEncoded == null
                                    ? IconButton(
                                        onPressed: () async {
                                          final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
                                          image!.readAsBytes().then((value) {
                                            imageEncoded = base64Encode(value);
                                            newLocation.imageCover = imageEncoded!;
                                            setState(() {});
                                          });
                                        },
                                        icon: Icon(FontAwesomeIcons.plus),
                                        color: Theme.of(context).hoverColor,
                                        iconSize: 50,
                                      )
                                    : Container(
                                        height: 160,
                                        width: 250,
                                        child: Stack(
                                          alignment: Alignment.center,
                                          children: [
                                            ClipRRect(
                                              child: Image.memory(
                                                base64Decode(imageEncoded!),
                                                width: 250,
                                                fit: BoxFit.cover,
                                              ),
                                              borderRadius: BorderRadius.all(Radius.circular(15)),
                                            ),
                                            if (_con!.isLoading)
                                              ClipPath(
                                                child: Container(
                                                  decoration:
                                                      BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(15)), color: Colors.black45),
                                                  child: Center(child: CircularProgressIndicator()),
                                                ),
                                              )
                                          ],
                                        ),
                                      ),
                                          
                                if (imageEncoded == null)
                                  Container(
                                      margin: EdgeInsets.only(top: 10),
                                      child: Text(
                                        "Aggiungi immagine",
                                        style: TextStyle(color: Theme.of(context).hoverColor, fontSize: 15, fontWeight: FontWeight.w300),
                                      ))
                              ],
                            ),
                          if (selectedPlace != null)
                            CustomButton(
                                onPressed: () {
                                  if (key.currentState!.validate()) {
                                    key.currentState!.save();
                                    newLocation.latitude = selectedPlace!.latitude;
                                    newLocation.longitude = selectedPlace!.longitude;
                                    if ((newLocation.latitude != null || newLocation.latitude == 0) &&
                                        (newLocation.longitude != null || newLocation.longitude == 0)) {
                                      _con!.isLoading = true;
                                      _con!.createLocation(newLocation, context);
                                      setState(() {});
                                    } else {
                                      showDialog(
                                          context: context,
                                          builder: (_) => DefaultErrorDialog(
                                                title: "Attenzione!",
                                                subtitle: "Seleziona un indirizzo valido",
                                                buttonText: "Ok",
                                              ));
                                    }
                                  }
                                },
                                text: 'Crea location',
                                color: Theme.of(context).hintColor),
                        ],
                      ),
                    )),
                color: Theme.of(context).accentColor)),
        onWillPop: () => goBack(),
      ),
    );
  }

  goBack() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => PagesBottomTabbar(
          currentTab: 2,
        ),
      ),
    );
  }

  TextFormField txtTitle(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => newLocation.title = value!,
      validator: (value) {
        if (value!.length < 3) {
          return 'inserire un titolo valido';
        } else
          return null;
      },
      controller: titleCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtAddress(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      readOnly: selectedPlace == null ? false : true,
      validator: (value) => value!.length < 3 ? "Inserisci un indirizzo valido." : null,
      controller: addressCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtCivicNumber(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      validator: (value) => value!.length == 0 ? "Inserisci un numero civico valido." : null,
      controller: civicNumberCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtCity(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      validator: (value) => value!.length < 3 ? "Inserisci una città valida." : null,
      controller: cityCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtCountry(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      validator: (value) => value!.length < 3 ? "Inserisci una provincia valida." : null,
      controller: countryCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }
}
