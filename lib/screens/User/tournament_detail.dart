import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/Friend.dart';
import 'package:calciobalilla24/models/Location.dart';
import 'package:calciobalilla24/models/Tournament.dart';
import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/repositories/friend_repo.dart';
import 'package:calciobalilla24/repositories/user_repo.dart';
import 'package:calciobalilla24/screens/User/map.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_list_tile.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page_hero.dart';
import 'package:calciobalilla24/widgets/empty_list_widget.dart';
import 'package:calciobalilla24/widgets/pages.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmerCircle.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmer_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shimmer/shimmer.dart';

class TournamentDetail extends StatefulWidget {
  final Tournament? tournament;
  @override
  TournamentDetail(this.tournament);
  _TournamentDetailState createState() => _TournamentDetailState();
}

class _TournamentDetailState extends StateMVC<TournamentDetail> {
  List<Friend> friendsList = [];
  List<Friend> updatedFriendsList = [];
  User myUser = User();
  Friend friendSelected = new Friend();
  bool _isSelected = false;
  bool _isLoading = false;
  TournamentsController? _con;
  String assetPath = "assets/img/custom_loading.gif";

  _TournamentDetailState() : super(TournamentsController()) {
    _con = controller as TournamentsController;
  }

  @override
  initState() {
    setState(() {
      _isLoading = true;
    });
    getFriends().then((value) {
      setState(() {
        friendsList = value;
        updatedFriendsList = value;
        _isLoading = false;
      });
    });
    getMyUser().then((value) {
      setState(() {
        myUser = value;
        _isLoading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: CustomScrollPageHero(
          //altezza immagine+parte bianca
          height: 370,
          //numero più basso = aumenta di dimensione e viceversa
          heroTag: "tImage" + this.widget.tournament!.id.toString(),
          backArrow: true,
          navigateWidget: PagesBottomTabbar(
            currentTab: 1,
          ),
          imageDimension: 4.5,
          custom_bar: Container(
            padding: EdgeInsets.all(generalPadding),
            child: this.widget.tournament!.title == ""
                ? Shimmer.fromColors(
                    baseColor: Theme.of(context).dividerColor,
                    highlightColor: PRIMARY_LIGHT_COLOR,
                    enabled: true,
                    child: ShimmerText(
                      3,
                      mainAxis: MainAxisAlignment.center,
                      padding: EdgeInsets.only(right: 15),
                    ))
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            this.widget.tournament!.title,
                            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            this.widget.tournament!.location.title.toUpperCase() +
                                " - " +
                                this.widget.tournament!.location.address +
                                " " +
                                this.widget.tournament!.location.city +
                                " ",
                            style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
                          ),
                          Divider(
                            thickness: 0.5,
                          ),
                          RichText(
                              text: TextSpan(
                                  text: 'Data inizio torneo: ',
                                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
                                  children: [
                                TextSpan(
                                    text: formatHour.format(this.widget.tournament!.dateScheduled),
                                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300))
                              ])),
                          RichText(
                              text: TextSpan(
                                  text: 'Numero di tavoli: ',
                                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
                                  children: [
                                TextSpan(
                                    text: this.widget.tournament!.foosballField.toString(),
                                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300))
                              ])),
                          RichText(
                              text: TextSpan(
                                  text: 'Numero di squadre: ',
                                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
                                  children: [
                                TextSpan(text: this.widget.tournament!.teams.toString(), style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300))
                              ])),
                         if (this.widget.tournament!.notes != "")
                            RichText(
                                text: TextSpan(
                                    text: 'Note: ',
                                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
                                    children: [
                                  TextSpan(text: this.widget.tournament!.notes, style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300))
                                ])),
                          if (this.widget.tournament!.rules != "")
                            RichText(
                                text: TextSpan(
                                    text: 'Regole: ',
                                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
                                    children: [
                                  TextSpan(text: this.widget.tournament!.notes, style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300))
                                ])) 
                        ],
                      ),
                    ],
                  ),
          ),

          imageUrl: this.widget.tournament!.location.imageCover,

          custom_body: CustomContainer(
            backgroundColor: Colors.white,
            color: PRIMARY_LIGHT_COLOR,
            child: Container(
              // height: MediaQuery.of(context).size.height / 1.6,
              margin: EdgeInsets.all(generalPadding),
              child: Column(children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Elenco amici",
                      style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: TextField(
                        onChanged: (text) {
                          updatedFriendsList.clear();
                          updatedFriendsList = _con!.getSearchedList(text, friendsList);
                          setState(() {});
                        },
                        decoration: InputDecoration(
                            fillColor: Theme.of(context).hoverColor,
                            filled: true,
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide.none),
                            prefixIcon: Icon(
                              Icons.search,
                              color: Theme.of(context).secondaryHeaderColor,
                              size: 30,
                            ),
                            hintText: "Cerca",
                            hintStyle: TextStyle(
                              color: Theme.of(context).secondaryHeaderColor,
                            )),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        color: Theme.of(context).hoverColor,
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 40, horizontal: 20),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    _isLoading
                                        ? Shimmer.fromColors(
                                            baseColor: Colors.grey[300]!,
                                            highlightColor: Colors.grey[100]!,
                                            enabled: true,
                                            child: ShimmerCircle(
                                              radius: 45,
                                            ))
                                        : CircleAvatar(
                                            backgroundColor: PRIMARY_LIGHT_COLOR,
                                            foregroundImage: NetworkImage(myUser.gender == 'm' ? URL_AVATAR_M : URL_AVATAR_F),
                                            radius: 45,
                                          ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      myUser.firstname + " " + myUser.lastname,
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: Theme.of(context).secondaryHeaderColor,
                                      ),
                                    )
                                  ],
                                ),
                                _con!.isLoading
                                    ? Image.asset(
                                        assetPath,
                                        height: 60,
                                      )
                                    : Icon(
                                        Icons.link_rounded,
                                        size: 30,
                                        color: Theme.of(context).secondaryHeaderColor,
                                      ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    _isSelected
                                        ? CircleAvatar(
                                            backgroundColor: PRIMARY_LIGHT_COLOR,
                                            foregroundImage: NetworkImage(friendSelected.gender == 'm' ? URL_AVATAR_M : URL_AVATAR_F),
                                            radius: 45,
                                          )
                                        : Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(Radius.circular(100)),
                                              color: PRIMARY_LIGHT_COLOR,
                                            ),
                                            height: 90,
                                            width: 90,
                                            child: Icon(
                                              FontAwesomeIcons.plus,
                                              color: Theme.of(context).secondaryHeaderColor,
                                            ),
                                          ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      _isSelected ? friendSelected.firstname + " " + friendSelected.lastname : "Cerca un amico!",
                                      style: TextStyle(
                                        color: Theme.of(context).secondaryHeaderColor,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            if (_isSelected)
                              Container(
                                margin: EdgeInsets.only(top: 25),
                                child: CustomButton(
                                    onPressed: () {
                                      print("richiesta");
                                      setState(() {
                                        _con!.isLoading = true;
                                      });
                                      _con!.sendTournamentRequest(this.widget.tournament!, friendSelected, myUser, context);
                                    },
                                    text: "Invia richiesta",
                                    color: Theme.of(context).hintColor),
                              )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    _isLoading
                        ? Container(
                            height: MediaQuery.of(context).size.height / 3.2,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          )
                        : updatedFriendsList.isEmpty
                            ? EmptyListWidget(
                                containerHeight: MediaQuery.of(context).size.height / 3.2,
                                icon: FontAwesomeIcons.userFriends,
                                title: "Nessun amico, ",
                                textButton: "trovane uno!",
                                textButtonColor: Theme.of(context).hintColor,
                              )
                            : Container(
                                height: MediaQuery.of(context).size.height / 3.2,
                                child: ListView.separated(
                                  scrollDirection: Axis.vertical,
                                  itemCount: updatedFriendsList.length,
                                  shrinkWrap: true,
                                  physics: updatedFriendsList.length > 2 ? null : NeverScrollableScrollPhysics(),
                                  itemBuilder: (BuildContext context, int index) {
                                    return CustomListTile(
                                      index,
                                      onPressedButton: () {
                                        if (updatedFriendsList[index].firstname != "") {
                                          friendSelected = updatedFriendsList[index];
                                          _isSelected = true;
                                          setState(() {});
                                        }
                                      },
                                      textButton: "Seleziona",
                                      title: updatedFriendsList[index].firstname + " " + updatedFriendsList[index].lastname,
                                      subtitle: updatedFriendsList[index].email,
                                    );
                                  },
                                  separatorBuilder: (BuildContext context, int index) => SizedBox(
                                          height: 20,
                                        )
                                ),
                              ),
                  ],
                ),
              ]),
            ),
          ),
        ),
      ),
      onWillPop: () => goBack(),
    );
  }

  goBack() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => PagesBottomTabbar(
                  currentTab: 1,
                )));
  }
}
