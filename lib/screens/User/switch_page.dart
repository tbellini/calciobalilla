import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/repositories/user_repo.dart';
import 'package:calciobalilla24/screens/User/switch_page_sub_widgets/redirect_widget.dart';
import 'package:calciobalilla24/screens/User/switch_page_sub_widgets/subscription_widget.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_rules_carousel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SwitchPage extends StatefulWidget {
  const SwitchPage({Key? key}) : super(key: key);

  @override
  _SwitchPageState createState() => _SwitchPageState();
}

class _SwitchPageState extends State<SwitchPage> {
  User? myUser;

  @override
  void initState() {
    getMyUser().then((value) {
      setState(() {
        myUser = value;
      });
    });
    super.initState();
  }

  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Widget selectedWidget() {
    if (myUser == null) {
      return Center(child: CircularProgressIndicator());
    } else if (myUser!.dateExpire == DateTime(0)) {
      return SubscriptionWidget();
    } else {
      return RedirectWidget('admin');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey[50],
          elevation: 0,
          title: Text(
            "Switch".toUpperCase(),
            style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        body: selectedWidget());
  }
}
