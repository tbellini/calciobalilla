import 'dart:async';
import 'dart:math';
import 'dart:typed_data';

import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/TournamentInfo.dart';
import 'package:calciobalilla24/screens/User/tournament_detail.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/tournaments_carouser.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class MapPage extends StatefulWidget {
  @override
  _MapState createState() => _MapState();
}

class _MapState extends StateMVC<MapPage> {
  Completer<GoogleMapController> _controller = Completer();
  bool? isLocationEnable;
  LatLng? _center;
  LatLng? currentCameraPosition;
  Set<Marker> _markers = {};
  List<TournamentInfo> tournamentInfoList = [];
  TournamentsController? _con;

  _MapState() : super(TournamentsController()) {
    _con = controller as TournamentsController;
  }

  List<Marker> tournamentInfoToMarkersConverter(List<TournamentInfo> tournamentInfoList) {
    List<Marker> markersList = [];
    for (TournamentInfo tournament in tournamentInfoList) {
      markersList.add(Marker(
        markerId: MarkerId(tournament.id.toString()),
        position: LatLng(tournament.location.latitude!, tournament.location.longitude!),
        infoWindow: InfoWindow(title: tournament.title, snippet: formatHour.format(tournament.dateScheduled)),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
      ));
    }
    return markersList;
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error('Location permissions are permanently denied, we cannot request permissions.');
    }
    return await Geolocator.getCurrentPosition();
  }

  void _onMapCreated(GoogleMapController controller) {
    print("++++++++++++++++++++++++++++++++++++++++++++++++");
    _controller.complete(controller);
  }

  @override
  void initState() {
    Geolocator.isLocationServiceEnabled().then((value) {
      setState(() {
        isLocationEnable = value;
      });
    });
    _determinePosition().then((act_pos) {
      _con!.getTournamentsList().then((value) {
        setState(() {
          _center = LatLng(act_pos.latitude, act_pos.longitude);
          tournamentInfoList = _con!.getTournamentListRange(value, _center!, 6);
          print("");
          _markers.addAll(tournamentInfoToMarkersConverter(value));
        });
      });
    });

    super.initState();
  }

  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: isLocationEnable == null
                ? Center(child: CircularProgressIndicator())
                : !isLocationEnable!
                    ? Container(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height / 2, left: 15, right: 15),
                        child: Column(
                          children: [
                            Text(
                              "Attenzione, la geolocalizzazione non è attiva.\nPer usare questo servizio è necessario attivarla.",
                              textAlign: TextAlign.center,
                              style: TextStyle(fontWeight: FontWeight.w300),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            CustomButton(
                                onPressed: () {
                                  Geolocator.isLocationServiceEnabled().then((value) {
                                    setState(() {
                                      isLocationEnable = value;
                                    });
                                  });
                                },
                                width: MediaQuery.of(context).size.width - 120,
                                text: "Ho attivato la localizzazione",
                                color: Theme.of(context).hintColor),
                          ],
                        ))
                    : _center != null
                        ? Stack(
                            alignment: AlignmentDirectional.bottomStart,
                            children: <Widget>[
                              GoogleMap(
                                  mapToolbarEnabled: false,
                                  mapType: MapType.normal,
                                  initialCameraPosition: CameraPosition(target: _center!, zoom: 12.5),
                                  zoomControlsEnabled: false,
                                  myLocationEnabled: true,
                                  minMaxZoomPreference: MinMaxZoomPreference(11, 18),
                                  markers: _markers,
                                  onCameraIdle: () {
                                    if (currentCameraPosition != null) {
                                      _con!.getTournamentsList().then((value) {
                                        setState(() {
                                          tournamentInfoList = _con!.getTournamentListRange(value, currentCameraPosition!, 6);
                                        });
                                      });
                                    }
                                  },
                                  onCameraMove: (CameraPosition position) async {
                                    setState(() {
                                      currentCameraPosition = position.target;
                                    });
                                  }),
                              Visibility(
                                visible: !tournamentInfoList.isEmpty,
                                child: TouramentsCarousel(
                                  onTap: (id) {
                                    _con!.getTournamentDetail(id).then((value) {
                                      Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => TournamentDetail(value),
                                        ),
                                      );
                                    });
                                  },
                                  list: tournamentInfoList,
                                  height: 180,
                                  bottomPadding: 80,
                                ),
                              )
                            ],
                          )
                        : Center(child: CircularProgressIndicator())));
  }
}
