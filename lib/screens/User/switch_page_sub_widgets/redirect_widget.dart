import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RedirectWidget extends StatefulWidget {
  final String? role;
  const RedirectWidget(this.role, {Key? key}) : super(key: key);

  @override
  _RedirectWidgetState createState() => _RedirectWidgetState();
}

class _RedirectWidgetState extends State<RedirectWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      
      padding: EdgeInsets.symmetric(horizontal: 20),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          this.widget.role == "admin"
              ? SvgPicture.asset(
                  "assets/img/treasure.svg",
                  height: 180,
                  semanticsLabel: 'label_1',
                )
              : Image(
                  image: NetworkImage(
                    URL_AVATAR_M,
                  ),
                  height: 220,
                ),
          SizedBox(
            height: 20,
          ),
          RichText(
              text: TextSpan(
                  text: "Vuoi entrare nella sezione ",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Colors.black),
                  children: <TextSpan>[
                TextSpan(text: this.widget.role! + "?", style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700, color: Colors.black)),
              ])),
          SizedBox(
            height: 10,
          ),
          Text(
            "Per procedere premi il pulsante qui sotto:",
            style: TextStyle(fontWeight: FontWeight.w400, color: Colors.grey[400], fontSize: 12),
          ),
          SizedBox(
            height: 25,
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              border: Border.all(color: Colors.red, width: 2),
            ),
            child: IconButton(
                iconSize: 50,
                color: Colors.red,
                enableFeedback: true,
                onPressed: () async {
                  await changeRole(this.widget.role!);
                  Navigator.of(context).pushReplacementNamed('pages');
                },
                icon: Icon(
                  Icons.repeat,
                )),
          )
        ],
      ),
    );
  }
}
