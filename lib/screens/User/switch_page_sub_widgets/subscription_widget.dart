import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SubscriptionWidget extends StatefulWidget {
  const SubscriptionWidget({Key? key}) : super(key: key);

  @override
  _SubscriptionWidgetState createState() => _SubscriptionWidgetState();
}

class _SubscriptionWidgetState extends State<SubscriptionWidget> {
  List<String> benefits = ["Creazione tornei", "gestione delle location", "visualizzazione tornei", "gestione torneo in autonomia"];
  TextStyle listStyle = TextStyle(fontWeight: FontWeight.w300, fontSize: 15);
  final String assetPath = 'assets/img/treasure.svg';
  int _tabSelected = 0;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Container(
      padding: EdgeInsets.only(left: 20, right: 20, top: 50, bottom: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Go premium",
                    style: TextStyle(fontWeight: FontWeight.w700, fontSize: 10, color: Theme.of(context).hintColor),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Benefit premium",
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 30,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "●  " + benefits[0],
                    style: listStyle,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "●  " + benefits[1],
                    style: listStyle,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "●  " + benefits[2],
                    style: listStyle,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "●  " + benefits[3],
                    style: listStyle,
                  ),
                ],
              ),
           
              SvgPicture.asset(
                assetPath,
                semanticsLabel: 'label_1',
                height: 110,
              ),
              
            ],
          ),

          SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomCard(
                price: "5€",
                subtitle: "Al mese",
                onTap: () {
                  setState(() {
                    _tabSelected = 0;
                  });
                },
                id: 0,
                tabSelected: _tabSelected,
              ),
              CustomCard(
                price: "10€",
                subtitle: "Al mese",
                onTap: () {
                  setState(() {
                    _tabSelected = 1;
                  });
                },
                id: 1,
                tabSelected: _tabSelected,
              ),
              CustomCard(
                price: "99€",
                subtitle: "All'anno",
                onTap: () {
                  setState(() {
                    _tabSelected = 2;
                  });
                },
                id: 2,
                tabSelected: _tabSelected,
              )
            ],
          ),
          SizedBox(
            height: 50,
          ),
          Text(
            "Prova premium per 7 giorni",
            style: TextStyle(fontWeight: FontWeight.w600, color: Colors.grey[400]),
          ),
          SizedBox(
            height: 20,
          ),
          CustomButton(
            onPressed: () {},
            text: "Seleziona questo pacchetto",
            color: Theme.of(context).hintColor,
            width: 250,
          ),
          SizedBox(
            height: 50,
          ),
          Text(
            "Della carta di circolazione, i veicoli che circolano su strada per esigenze connesse con prove tecniche, sperimentali o costruttive, dimostrazioni o trasferimenti, anche per ragioni di vendita",
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.w400, color: Colors.grey[400], fontSize: 12),
          ),
        ],
      ),
    ));
  }
}

class CustomCard extends StatelessWidget {
  final String? price;
  final String? subtitle;
  final Function()? onTap;
  final int? id;
  final int? tabSelected;
  const CustomCard({Key? key, this.price, this.subtitle, this.onTap, this.id, this.tabSelected}) : super(key: key);

  isSelected() {
    if (tabSelected == id) {
      return true;
    } else
      return false;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 150,
        width: (MediaQuery.of(context).size.width - 50) / 3,
        child: InkWell(
            onTap: onTap!,
            child: Card(
              elevation: 4,
              child: Container(
                padding: EdgeInsets.only(top: 45, right: 10, left: 10, bottom: 15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      this.price!,
                      style: TextStyle(fontWeight: FontWeight.w700, fontSize: 22, color: isSelected() ? Colors.white : Colors.black),
                    ),
                    Text(
                      this.subtitle!,
                      style: TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: isSelected() ? Colors.white : Colors.black),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Il prezzo finale sarà\ncompreso di iva al 22%",
                      style: TextStyle(fontSize: 8, fontWeight: FontWeight.w300, color: isSelected() ? Colors.white : Colors.black),
                    )
                  ],
                ),
              ),
              color: isSelected() ? Theme.of(context).hintColor : Colors.white,
            )));
  }
}
