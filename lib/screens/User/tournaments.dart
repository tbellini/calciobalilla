import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_list_item.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Tournaments extends StatefulWidget {
  @override
  _TournamentsState createState() => _TournamentsState();
}

class _TournamentsState extends State<Tournaments> {
  final String assetPath = 'assets/img/cup.svg';

  List tournamentsProgramma = [
    {'bar': 'torneo a', 'due_date': '2021-08-01'},
    {'bar': 'torneo b', 'due_date': '2021-08-01'},
    {'bar': 'torneo c', 'due_date': '2021-08-01'},
    {'bar': 'torneo c', 'due_date': '2021-08-01'}
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: 
     Scaffold(
      body: CustomScrollPage(
        title_bar: "tornei",
        icon_right_bar: Icons.home,
        backArrow: true,
        iconRightTap: () {
          Navigator.of(context).pushReplacementNamed('pages');
        },
        color_bar: SECONDARY_DARK_COLOR,
        color_title_bar: Theme.of(context).hoverColor,
        height: 320,
        custom_bar: Container(
          padding: EdgeInsets.all(generalPadding),
          child: Row(
            children: [
              Text(
                "Tornei in \nprogramma",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800),
              ),
              Expanded(
                child: SvgPicture.asset(
                  assetPath,
                  semanticsLabel: 'label_1',
                ),
              )
            ],
          ),
        ),
        custom_body: CustomContainer(
            child: Container(
                margin: EdgeInsets.only(bottom: generalPadding * 2, left: generalPadding - 10, right: generalPadding - 10, top: generalPadding + 15),
                child: Column(
                  children: [
                    CustomListItem(
                      color: SECONDARY_DARK_COLOR,
                      title: "Bar nuraghi",
                      list: tournamentsProgramma,
                      textColor: Theme.of(context).backgroundColor,
                      
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomListItem(
                      color: SECONDARY_DARK_COLOR,
                      title: "Bar nurra",
                      list: tournamentsProgramma,
                      textColor: Theme.of(context).backgroundColor,
                      
                    ),
                  ],
                )),
            color: Theme.of(context).accentColor),
      ),
    ),
    onWillPop: () => goBack(),
    );
  }
  goBack(){
    Navigator.of(context).pushNamed('pages');
  }
}
