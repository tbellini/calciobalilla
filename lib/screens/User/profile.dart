// import 'package:calciobalilla24/models/Avatar.dart';
// import 'package:calciobalilla24/widgets/custom_button.dart';
// import 'package:calciobalilla24/widgets/custom_container.dart';
// import 'package:calciobalilla24/widgets/custom_list_item.dart';
// import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
// import 'package:calciobalilla24/widgets/custom_table.dart';
// import 'package:flutter/material.dart';
// import 'package:mvc_pattern/mvc_pattern.dart';

// class Profile extends StatefulWidget {
//   @override
//   _ProfileState createState() => _ProfileState();
// }

// class _ProfileState extends StateMVC<Profile> {
//   @override
//   Widget build(BuildContext context) {
//     double fieldsFontSize = 15;
//     return Scaffold(
//       backgroundColor: Theme.of(context).backgroundColor,
//       body: CustomScrollPage(
//         title_bar: "PROFILO",
//         color_title_bar: Colors.white,
//         color_bar: Theme.of(context).accentColor,
//         custom_bar: Container(
//             padding: EdgeInsets.all(30),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Text(
//                       'Mario Rossi',
//                       style:
//                           TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
//                     ),
//                     Text(
//                       'Partire vinte: 10',
//                       style:
//                           TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
//                     ),
//                     Text(
//                       'Tornei giocati: 3',
//                       style:
//                           TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
//                     ),
//                     SizedBox(height: 10),
//                     ElevatedButton.icon(
//                       onPressed: () {
//                         Navigator.of(context).pushNamed('edit-profile');
//                       },
//                       label: Text(
//                         'Modifica Profilo',
//                         style: TextStyle(fontSize: 14),
//                       ),
//                       icon: Icon(Icons.edit_rounded),
//                       style: ElevatedButton.styleFrom(
//                         // background color
//                         primary: Colors.blue,
//                         shape: const StadiumBorder(),
//                         padding:
//                             EdgeInsets.symmetric(horizontal: 30, vertical: 15),
//                         textStyle: TextStyle(fontSize: 20),
//                       ),
//                     ),
//                     SizedBox(height: 30),
//                     Row(
//                       children: [
//                         Text('Username: ',
//                             style: TextStyle(
//                                 fontSize: fieldsFontSize, fontWeight: FontWeight.bold)),
//                         Text('Pippo', style: TextStyle(fontSize: fieldsFontSize))
//                       ],
//                     ),
//                     Row(
//                       children: [
//                         Text('Data di Nascita: ',
//                             style: TextStyle(
//                                 fontSize: fieldsFontSize, fontWeight: FontWeight.bold)),
//                         Text('24/08/1998',style: TextStyle(fontSize: fieldsFontSize)),
//                       ],
//                     ),
//                     Row(
//                       children: [
//                         Text('Sesso: ',
//                             style: TextStyle(
//                                 fontSize: fieldsFontSize, fontWeight: FontWeight.bold)),
//                         Text('M',style: TextStyle(fontSize: fieldsFontSize)),
//                       ],
//                     ),
//                     SizedBox(
//                       height: 15,
//                     ),
//                      Divider(
//                           color: Colors.black,
//                           thickness: 0.5,
//                         ),
//                     Column(
//                       children: [
                       
//                         Text('Lista Amici',
//                             style: TextStyle(
//                                 fontSize: 20, fontWeight: FontWeight.bold))
//                       ],
//                     )
//                   ],
//                 ),
//                 Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
//                   CircleAvatar(
//                     backgroundImage: NetworkImage(
//                         'https://img.pngio.com/avatar-icon-png-105-images-in-collection-page-3-avatarpng-512_512.png'),
//                     radius: MediaQuery.of(context).size.width / 6,
//                     foregroundColor: Colors.red,
//                   ),
//                 ]),
//               ],
//             )),
//         height: 1000,
//       ),
//     );
//   }
// }
