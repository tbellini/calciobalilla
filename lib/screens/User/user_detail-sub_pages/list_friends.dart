import 'package:calciobalilla24/controller/friends_controller.dart';
import 'package:calciobalilla24/models/Friend.dart';
import 'package:calciobalilla24/repositories/friend_repo.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_friend_list_tile.dart';
import 'package:calciobalilla24/widgets/custom_progress_indicator.dart';
import 'package:calciobalilla24/widgets/empty_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ListFriends extends StatefulWidget {
  final double? blankSpaceHeight;
  const ListFriends({Key? key, @required this.blankSpaceHeight}) : super(key: key);

  @override
  _ListFriendsState createState() => _ListFriendsState();
}

class _ListFriendsState extends StateMVC<ListFriends> {
  List<Friend> friendsList = [];
  FriendsController? _con;

  _ListFriendsState() : super(FriendsController()) {
    _con = controller as FriendsController;
  }

  @override
  void initState() {
    _con!.isLoading = true;
    _con!.getFriends().then((value) {
      friendsList = value;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _con!.isLoading
          ? CustomProgressIndicator(
              topSpace: 0,
            )
          : friendsList.isEmpty
              ? EmptyListWidget(
                  containerHeight: this.widget.blankSpaceHeight,
                  icon: FontAwesomeIcons.users,
                  title: "Non ci sono amici per il momento,\n trovane uno!",
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ListView.separated(
                      padding: EdgeInsets.only(top: 15, bottom: BOTTOM_BAR_HEIGHT),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: friendsList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Slidable(
                          actionPane: SlidableDrawerActionPane(),
                          actionExtentRatio: 0.25,
                          child: CustomFriendListTile(
                            index: index,
                            list: friendsList,
                          ),
                          actions: [
                            IconSlideAction(
                              caption: "Rimuovi",
                              color: Colors.red,
                              icon: FontAwesomeIcons.userTimes,
                              onTap: () {
                                String response = "";
                                removeFriend(friendsList[index].id).then((value) {
                                  response = value.toString();
                                  showDialog(
                                    context: context,
                                    builder: (_) => AlertDialog(
                                      title: Text(response),
                                      // content:
                                      //     Text(friendsList[index].firstname + " " + friendsList[index].lastname + "eliminato dalla lista degli amici."),
                                      actions: [
                                        TextButton(
                                            onPressed: () {
                                              _con!.getFriends().then((value) {
                                                friendsList = value;
                                              });
                                              Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              "Ok",
                                              style: TextStyle(fontWeight: FontWeight.w700),
                                            )),
                                      ],
                                    ),
                                  );
                                });
                              },
                            ),
                          ],
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) => Divider(),
                    ),
                  ],
                ),
    );
  }
}
