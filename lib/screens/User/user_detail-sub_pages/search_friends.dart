import 'package:calciobalilla24/controller/friends_controller.dart';
import 'package:calciobalilla24/models/Friend.dart';
import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/repositories/notification_repo.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_friend_list_tile.dart';
import 'package:calciobalilla24/widgets/custom_progress_indicator.dart';
import 'package:calciobalilla24/widgets/empty_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class SearchFriends extends StatefulWidget {
  final double? blankSpaceHeight;
  final User? myUser;
  const SearchFriends({Key? key, @required this.blankSpaceHeight, @required this.myUser}) : super(key: key);

  @override
  _SearchFriendsState createState() => _SearchFriendsState();
}

class _SearchFriendsState extends StateMVC<SearchFriends> {
  FriendsController? _con;
  TextEditingController searchController = new TextEditingController();
  ScrollController scrollController = ScrollController();
  _SearchFriendsState() : super(FriendsController()) {
    _con = controller as FriendsController?;
  }

  @override
  void initState() {
    print("search friends");
    super.initState();
    scrollController.addListener(_scrollListener);
  }

  void _scrollListener() {
    if (scrollController.offset >= scrollController.position.maxScrollExtent && !scrollController.position.outOfRange) {
      print("at the end of list");
      _con!.fetchNextList();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          if (_con!.isLoading)
            CustomProgressIndicator(
              topSpace: 0,
            ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: TextField(
              onChanged: (value) {
                _con!.isLoading = true;
                _con!.updateList(value);
                setState(() {});
              },
              style: TextStyle(color: Colors.black),
              controller: searchController,
              decoration: InputDecoration(hintText: "Cerca un amico", hintStyle: TextStyle(color: Colors.black)),
            ),
          ),
          SizedBox(
            height: this.widget.blankSpaceHeight,
            child: _con!.friendsList!.isEmpty
                ? EmptyListWidget(containerHeight: this.widget.blankSpaceHeight, icon: FontAwesomeIcons.searchPlus, title: "Nessun amico trovato.")
                : ListView.separated(
                    controller: scrollController,
                    padding: EdgeInsets.only(top: 15),
                    shrinkWrap: false,
                    itemCount: _con!.friendsList!.length,
                    // physics: NeverScrollableScrollPhysics(),
                    separatorBuilder: (BuildContext context, int index) => SizedBox(
                      height: 20,
                    ),
                    itemBuilder: (BuildContext context, int i) {
                      return Slidable(
                        enabled: true,
                        actionPane: SlidableDrawerActionPane(),
                        actionExtentRatio: 0.25,
                        child: CustomFriendListTile(
                          index: i,
                          list: _con!.friendsList!,
                        ),
                        actions: [
                          IconSlideAction(
                            closeOnTap: true,
                            caption: "Aggiungi",
                            color: Colors.green,
                            icon: FontAwesomeIcons.plus,
                            onTap: () {
                              showDialog(
                                context: context,
                                builder: (_) => AlertDialog(
                                  title: Text("Aggiungere agli amici?"),
                                  content: Text("Invierai una richiesta d'amicizia a " +
                                      _con!.friendsList![i].firstname +
                                      " " +
                                      _con!.friendsList![i].lastname),
                                  actions: [
                                    TextButton(
                                        onPressed: () {
                                          _con!.sendRequest(_con!.friendsList![i].id, context, this.widget.myUser!);
                                          searchController.clear();
                                          Navigator.of(context, rootNavigator: true).pop();
                                        },
                                        child: Text(
                                          "Si",
                                          style: TextStyle(fontWeight: FontWeight.w700),
                                        )),
                                    TextButton(
                                        onPressed: () {
                                          searchController.clear();
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          "No",
                                          style: TextStyle(fontWeight: FontWeight.w700),
                                        ))
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      );
                    },
                  ),
          ),
        ],
      ),
    );
  }
}
