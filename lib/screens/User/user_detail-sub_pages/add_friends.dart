import 'package:calciobalilla24/controller/friends_controller.dart';
import 'package:calciobalilla24/models/Friend.dart';
import 'package:calciobalilla24/models/FriendRequest.dart';
import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/repositories/friend_repo.dart';
import 'package:calciobalilla24/repositories/notification_repo.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_friend_list_tile.dart';
import 'package:calciobalilla24/widgets/custom_grid_tile.dart';
import 'package:calciobalilla24/widgets/custom_progress_indicator.dart';
import 'package:calciobalilla24/widgets/empty_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class AddFriends extends StatefulWidget {
  final double? blankSpaceHeight;
  final User? myUser;
  const AddFriends({Key? key, @required this.blankSpaceHeight, @required this.myUser}) : super(key: key);

  @override
  _AddFriendsState createState() => _AddFriendsState();
}

class _AddFriendsState extends StateMVC<AddFriends> {
  List<FriendRequest> requestsList = [];
  FriendsController? _con;

  _AddFriendsState() : super(FriendsController()) {
    _con = controller as FriendsController;
  }

  @override
  void initState() {
    print("add friends");
    _con!.isLoading = true;
    _con!.getFriendsRequestList().then((value) {
      requestsList = value;
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _con!.isLoading
          ? CustomProgressIndicator(
              topSpace: 0,
            )
          : requestsList.isEmpty
              ? EmptyListWidget(
                  containerHeight: this.widget.blankSpaceHeight,
                  icon: FontAwesomeIcons.userPlus,
                  title: "Nessuna richiesta d'amicizia.",
                )
              : Column(
                  children: [
                    ListView.separated(
                      padding: EdgeInsets.only(top: 15),
                      shrinkWrap: true,
                      itemCount: requestsList.length,
                      physics: NeverScrollableScrollPhysics(),
                      separatorBuilder: (BuildContext context, int index) => SizedBox(
                        height: 20,
                      ),
                      itemBuilder: (BuildContext context, int i) {
                        return Slidable(
                          actionPane: SlidableDrawerActionPane(),
                          actionExtentRatio: 0.25,
                          child: CustomFriendListTile(
                            index: i,
                            list: requestsList,
                          ),
                          actions: [
                            IconSlideAction(
                              caption: "Conferma",
                              color: Colors.green,
                              icon: FontAwesomeIcons.plus,
                              onTap: () {
                                sendNotification(
                                    "Richiesta d'amicizia accettata",
                                    "la richiesta d'amicizia inviata a " +
                                        this.widget.myUser!.firstname +
                                        " " +
                                        this.widget.myUser!.lastname +
                                        " è stata acccetta!",
                                    [requestsList[i].senderId],
                                );
                                responseRequest(1, requestsList[i].id!).then((value) {
                                  String response = value.toString();
                                  showDialog(
                                    context: context,
                                    builder: (_) => AlertDialog(
                                      title: Text("Stato richiesta: "),
                                      content: Text(response),
                                      actions: [
                                        TextButton(
                                            onPressed: () {
                                              _con!.getFriendsRequestList().then((value) {
                                                requestsList = value;
                                                setState(() {});
                                              });
                                              Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              "Ok",
                                              style: TextStyle(fontWeight: FontWeight.w700),
                                            )),
                                      ],
                                    ),
                                  );
                                });
                                setState(() {});
                              },
                            ),
                            IconSlideAction(
                              caption: "Rifiuta",
                              color: Colors.red,
                              icon: FontAwesomeIcons.times,
                              onTap: () {
                                showDialog(
                                  context: context,
                                  builder: (_) => AlertDialog(
                                    title: Text("Confermi di voler rifiutare?"),
                                    content: Text("Rifiuterai la richiesta di amicizia da parte di " +
                                        requestsList[i].firstname +
                                        " " +
                                        requestsList[i].lastname +
                                        "."),
                                    actions: [
                                      TextButton(
                                          onPressed: () {
                                            var response = responseRequest(2, requestsList[i].id!).then((value) {
                                              if (value != null) {
                                                _con!.getFriendsRequestList().then((value) {
                                                  requestsList = value;
                                                  setState(() {});
                                                });

                                                Navigator.of(context).pop();
                                              }
                                            });
                                          },
                                          child: Text(
                                            "Si",
                                            style: TextStyle(fontWeight: FontWeight.w700),
                                          )),
                                      TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: Text(
                                            "No",
                                            style: TextStyle(fontWeight: FontWeight.w700),
                                          )),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ],
                        );
                      },
                    ),
                  ],
                ),
    );
  }
}
