import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/EnrollTournament.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_grid_tile.dart';
import 'package:calciobalilla24/widgets/custom_list_tile.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:calciobalilla24/widgets/empty_list_widget.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmer_text.dart';
import 'package:calciobalilla24/widgets/tournament_detail_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shimmer/shimmer.dart';

class TournamentsPartecipations extends StatefulWidget {
  @override
  _TournamentsPartecipationsState createState() => _TournamentsPartecipationsState();
}

class _TournamentsPartecipationsState extends StateMVC<TournamentsPartecipations> {
  TournamentsController? _con;
  List<EnrollTournament> requestsList = [];

  _TournamentsPartecipationsState() : super(TournamentsController()) {
    _con = controller as TournamentsController;
  }

  @override
  void initState() {
    _con!.getTournamentRequestList().then((value) {
      setState(() {
        requestsList = value;
      });
    });
    super.initState();
  }

  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollPage(
        custom_bar: Container(
          padding: EdgeInsets.symmetric(vertical: 25, horizontal: 40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RichText(
                  text: TextSpan(text: "Richieste di ", style: TextStyle(fontSize: 25, fontWeight: FontWeight.w400, color: Colors.black), children: [
                TextSpan(
                  text: "\nPartecipazione",
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.w800, color: Colors.black),
                )
              ])),
              SizedBox(
                height: 15,
              ),
              CustomButton(
                onPressed: () {},
                text: "Notifiche",
                color: Theme.of(context).hintColor,
                horizontal: 10,
              )
            ],
          ),
        ),
        height: 270,
        custom_body: CustomContainer(
            backgroundColor: Theme.of(context).backgroundColor,
            child: Container(
              height: requestsList.length >= 3 ? null : MediaQuery.of(context).size.height / 1.6,
              margin: EdgeInsets.symmetric(horizontal: generalPadding - 10, vertical: generalPadding + 10),
              child: Column(children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                        text: TextSpan(
                            text: "Elenco richieste",
                            style: TextStyle(fontSize: 23, fontWeight: FontWeight.w400, color: Colors.black),
                            children: [
                          TextSpan(
                            text: "\npartecipazione torneo:",
                            style: TextStyle(fontSize: 24, fontWeight: FontWeight.w800, color: Colors.black),
                          )
                        ])),
                    SizedBox(
                      height: 10,
                    ),
                    _con!.isLoading
                        ? Container(
                            height: MediaQuery.of(context).size.height / 1.6 - 70,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          )
                        : requestsList.isEmpty
                            ? EmptyListWidget(
                                containerHeight: MediaQuery.of(context).size.height / 1.6 - 70,
                                icon: FontAwesomeIcons.userPlus,
                                title: "Nessuna Richiesta!")
                            : Container(
                                child: ListView.separated(
                                  scrollDirection: Axis.vertical,
                                  itemCount: requestsList.length,
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemBuilder: (BuildContext context, int index) {
                                    return CustomListTile(
                                      index,
                                      title: requestsList[index].tournament!.title,
                                      subtitle: "Richiesta da parte di: " + requestsList[index].playerOne.toString().toUpperCase(),
                                      onPressedTile: () {
                                        showModalBottomSheet<void>(
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(RADIUS_STANDARD_CONTAINER_BORDER)),
                                          context: context,
                                          builder: (BuildContext context) {
                                            return TournamentDetailWidget(
                                              tournament: requestsList[index].tournament,
                                              location: requestsList[index].location,
                                            );
                                          },
                                        );
                                      },
                                      trailingWidget: Container(
                                        width: 100,
                                        child: Row(
                                          children: [
                                            IconButton(
                                              onPressed: () {
                                                _con!
                                                    .replyRequest(
                                                        requestsList[index].id!, 1, requestsList[index].tournament!, requestsList[index].playerOneId)
                                                    .then((jsonData) {
                                                  getDialogListRequest(jsonData);
                                                });
                                              },
                                              icon: Icon(
                                                Icons.thumb_up,
                                                color: Colors.green,
                                              ),
                                            ),
                                            IconButton(
                                                onPressed: () {
                                                  _con!
                                                      .replyRequest(requestsList[index].id!, 2, requestsList[index].tournament!,
                                                          requestsList[index].playerOneId)
                                                      .then((jsonData) {
                                                    getDialogListRequest(jsonData);
                                                  });
                                                },
                                                icon: Icon(
                                                  Icons.thumb_down,
                                                  color: Colors.red,
                                                ))
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                  separatorBuilder: (BuildContext context, int index) => Divider(
                                    color: Theme.of(context).hoverColor,
                                  ),
                                ),
                              ),
                  ],
                ),
              ]),
            ),
            color: PRIMARY_LIGHT_COLOR),
        title_bar: "tornei",
        color_title_bar: Theme.of(context).hoverColor,
        color_bar: SECONDARY_DARK_COLOR,
      ),
    );
  }

  getDialogListRequest(String jsonData) {
    return showDialog(
        context: context,
        builder: (_) => AlertDialog(
              title: Text("Stato richiesta: "),
              content: Text(jsonData),
              actions: [
                TextButton(
                    onPressed: () {
                      _con!.getTournamentRequestList().then((value) {
                        Navigator.of(context).pop();
                        setState(() {
                          requestsList = value;
                        });
                      });
                    },
                    child: Text("Ok"))
              ],
            ));
  }
}
