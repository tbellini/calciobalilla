import 'package:calciobalilla24/controller/tournaments_controller.dart';
import 'package:calciobalilla24/models/Avatar.dart';
import 'package:calciobalilla24/models/EnrollTournament.dart';
import 'package:calciobalilla24/models/TournamentInfo.dart';
import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/repositories/user_repo.dart';
import 'package:calciobalilla24/screens/Tournament/tournament_paginator.dart';
import 'package:calciobalilla24/screens/Tournament/user/tournament_dashboard.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_list_container.dart';
import 'package:calciobalilla24/widgets/custom_list_item.dart';
import 'package:calciobalilla24/widgets/custom_list_tile.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:calciobalilla24/widgets/custom_table.dart';
import 'package:calciobalilla24/widgets/empty_card_widget.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmerCircle.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmer_text.dart';
import 'package:calciobalilla24/widgets/tournament_detail_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends StateMVC<Dashboard> {
  User myUser = User();
  TournamentsController? _con;
  List<EnrollTournament> scheduledTournament = [];
  List<EnrollTournament> endedTournament = [];
  List<EnrollTournament> deletedTournament = [];
  List<EnrollTournament> waitingTournament = [];

  int itemCount = 4;
  int? itemCountScheduled, itemCountEnded, itemCountWaiting;

  @override
  void initState() {
    setTournamnetEnrollList();

    checkInternet(context);
    getMyUser().then((value) {
      setState(() {
        myUser = value;
      });
    });
    super.initState();
  }

  setTournamnetEnrollList() {
    //AGGIUNGERE WAITING -------- DA FARE
    _con!.getEnrollWaiting().then((value) {
      waitingTournament = value;
      if (value.length < itemCount) {
        itemCountWaiting = value.length;
      } else {
        itemCountWaiting = itemCount;
      }
    });
    _con!.getEnrollScheduled().then((value) {
      scheduledTournament = value;
      if (value.length < itemCount) {
        itemCountScheduled = value.length;
      } else {
        itemCountScheduled = itemCount;
      }
      setState(() {});
    });
    _con!.getEnrollEnded().then((value) {
      endedTournament = value;
      if (value.length < itemCount) {
        itemCountEnded = value.length;
      } else {
        itemCountEnded = itemCount;
      }
      setState(() {});
    });
    _con!.getEnrollDeleted().then((value) {
      deletedTournament = value;
      setState(() {});
    });
  }

  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  _DashboardState() : super(TournamentsController()) {
    _con = controller as TournamentsController;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: CustomScrollPage(
        iconRightTap: () async {
          SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
          sharedPreferences.clear();
          Navigator.of(context).pushReplacementNamed('login');
        },
        icon_right_bar: Icons.logout,
        title_bar: "HOME",
        color_title_bar: Theme.of(context).hoverColor,
        color_bar: Theme.of(context).hintColor,
        custom_bar: Container(
            padding: EdgeInsets.all(generalPadding),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                myUser.gender == ""
                    ? Expanded(
                        child: Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            enabled: true,
                            child: ShimmerCircle(radius: MediaQuery.of(context).size.width / 6)),
                      )
                    : CircleAvatar(
                        backgroundImage: myUser.gender == 'm' ? NetworkImage(URL_AVATAR_M) : NetworkImage(URL_AVATAR_F),
                        radius: MediaQuery.of(context).size.width / 6,
                        backgroundColor: PRIMARY_LIGHT_COLOR,
                      ),
                myUser.firstname == ""
                    ? Expanded(
                        child: Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            enabled: true,
                            child: ShimmerText(
                              2,
                              mainAxis: MainAxisAlignment.center,
                              padding: EdgeInsets.only(right: 15),
                            )),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            myUser.firstname + " " + myUser.lastname,
                            style: TextStyle(fontSize: MAIN_TITLE_SIZE, fontWeight: MAIN_TITLE_WEIGHT),
                          ),
                          Text(
                            format.format(myUser.birthday),
                            style: TextStyle(fontSize: DESCRIPTION_SIZE, fontWeight: DESCRIPTION_WEIGHT),
                          ),
                          Text(
                            myUser.phoneNumber,
                            style: TextStyle(fontSize: DESCRIPTION_SIZE, fontWeight: DESCRIPTION_WEIGHT),
                          ),
                        ],
                      ),
              ],
            )),
        custom_body: CustomContainer(
            child: Container(
                margin: EdgeInsets.only(bottom: generalPadding * 2, left: generalPadding - 10, right: generalPadding - 10, top: generalPadding + 15),
                child: Column(
                  children: [
                    _con!.isLoading
                        ? Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            enabled: true,
                            child: ShimmerText(
                              0,
                              mainAxis: MainAxisAlignment.center,
                              padding: EdgeInsets.all(0),
                              borderRadius: 20,
                              mainHeight: 250,
                            ))
                        : scheduledTournament.isEmpty
                            ? EmptyCardWidget(
                                height: 350,
                                titleCard: "Tornei in programma",
                                titleWidget: "Nessun torneo, trovano uno!",
                                firstColor: Colors.grey[100],
                                secondaryColor: Colors.grey[400],
                                textColorEmptyListWidget: Colors.grey,
                                textColor: Colors.black54,
                                iconColor: PRIMARY_LIGHT_COLOR,
                                color: PRIMARY_LIGHT_COLOR,
                                icon: FontAwesomeIcons.calendarPlus,
                              )
                            : CustomListContainer(
                                listLength: scheduledTournament.length,
                                color: PRIMARY_LIGHT_COLOR,
                                textColor: Colors.black54,
                                title: "Tornei in programma",
                                subtitle: "Tieni premuto per eliminare l'iscrizione",
                                listWidget: Padding(
                                  padding: EdgeInsets.symmetric(vertical: 15),
                                  child: ListView.separated(
                                    padding: EdgeInsets.only(top: 5),
                                    shrinkWrap: true,
                                    physics: const NeverScrollableScrollPhysics(),
                                    itemCount: itemCountScheduled!,
                                    separatorBuilder: (BuildContext context, int index) => SizedBox(
                                      height: 12,
                                    ),
                                    itemBuilder: (BuildContext context, int index) {
                                      return CustomListTile(
                                        index,
                                        onPressedTile: () {
                                          Navigator.pushReplacement(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) => TournamentDashboard(
                                                id: scheduledTournament[index].tournamentId,
                                              ),
                                            ),
                                          );
                                        },
                                        title: scheduledTournament[index].tournament!.title,
                                        subtitle: formatHour.format(scheduledTournament[index].tournament!.dateScheduled),
                                        urlImage: scheduledTournament[index].location!.imageCover,
                                        onLongPressedButton: () {
                                          showDialog(
                                            context: context,
                                            builder: (_) => AlertDialog(
                                              title: Text("Eliminare iscrizione torneo?"),
                                              content: RichText(
                                                text: TextSpan(
                                                  text: "Procedere con l'eliminazione della squadra dal torneo: ",
                                                  style: TextStyle(color: Colors.black),
                                                  children: [
                                                    TextSpan(
                                                      text: scheduledTournament[index].tournament!.title,
                                                      style: TextStyle(fontWeight: FontWeight.bold),
                                                    ),
                                                    TextSpan(
                                                      text: "?",
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              actions: [
                                                TextButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                      _con!.deleteEnroll(scheduledTournament[index].id!).then((value) {
                                                        setTournamnetEnrollList();
                                                      });
                                                    },
                                                    child: Text("Si"))
                                              ],
                                            ),
                                          );
                                        },
                                        radius: 25,
                                        textButton: "Dettaglio",
                                        onPressedButton: () {
                                          showModalBottomSheet<void>(
                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(RADIUS_STANDARD_CONTAINER_BORDER)),
                                            context: context,
                                            builder: (BuildContext context) {
                                              return TournamentDetailWidget(
                                                tournament: scheduledTournament[index].tournament,
                                                location: scheduledTournament[index].location,
                                              );
                                            },
                                          );
                                        },
                                      );
                                    },
                                  ),
                                ),
                                onPressedIcon: () {
                                  Navigator.pushReplacement(
                                      context, MaterialPageRoute(builder: (context) => TournamentPaginator(enrollList: scheduledTournament)));
                                },
                              ),
                    SizedBox(
                      height: 20,
                    ),
                    _con!.isLoading
                        ? Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            enabled: true,
                            child: ShimmerText(
                              0,
                              mainAxis: MainAxisAlignment.center,
                              padding: EdgeInsets.all(0),
                              borderRadius: 20,
                              mainHeight: 250,
                            ))
                        : waitingTournament.isEmpty
                            ? EmptyCardWidget(
                                height: 350,
                                titleCard: "Tornei in attesa",
                                titleWidget: "Nessun torneo, trovano uno!",
                                firstColor: Colors.grey[100],
                                secondaryColor: Colors.grey[400],
                                textColorEmptyListWidget: Colors.grey,
                                textColor: Colors.black54,
                                iconColor: PRIMARY_LIGHT_COLOR,
                                color: PRIMARY_LIGHT_COLOR,
                                icon: FontAwesomeIcons.pauseCircle,
                              )
                            : CustomListContainer(
                                listLength: scheduledTournament.length,
                                color: PRIMARY_LIGHT_COLOR,
                                textColor: Colors.black54,
                                title: "Tornei in attesa",
                                subtitle: "Tieni premuto per eliminare l'iscrizione",
                                listWidget: Padding(
                                  padding: EdgeInsets.symmetric(vertical: 15),
                                  child: ListView.separated(
                                    padding: EdgeInsets.only(top: 5),
                                    shrinkWrap: true,
                                    physics: const NeverScrollableScrollPhysics(),
                                    itemCount: itemCountWaiting!,
                                    separatorBuilder: (BuildContext context, int index) => SizedBox(
                                      height: 12,
                                    ),
                                    itemBuilder: (BuildContext context, int index) {
                                      return CustomListTile(
                                        index,
                                        onPressedTile: () {
                                          Navigator.pushReplacement(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) => TournamentDashboard(
                                                id: waitingTournament[index].tournamentId,
                                              ),
                                            ),
                                          );
                                        },
                                        title: waitingTournament[index].tournament!.title,
                                        subtitle: formatHour.format(waitingTournament[index].tournament!.dateScheduled),
                                        urlImage: waitingTournament[index].location!.imageCover,
                                        onLongPressedButton: () {
                                          showDialog(
                                            context: context,
                                            builder: (_) => AlertDialog(
                                              title: Text("Eliminare iscrizione torneo?"),
                                              content: RichText(
                                                text: TextSpan(
                                                  text: "Procedere con l'eliminazione della squadra dal torneo: ",
                                                  style: TextStyle(color: Colors.black),
                                                  children: [
                                                    TextSpan(
                                                      text: waitingTournament[index].tournament!.title,
                                                      style: TextStyle(fontWeight: FontWeight.bold),
                                                    ),
                                                    TextSpan(
                                                      text: "?",
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              actions: [
                                                TextButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                      _con!.deleteEnroll(waitingTournament[index].id!).then((value) {
                                                        setTournamnetEnrollList();
                                                      });
                                                    },
                                                    child: Text("Si"))
                                              ],
                                            ),
                                          );
                                        },
                                        radius: 25,
                                        textButton: "Dettaglio",
                                        onPressedButton: () {
                                          showModalBottomSheet<void>(
                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(RADIUS_STANDARD_CONTAINER_BORDER)),
                                            context: context,
                                            builder: (BuildContext context) {
                                              return TournamentDetailWidget(
                                                tournament: waitingTournament[index].tournament,
                                                location: waitingTournament[index].location,
                                              );
                                            },
                                          );
                                        },
                                      );
                                    },
                                  ),
                                ),
                                onPressedIcon: () {
                                  Navigator.pushReplacement(
                                      context, MaterialPageRoute(builder: (context) => TournamentPaginator(enrollList: waitingTournament)));
                                },
                              ),
                    SizedBox(
                      height: 20,
                    ),
                    _con!.isLoading
                        ? Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            enabled: true,
                            child: ShimmerText(
                              0,
                              mainAxis: MainAxisAlignment.center,
                              padding: EdgeInsets.all(0),
                              borderRadius: 20,
                              mainHeight: 250,
                            ))
                        : endedTournament.isEmpty
                            ? EmptyCardWidget(
                                height: 350,
                                titleCard: "Tornei terminati",
                                titleWidget: "Nessun torneo terminato.",
                                firstColor: Colors.grey[100],
                                secondaryColor: Colors.grey[400],
                                textColorEmptyListWidget: Theme.of(context).secondaryHeaderColor,
                                textColor: Colors.black54,
                                iconColor: PRIMARY_LIGHT_COLOR,
                                color: PRIMARY_LIGHT_COLOR,
                                icon: FontAwesomeIcons.calendarCheck,
                              )
                            : CustomListContainer(
                                listLength: endedTournament.length,
                                color: PRIMARY_LIGHT_COLOR,
                                textColor: Colors.black54,
                                title: "Tornei terminati",
                                listWidget: Padding(
                                    padding: EdgeInsets.symmetric(vertical: 15),
                                    child: ListView.separated(
                                      padding: EdgeInsets.only(top: 5),
                                      shrinkWrap: true,
                                      physics: const NeverScrollableScrollPhysics(),
                                      itemCount: itemCountEnded!,
                                      separatorBuilder: (BuildContext context, int index) => SizedBox(
                                        height: 12,
                                      ),
                                      itemBuilder: (BuildContext context, int index) {
                                        return CustomListTile(
                                          index,
                                          title: endedTournament[index].tournament!.title,
                                          subtitle: formatHour.format(endedTournament[index].tournament!.dateScheduled),
                                          urlImage: endedTournament[index].location!.imageCover,
                                          onLongPressedButton: () {},
                                          radius: 30,
                                          textButton: "Dettaglio",
                                          onPressedButton: () {
                                            showModalBottomSheet<void>(
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(RADIUS_STANDARD_CONTAINER_BORDER)),
                                              context: context,
                                              builder: (BuildContext context) {
                                                return TournamentDetailWidget(
                                                  tournament: endedTournament[index].tournament,
                                                  location: endedTournament[index].location,
                                                );
                                              },
                                            );
                                          },
                                        );
                                      },
                                    )),
                              ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                )),
            color: Theme.of(context).secondaryHeaderColor),
        height: STANDARD_HEAD_HEIGHT,
      ),
    );
  }
}
