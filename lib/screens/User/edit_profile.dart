import 'package:calciobalilla24/controller/auth_controller.dart';
import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/repositories/user_repo.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:calciobalilla24/widgets/pages.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmerCircle.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmer_text.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shimmer/shimmer.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends StateMVC<EditProfile> {
  bool _obscureText = true;
  AuthController? _con;
  final _key = GlobalKey<FormState>();
  TextEditingController nameCon = new TextEditingController();
  TextEditingController surnameCon = new TextEditingController();
  TextEditingController dateCon = new TextEditingController();
  TextEditingController emailCon = new TextEditingController();
  TextEditingController phoneCon = new TextEditingController();
  User userEdited = new User();
  User currentUser = new User();
  String dropGender = "";
  bool isSexSelected = false;
  _EditProfileState() : super(AuthController()) {
    _con = controller as AuthController?;
  }

  @override
  void initState() {
    getMyUser().then((value) {
      setState(() {
        currentUser = value;
      });
      nameCon.text = value.firstname;
      surnameCon.text = value.lastname;
      dateCon.text = format.format(value.birthday);
      emailCon.text = value.email;
      phoneCon.text = value.phoneNumber;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: CustomScrollPage(
          iconLeftTap: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => PagesBottomTabbar(
                  currentTab: 3,
                ),
              ),
            );
          },
          icon_left_bar: Icons.arrow_back,
          custom_bar: Container(
            padding: EdgeInsets.all(generalPadding),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                
   
                  currentUser.gender == ""
                    ? Expanded(
                        child: Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            enabled: true,
                            child: ShimmerCircle(radius: MediaQuery.of(context).size.width / 6)),
                      )
                    : CircleAvatar(
                        backgroundImage: currentUser.gender == 'm' ? NetworkImage(URL_AVATAR_M) : NetworkImage(URL_AVATAR_F),
                        radius: MediaQuery.of(context).size.width / 6,
                        backgroundColor: PRIMARY_LIGHT_COLOR,
                      ),



                        currentUser.firstname == ""
                    ? Expanded(
                        child: Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            enabled: true,
                            child: ShimmerText(
                              2,
                              mainAxis: MainAxisAlignment.center,
                              padding: EdgeInsets.only(right: 15),
                            )),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            currentUser.firstname + " " + currentUser.lastname,
                            style: TextStyle(fontSize: MAIN_TITLE_SIZE, fontWeight: MAIN_TITLE_WEIGHT),
                          ),
                          Text(
                            format.format(currentUser.birthday),
                            style: TextStyle(fontSize: DESCRIPTION_SIZE, fontWeight: DESCRIPTION_WEIGHT),
                          ),
                          Text(
                            currentUser.phoneNumber,
                            style: TextStyle(fontSize: DESCRIPTION_SIZE, fontWeight: DESCRIPTION_WEIGHT),
                          ),
                        ],
                      ),
            
              ],
            ),
          ),
          backArrow: true,
          custom_body: CustomContainer(
              child: Container(
                margin: EdgeInsets.all(generalPadding + 20),
                child: Form(
                  key: _key,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Modifica i dati",
                            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          txtEmail("Email", Colors.white60),
                          SizedBox(
                            height: 20,
                          ),
                          txtFirstname("Nome", Theme.of(context).hoverColor),
                          SizedBox(
                            height: 20,
                          ),
                          txtLastname("Cognome", Theme.of(context).hoverColor),
                          SizedBox(
                            height: 20,
                          ),
                          txtGender("Genere", Theme.of(context).hoverColor),
                          SizedBox(
                            height: 20,
                          ),
                          txtBirthday("Data di nascita", Theme.of(context).hoverColor, icon: Icons.person_outline),
                          SizedBox(
                            height: 20,
                          ),
                          txtPhone("Cellulare", Theme.of(context).hoverColor),
                          SizedBox(
                            height: 30,
                          ),
                        ],
                      ),
                      CustomButton(
                          onPressed: () {
                            if (_key.currentState!.validate()) {
                              _key.currentState!.save();
                              setState(() {
                                _con!.isLoading = true;
                              });
                              _con!.updateProfile(userEdited, context);
                            }
                          },
                          text: 'Conferma',
                          color: Theme.of(context).hintColor)
                    ],
                  ),
                ),
              ),
              color: Theme.of(context).accentColor),
          height: STANDARD_HEAD_HEIGHT,
          title_bar: "Modifica profilo",
          color_bar: SECONDARY_DARK_COLOR,
          color_title_bar: Theme.of(context).backgroundColor,
        ),
      ),
      onWillPop: () => goBack(),
    );
  }

  goBack() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => PagesBottomTabbar(
          currentTab: 3,
        ),
        
      ),
    );
  }

  TextFormField txtEmail(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      enabled: false,
      onSaved: (value) => userEdited.email = value!.split(' ')[0],
      validator: (value) {
        if (!value!.contains("@") || value.length < 3) {
          return 'inserire un mail valida';
        } else
          return null;
      },
      controller: emailCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtFirstname(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => userEdited.firstname = value!,
      validator: (value) => value!.length < 3 ? "Inserisci un nome valido." : null,
      controller: nameCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtLastname(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => userEdited.lastname = value!,
      validator: (value) => value!.length < 3 ? "Inserisci un cognome valido." : null,
      controller: surnameCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  TextFormField txtPhone(String title, Color mainColor, {IconData? icon}) {
    return TextFormField(
      onSaved: (value) => userEdited.phoneNumber = value!,
      validator: (value) {
        if (value!.length > 2 && !NUMBERS_VALIDATOR.hasMatch(value)) {
          return "Inserisci un numero valido.";
        } else {
          return null;
        }
      },
      controller: phoneCon,
      style: TextStyle(color: mainColor),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: mainColor),
      ),
    );
  }

  DropdownButtonFormField txtGender(String title, Color color, {IconData? icon}) {
    return new DropdownButtonFormField<String>(
      // validator: (value) => value == null ? "Seleziona un genere." : null,
      onSaved: (newValue) {
        if (isSexSelected) {
          userEdited.gender = getElementFromJson(genderList, 'name', 'code', newValue!);
        } else {
          userEdited.gender = currentUser.gender;
        }
        setState(() {});
      },
      hint: Text(
        currentUser.gender == "" ? title : getElementFromJson(genderList, 'code', 'name', currentUser.gender),
        style: TextStyle(color: color),
      ),
      dropdownColor: SECONDARY_DARK_COLOR,
      isExpanded: true,
      items: getListFromJson(genderList, 'name').map((String value) {
        return new DropdownMenuItem<String>(
          value: value,
          child: new Text(
            value,
            style: TextStyle(
              color: Theme.of(context).hoverColor,
            ),
          ),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          dropGender = newValue!;
          isSexSelected = true;
        });
      },
    );
  }

  DateTimeField txtBirthday(String title, Color mainColor, {IconData? icon, Locale? locale}) {
    return DateTimeField(
      onSaved: (newValue) => userEdited.birthday = newValue!,
      cursorColor: mainColor,
      controller: dateCon,
      validator: (value) => value == null ? "Inserisci una data valida." : null,
      onShowPicker: (context, currentValue) {
        return showDatePicker(
            context: context,
            locale: locale == null ? const Locale('it', 'IT') : locale,
            initialDate: currentValue ?? DateTime.now(),
            firstDate: DateTime(1940),
            lastDate: DateTime(2023));
      },
      style: TextStyle(color: mainColor),
      format: format,
      decoration: InputDecoration(
        labelText: title,
        labelStyle: TextStyle(color: mainColor),
        contentPadding: EdgeInsets.all(12),
        hintText: "1/1/1998",
        hintStyle: TextStyle(color: mainColor),
        prefixIcon: Icon(icon, color: mainColor),
        // border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
        // focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.5))),
        // enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
      ),
    );
  }
}
