import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/repositories/user_repo.dart';
import 'package:calciobalilla24/screens/User/user_detail-sub_pages/add_friends.dart';
import 'package:calciobalilla24/screens/User/user_detail-sub_pages/list_friends.dart';
import 'package:calciobalilla24/screens/User/user_detail-sub_pages/search_friends.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:calciobalilla24/widgets/custom_scroll_page.dart';
import 'package:calciobalilla24/widgets/internal_navigation_button.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmerCircle.dart';
import 'package:calciobalilla24/widgets/shimmer_widgets/shimmer_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shimmer/shimmer.dart';

class UserDetail extends StatefulWidget {
  @override
  _UserDetailState createState() => _UserDetailState();
}

class _UserDetailState extends StateMVC<UserDetail> {
  User myUser = User();
  int? tabSelected;
  double standardHeight = 350;

  List usersList = [
    {'name': 'Andrea', 'date_birth': '2021-08-01'},
    {'name': 'Mario', 'date_birth': '2021-08-01'},
    {'name': 'Luca', 'date_birth': '2021-08-01'},
    {'name': 'Ciro', 'date_birth': '2021-08-01'},
    {'name': 'Andrea', 'date_birth': '2021-08-01'},
    {'name': 'Mario', 'date_birth': '2021-08-01'},
    {'name': 'Luca', 'date_birth': '2021-08-01'},
    {'name': 'Ciro', 'date_birth': '2021-08-01'},
    {'name': 'Andrea', 'date_birth': '2021-08-01'},
    {'name': 'Mario', 'date_birth': '2021-08-01'},
    {'name': 'Luca', 'date_birth': '2021-08-01'},
    {'name': 'Ciro', 'date_birth': '2021-08-01'}
  ];

  @override
  void initState() {
    tabSelected = 0;
    getMyUser().then((value) {
      setState(() {
        myUser = value;
      });
    });
    super.initState();
  }

  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: CustomScrollPage(
          title_bar: "profilo".toUpperCase(),
          color_title_bar: Colors.white,
          color_bar: SECONDARY_DARK_COLOR,
          height: standardHeight,
          custom_bar: CustomContainer(
            color: Theme.of(context).backgroundColor,
            child: Container(
              padding: EdgeInsets.all(generalPadding),
              child: Column(children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    myUser.gender == ""
                        ? Expanded(
                            child: Shimmer.fromColors(
                                baseColor: Colors.grey[300]!,
                                highlightColor: Colors.grey[100]!,
                                enabled: true,
                                child: ShimmerCircle(radius: MediaQuery.of(context).size.width / 6)),
                          )
                        : CircleAvatar(
                            backgroundImage: myUser.gender == 'm' ? NetworkImage(URL_AVATAR_M) : NetworkImage(URL_AVATAR_F),
                            radius: MediaQuery.of(context).size.width / 6,
                            backgroundColor: PRIMARY_LIGHT_COLOR,
                          ),
                    myUser.firstname == ""
                        ? Expanded(
                            child: Shimmer.fromColors(
                                baseColor: Colors.grey[300]!,
                                highlightColor: Colors.grey[100]!,
                                enabled: true,
                                child: ShimmerText(
                                  2,
                                  padding: EdgeInsets.only(left: 15),
                                )),
                          )
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                myUser.firstname + " " + myUser.lastname,
                                style: TextStyle(fontSize: MAIN_TITLE_SIZE, fontWeight: MAIN_TITLE_WEIGHT),
                                maxLines: 2,
                              ),
                              Text(
                                format.format(myUser.birthday),
                                style: TextStyle(fontSize: DESCRIPTION_SIZE, fontWeight: DESCRIPTION_WEIGHT),
                              ),
                              Text(
                                myUser.phoneNumber,
                                style: TextStyle(fontSize: DESCRIPTION_SIZE, fontWeight: DESCRIPTION_WEIGHT),
                              ),
                              // SizedBox(
                              //   height: 12,
                              // ),
                            ],
                          ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                CustomButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed('edit-profile');
                    },
                    width: 400,
                    text: "Modifica profilo",
                    color: Theme.of(context).hintColor)
              ]),
            ),
          ),
          custom_body: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                      child: InternalNavigationButton(
                    tabNumber: 0,
                    title: "Amici",
                    tabSelected: tabSelected,
                    onPressed: () {
                      setState(() {
                        tabSelected = 0;
                      });
                    },
                  )),
                  Expanded(
                      child: InternalNavigationButton(
                    tabNumber: 1,
                    title: "Cerca",
                    tabSelected: tabSelected,
                    onPressed: () {
                      setState(() {
                        tabSelected = 1;
                      });
                    },
                  )),
                  Expanded(
                      child: InternalNavigationButton(
                    tabNumber: 2,
                    title: "Richieste",
                    tabSelected: tabSelected,
                    onPressed: () {
                      setState(() {
                        tabSelected = 2;
                      });
                    },
                  )),
                ],
              ),
              getNavigationWidget(tabSelected),
            ],
          ),
        ));
  }

  Widget getNavigationWidget(int? tabSelected) {
    switch (tabSelected!) {
      case 0:
        return ListFriends(
          blankSpaceHeight: (MediaQuery.of(context).size.height - (standardHeight + APP_BAR_HEIGHT + BOTTOM_BAR_HEIGHT)),
        );
      case 1:
        return SearchFriends(
          //54  = height navbar button = 20(padding) + 16(default text size)
          blankSpaceHeight: (MediaQuery.of(context).size.height - (standardHeight + APP_BAR_HEIGHT + 56 + BOTTOM_BAR_HEIGHT)),
          myUser: myUser,
        );
      case 2:
        return AddFriends(
          blankSpaceHeight: (MediaQuery.of(context).size.height - (standardHeight + APP_BAR_HEIGHT + BOTTOM_BAR_HEIGHT)),
          myUser: myUser,
        );
      default:
        return ListFriends(
          blankSpaceHeight: (MediaQuery.of(context).size.height - (standardHeight + APP_BAR_HEIGHT + BOTTOM_BAR_HEIGHT)),
        );
    }
  }
}
