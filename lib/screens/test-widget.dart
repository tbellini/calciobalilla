import 'package:calciobalilla24/widgets/custom_button.dart';
import 'package:calciobalilla24/widgets/custom_container.dart';
import 'package:flutter/material.dart';

class ShowWidgetPage extends StatefulWidget {
  @override
  _ShowWidgetPageState createState() => _ShowWidgetPageState();
}

class _ShowWidgetPageState extends State<ShowWidgetPage> with TickerProviderStateMixin {

  late AnimationController controller;

  @override
  void initState() {
    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    )..addListener(() {
        setState(() {});
      });
    controller.repeat(reverse: false);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     
        body:Container(

        
        ),
    );
  }
}
