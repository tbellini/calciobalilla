import 'dart:convert';

import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<User> getMyUser() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String? id = sharedPreferences.getString('id');
  User myUser = User();
  List<User> users = [];
  var response = await authenticatedGet(API_GET_USER(id!));
  var jsonData;
  if (response.statusCode >= 200 && response.statusCode < 300) {
    jsonData = jsonDecode(response.body);
    users = myUser.fromMapList(jsonData);
  } else {
    jsonData = jsonDecode(response.body);
    print(jsonData);
  }
  return users.first;
}


Future<User> getUserFromID(String userId) async {
  User myUser = User();
  List<User> users = [];
  var response = await authenticatedGet(API_GET_USER(userId));
  var jsonData;
  if (response.statusCode >= 200 && response.statusCode < 300) {
    jsonData = jsonDecode(response.body);
    users = myUser.fromMapList(jsonData);
  } else {
    jsonData = jsonDecode(response.body);
    print(jsonData);
  }
  return users.first;
}


