import 'package:calciobalilla24/models/Location.dart';
import 'package:flutter/cupertino.dart';

Location? getLocationFromList(int id, List<Location> locationList) {
  for (Location location in locationList) {
    if (location.id == id) {
      return location;
    }
  }
}
