import 'dart:convert';
import 'package:calciobalilla24/models/Notification.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';

sendNotification(String title, String body, List<String> idRecivers) async {
  Notification newNotification = new Notification();
  newNotification.title = title;
  newNotification.body = body;
  newNotification.idUsers = idRecivers;
  var response = await authenticatedPost(API_SEND_NOTIFICATION, newNotification.toMap());
  if (response.statusCode >= 200 && response.statusCode < 300) {
    return jsonDecode(response.body);
  }
}
