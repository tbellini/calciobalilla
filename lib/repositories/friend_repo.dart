import 'dart:convert';

import 'package:calciobalilla24/models/Friend.dart';
import 'package:calciobalilla24/models/FriendRequest.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<List<Friend>> searchFriends(String keyward, int pagesNumber, int skipPagesNumber) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String? id = sharedPreferences.getString('id');
  List<Friend> firendsList = [];
  // Friend friend = new Friend();
  //keyward = name or email
  Map searchModel = {
    "idUser": id!,
    "keyword": keyward,
  };
  var response = await authenticatedPost(API_SEARCH_FRIENDS(pagesNumber, skipPagesNumber), searchModel);
  var jsonData;

  if (response.statusCode >= 200 && response.statusCode < 300) {
    jsonData = jsonDecode(response.body);
    print(jsonData);
    firendsList = Friend().fromMapList(jsonData);

    return firendsList;
  } else
    return firendsList;
}

sendFriendRequest(String idReceiver) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String? idSender = sharedPreferences.getString('id');
  Map friendRequestMap = {"idSender": idSender, "idReceiver": idReceiver};
  var response = await authenticatedPost(API_SEND_REQUEST_FRIEND, friendRequestMap);
  return response;
}

getFriendsRequestList() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String? idUser = sharedPreferences.getString('id');
  List<FriendRequest> requestsList = [];
  var response = await authenticatedGet(API_GET_REQUESTS_LIST(idUser!));
  var jsonData;
  if (response.statusCode >= 200 && response.statusCode < 300) {
    jsonData = jsonDecode(response.body);
    requestsList = FriendRequest().fromMapList(jsonData);
    return requestsList;
  } else {
    return requestsList;
  }
}

Future<List<Friend>> getFriends() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String? id = sharedPreferences.getString('id');
  List<Friend> localFriendsList = [];
  var response = await authenticatedGet(API_GET_FRIENDS(id!));
  var jsonData;
  if (response.statusCode >= 200 && response.statusCode < 300) {
    jsonData = jsonDecode(response.body);
    localFriendsList = Friend().fromMapList(jsonData);
    return localFriendsList;
  } else
    return localFriendsList;
}

responseRequest(int state, int idRequest) async {
  var map = Map<String, dynamic>();
  map["idRequest"] = idRequest.toString();
  map["state"] = state.toString();

  var response = await authenticatedPut(API_RESPONSE_REQUEST_FRIEND, map);
  var jsonData = jsonDecode(response.body);
  return jsonData;
}

removeFriend(String idReceiver) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String? idSender = sharedPreferences.getString('id');

  Map removeFriendMap = {"idSender": idSender, "idReceiver": idReceiver};
  var response = await authenticatedPost(API_REMOVE_FRIEND, removeFriendMap);
  var jsonData = jsonDecode(response.body);
  return jsonData;
}
