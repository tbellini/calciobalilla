import 'dart:convert';
import 'package:calciobalilla24/models/EnrollTournament.dart';
import 'package:calciobalilla24/models/Friend.dart';
import 'package:calciobalilla24/models/Location.dart';
import 'package:calciobalilla24/models/LocationInfo.dart';
import 'package:calciobalilla24/models/Suggestion.dart';
import 'package:calciobalilla24/models/TeamsPartecipant.dart';
import 'package:calciobalilla24/models/Tournament.dart';
import 'package:calciobalilla24/models/TournamentInfo.dart';
import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/repositories/notification_repo.dart';
import 'package:calciobalilla24/repositories/user_repo.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/default_error_dialog.dart';
import 'package:calciobalilla24/widgets/pages.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TournamentsController extends ControllerMVC {
  bool isLoading = false;
  bool isLoadingSecondary = false;
  String? role;

  createTournament(Tournament tournament, BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? userId = sharedPreferences.getString('id');

    //finish tournament
    tournament.userId = userId!;

    print(tournament.toMap());

    var response = await authenticatedPost(API_CREATE_TOURNAMENT, tournament.toMap());
    var jsonData = jsonDecode(response.body);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(jsonData);
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("Stato creazione: "),
                content: Text(jsonData.toString()),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pushReplacementNamed('pages');
                      },
                      child: Text("ok"))
                ],
              ));
      return jsonData;
    } else {
      DefaultErrorDialog(
        subtitle: jsonData,
      );
    }
  }

  createLocation(Location location, BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? userId = sharedPreferences.getString('id');

    //finish location
    location.userId = userId!;

    //Da discutere con Fabio come acquisire le coordinate (oppure ereditarle)
    print(location.toMap());

    var response = await authenticatedPost(API_CREATE_LOCATION, location.toMap());
    var jsonData = jsonDecode(response.body);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(jsonData);
      isLoading = false;
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("Stato creazione torneo:"),
                content: Text(jsonData.toString()),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PagesBottomTabbar(
                              currentTab: 2,
                            ),
                          ),
                        );
                      },
                      child: Text("ok"))
                ],
              ));
      setState(() {});
      return jsonData;
    } else {
      print(jsonData);
      isLoading = false;
      showDialog(
          context: context,
          builder: (_) => DefaultErrorDialog(
                subtitle: jsonData.toString(),
              ));
      setState(() {});
    }
  }

  Future<Uri> getLocationApiUrl(String input, String language) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? userId = sharedPreferences.getString('id');
    String url =
        "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&types=address&language=$language&components=country:$language&key=$API_KEY_GOOGLE&sessiontoken=$userId'";
    return Uri.parse(url);
  }

  Future<List<Suggestion>> getListGoogleAddress(String input, String language) async {
    List<Suggestion> suggestionLis = [];
    Uri apiGoogleAutocomplete = await getLocationApiUrl(input, language);
    print(apiGoogleAutocomplete);
    var response = await apiGet(apiGoogleAutocomplete);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      if (result['status'] == 'OK') {
        // compose suggestions in a list
        suggestionLis = Suggestion().fromMapList(result["predictions"]);
        print(suggestionLis);
        return suggestionLis;
      }
      if (result['status'] == 'ZERO_RESULTS') {
        return suggestionLis;
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }

  Uri getCoordinatesApiUrl(String placeId) {
    String url = "https://maps.googleapis.com/maps/api/geocode/json?place_id=$placeId&key=$API_KEY_GOOGLE";
    return Uri.parse(url);
  }

  Future<LatLng> getLatLangFromPlaceId(String placeId) async {
    Uri apiUrl = getCoordinatesApiUrl(placeId);
    var response = await apiGet(apiUrl);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      if (result['status'] == 'OK') {
        // compose suggestions in a list
        LatLng latLang = LatLng(result["results"][0]["geometry"]["location"]["lat"], result["results"][0]["geometry"]["location"]["lng"]);
        return latLang;
      }
      if (result['status'] == 'ZERO_RESULTS') {
        return LatLng(0, 0);
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }

  Future<List<LocationInfo>> getLocationList() async {
    List<LocationInfo> locationsInfo = [];
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? id = sharedPreferences.getString('id');
    var response = await authenticatedGet(API_GET_LOCATION_LIST(id!));
    var jsonData;
    if (response.statusCode >= 200 && response.statusCode < 300) {
      jsonData = jsonDecode(response.body);
      locationsInfo = LocationInfo().fromMapList(jsonData);
      setState(() {
        isLoading = false;
      });
      return locationsInfo;
    } else {
      setState(() {
        isLoading = false;
      });
      return locationsInfo;
    }
  }

  Future<List<TournamentInfo>> getTournamentsList() async {
    List<TournamentInfo> tournamentsInfoList = [];
    var response = await authenticatedGet(API_GET_TOURNAMENTS_LIST);
    var jsonData;
    if (response.statusCode >= 200 && response.statusCode < 300) {
      jsonData = jsonDecode(response.body);
      tournamentsInfoList = TournamentInfo().fromMapList(jsonData);
      setState(() {
        isLoading = false;
      });
      return tournamentsInfoList;
    } else {
      setState(() {
        isLoading = false;
      });
      return tournamentsInfoList;
    }
  }

  Future<List<TournamentInfo>> getTournamentsListAdminID() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? idUser = sharedPreferences.getString('id');
    List<TournamentInfo> tournamentsInfoList = [];
    var response = await authenticatedGet(API_GET_TOURNAMENT_LIST_BY_ID(idUser!));
    var jsonData;
    if (response.statusCode >= 200 && response.statusCode < 300) {
      jsonData = jsonDecode(response.body);
      tournamentsInfoList = TournamentInfo().fromMapList(jsonData);
      setState(() {
        isLoading = false;
      });
      return tournamentsInfoList;
    } else {
      setState(() {
        isLoading = false;
      });
      return tournamentsInfoList;
    }
  }

  List<TournamentInfo> getTournamentListRange(List<TournamentInfo> bufferList, LatLng actualPosition, double range) {
    List<TournamentInfo> finalList = [];
    //check distance beetween coordinates --> range
    for (TournamentInfo tournament in bufferList) {
      if (distanceCalculator(
          actualPosition.latitude, actualPosition.longitude, tournament.location.latitude!, tournament.location.longitude!, 'K', range)) {
        finalList.add(tournament);
      }
    }
    return finalList;
  }

  Future<Tournament> getTournamentDetail(int id) async {
    var response = await authenticatedGet(API_GET_TOURNAMENT(id.toString()));
    var jsonData;

    if (response.statusCode >= 200 && response.statusCode < 300) {
      jsonData = jsonDecode(response.body);
      return Tournament.fromJSON(jsonData);
    } else
      return Tournament();
  }

  Future<Location> getLocationDetail(int id) async {
    List<Location> locationList = [];
    var response = await authenticatedGet(API_GET_LOCATION_DETAIL(id));
    var jsonData;

    if (response.statusCode >= 200 && response.statusCode < 300) {
      jsonData = jsonDecode(response.body);
      locationList = Location().fromMapList(jsonData);
      return locationList.first;
    } else
      return Future.error("bad response");
  }

  Future<String> editLocation(Location location, BuildContext context) async {
    var response = await authenticatedPut(API_UPDATE_LOCATION, location.toMap());
    var jsonData;

    if (response.statusCode >= 200 && response.statusCode < 300) {
      isLoading = false;
      jsonData = jsonDecode(response.body);
      setState(() {});
      return jsonData.toString();
    } else {
      isLoading = false;
      showDialog(context: context, builder: (_) => DefaultErrorDialog());
      setState(() {});
      return Future.error("bad response");
    }
  }

  deleteTournament(TournamentInfo tournamentInfo, BuildContext context, List<String> idRecivers) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? userId = sharedPreferences.getString('id');
    var response = await authenticatedDelete(API_DELETE_TOURNAMENT(tournamentInfo.id.toString(), userId!));
    var jsonData;
    if (response.statusCode >= 200 && response.statusCode < 300) {
      jsonData = jsonDecode(response.body);
      // sendNotification(
      //   "il torneo: " + tournamentInfo.title + "è stato eliminato.",
      //   "Il torneo " + tournamentInfo.title + " in data " + format.formatHour(tournamentInfo.dateScheduled) + " è stato eliminato.",
      //   idRecivers,
      // );
    } else {
      showDialog(
          context: context,
          builder: (_) => DefaultErrorDialog(
                buttonText: 'ok',
                subtitle: jsonData,
              ));
    }
  }

  deleteLocation(int locationId, BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? userId = sharedPreferences.getString('id');
    var response = await authenticatedDelete(API_DELETE_LOCATION(locationId.toString(), userId!));
    var jsonData = jsonDecode(response.body);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("Stato eliminazione: "),
                content: Text(jsonData.toString()),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PagesBottomTabbar(
                                      currentTab: 2,
                                    )));
                      },
                      child: Text("Ok"))
                ],
              ));
      //verificare che response da con postman e aggiungere dialog con aggiornamento stato al pop
    } else {
      showDialog(
          context: context,
          builder: (_) => DefaultErrorDialog(
                subtitle: jsonData.toString(),
                buttonText: "Ok",
              ));
    }
  }

  getControllerRole() async {
    getRole().then((value) {
      setState(() {
        role = value;
      });
    });
  }

  List<Friend> getSearchedList(String textWord, List<Friend> friendsList) {
    List<Friend> friendsListfiltered = [];
    bool isInside = false;
    for (Friend friend in friendsList) {
      if (friend.firstname.contains(textWord) || friend.lastname.contains(textWord) || friend.email.contains(textWord)) {
        friendsListfiltered.forEach((element) {
          if (element == friend) {
            isInside = true;
          }
        });
        if (!isInside) {
          friendsListfiltered.add(friend);
        } else {
          isInside = false;
        }
      }
    }
    return friendsListfiltered;
  }

  sendTournamentRequest(Tournament tournament, Friend friendTwo, User myUser, BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? player1ID = sharedPreferences.getString('id');
    Map requestMap = {
      "tournamentId": tournament.id.toString(),
      "playerOne": player1ID,
      "playerTwo": friendTwo.id,
    };
    var response = await authenticatedPost(API_SEND_TOURNAMENT_REQUEST, requestMap);
    var jsonData;
    if (response.statusCode >= 200 && response.statusCode < 300) {
      isLoading = false;
      jsonData = jsonDecode(response.body);
      sendNotification(
        "Richiesta partecipazione torneo",
        "Richiesta di partecipazione al torneo: " + tournament.title + " da parte di " + myUser.firstname + " " + myUser.lastname,
        [friendTwo.id],
      );
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("Stato richiesta: "),
                content: Text(jsonData.toString()),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PagesBottomTabbar(
                                      currentTab: 1,
                                    )));
                      },
                      child: Text("Ok"))
                ],
              ));
      setState(() {});
    } else {
      jsonData = jsonDecode(response.body);
      isLoading = false;
      showDialog(
          context: context,
          builder: (_) => DefaultErrorDialog(
                subtitle: jsonData.toString(),
              ));
      setState(() {});
    }
  }

  Future<List<EnrollTournament>> getTournamentRequestList() async {
    List<EnrollTournament> list = [];
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? idUser = sharedPreferences.getString('id');
    setState(() {
      isLoading = true;
    });
    var response = await authenticatedGet(API_GET_ENROLL_TOURNAMENT_LIST(idUser!));
    var jsonData = jsonDecode(response.body);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      isLoading = false;
      list = EnrollTournament().fromMapList(jsonData);
      setState(() {});
      return list;
    } else {
      isLoading = false;
      setState(() {});
      return list;
    }
  }

  Future<String> replyRequest(int idRequest, int idState, Tournament tournament, String idSender) async {
    var response = await authenticatedPut(API_REPLY_ENROLL_TOURNAMENT(idRequest, idState), {});
    var jsonData = jsonDecode(response.body);
    User? sender;
    await getMyUser().then((value) {
      sender = value;
    });
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(jsonData);
      //INVIO NOTIFICA DA FAREEEEEEEEEEEEEEEEEEEEEEEE
      sendNotification(
        "Richiesta di partecipazione accettata",
        sender!.firstname + " " + sender!.lastname + " ha accettato la richiesta di partecipazione al torneo: " + tournament.title + ".",
        [idSender],
      );
      return jsonData.toString();
    } else if (response.statusCode == 300) {
      sendNotification(
        "Torneo " + tournament.title + " è al completo",
        jsonData.toString(),
        [idSender],
      );
      return jsonData.toString();
    } else {
      //status code 300: mandare notifica all'utente mandate qunado numero massimo partecipanti torneo
      print(jsonData);
      return jsonData.toString();
    }
  }

  Future<List<EnrollTournament>> getEnrollScheduled() async {
    List<EnrollTournament> list = [];
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? idUser = sharedPreferences.getString('id');
    setState(() {
      isLoading = true;
    });
    var response = await authenticatedGet(API_GET_ENROLL_SCHEDULED(idUser!));
    var jsonData = jsonDecode(response.body);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      isLoading = false;
      list = EnrollTournament().fromMapList(jsonData);
      setState(() {});
      return list;
    } else {
      isLoading = false;
      setState(() {});
      return list;
    }
  }

  Future<List<EnrollTournament>> getEnrollWaiting() async {
    List<EnrollTournament> list = [];
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? idUser = sharedPreferences.getString('id');
    setState(() {
      isLoading = true;
    });
    var response = await authenticatedGet(API_GET_ENROLL_WAITING(idUser!));
    var jsonData = jsonDecode(response.body);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      isLoading = false;
      list = EnrollTournament().fromMapList(jsonData);
      setState(() {});
      return list;
    } else {
      isLoading = false;
      setState(() {});
      return list;
    }
  }

  Future<List<EnrollTournament>> getEnrollEnded() async {
    List<EnrollTournament> list = [];
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? idUser = sharedPreferences.getString('id');
    setState(() {
      isLoading = true;
    });
    var response = await authenticatedGet(API_GET_ENROLL_ENDED(idUser!));
    var jsonData = jsonDecode(response.body);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      isLoading = false;
      list = EnrollTournament().fromMapList(jsonData);
      setState(() {});
      return list;
    } else {
      isLoading = false;
      setState(() {});
      return list;
    }
  }

  Future<List<EnrollTournament>> getEnrollDeleted() async {
    List<EnrollTournament> list = [];
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? idUser = sharedPreferences.getString('id');
    setState(() {
      isLoading = true;
    });
    var response = await authenticatedGet(API_GET_ENROLL_KILLED(idUser!));
    var jsonData = jsonDecode(response.body);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      isLoading = false;
      list = EnrollTournament().fromMapList(jsonData);
      setState(() {});
      return list;
    } else {
      isLoading = false;
      setState(() {});
      return list;
    }
  }

  editTournament(Tournament editedTournament, BuildContext context) async {
    var response = await authenticatedPut(API_UPDATE_TOURNAMENT, editedTournament.toMap());
    var jsonData = jsonDecode(response.body);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text("Stato aggiornamento torneo: "),
          content: Text(jsonData.toString()),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PagesBottomTabbar(),
                    ),
                  );
                },
                child: Text("Ok"))
          ],
        ),
      );
    } else {
      print(jsonData);
      showDialog(
        context: context,
        builder: (_) => DefaultErrorDialog(
          title: "Stato aggiornamento torneo: ",
          subtitle: jsonData.toString(),
        ),
      );
      ;
    }
  }

  Future<String> deleteEnroll(int idEnroll) async {
    var response = await authenticatedPut(API_DELETE_ENROLL_TORUNAMENT(idEnroll), {});
    var jsonData = jsonDecode(response.body);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(jsonData);
      return jsonData.toString();
    } else {
      //status code 300: mandare notifica all'utente mandate qunado numero massimo partecipanti torneo
      print(jsonData);
      return jsonData.toString();
    }
  }

  Future<List<TeamsPartecipant>> getPartecipantTeams(int tournamentId) async {
    List<TeamsPartecipant> partecipantsList = [];
    var response = await authenticatedGet(API_GET_PARTECIPANTS_LIST(tournamentId));
    var jsonData;
    if (response.statusCode >= 200 && response.statusCode < 300) {
      jsonData = jsonDecode(response.body);
      partecipantsList = TeamsPartecipant().fromMapList(jsonData);
      return partecipantsList;
    } else
      return partecipantsList;
  }

  Future<bool> sendAbsentTeams(List<int>? absentTeamsList, int? idTournament, List<TeamsPartecipant> partecipantsList) async {
    setState(() {
      isLoading = true;
    });
    var list = ["32", "33"];

    Map absentMap = {
      "idTournament": jsonEncode(idTournament!),
      "listAbsent": jsonEncode(list).toString(),
    };
    var response = await authenticatedPost(API_TOURNAMENTS_START, absentMap);
    var jsonData = jsonDecode(response.body);
    print(jsonData);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return true;
    } else {
      setState(() {
        isLoading = false;
      });
      return false;
    }
  }
}
