import 'dart:convert';

import 'package:calciobalilla24/models/CredentialLogin.dart';
import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/screens/auth/forgot_password/forgot_password.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/default_error_dialog.dart';
import 'package:calciobalilla24/widgets/pages.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthController extends ControllerMVC {
  bool isLoading = false;

  Future<String> getFcmToken() async {
    String? fcmToken = await FirebaseMessaging.instance.getToken();
    print("fcm token: " + fcmToken!);
    return fcmToken;
  }

  login(BuildContext context, CredentialLogin userCredentialLogin) async {
    String? deviceInfoID;
    //CredentialLogin credentialLogin = new CredentialLogin();
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await getFcmToken().then((value) {
      userCredentialLogin.deviceID = value;
    });
    // await getDeviceID().then((value) {
    //   userCredentialLogin.deviceID = value.toString();
    // });
    var response = await apiPost(API_LOGIN, userCredentialLogin.toMap());
    var jsonData;
    if (response.statusCode >= 200 && response.statusCode < 300) {
      isLoading = false;
      jsonData = jsonDecode(response.body);
      //save token in device memory
      getFcmToken();
      print("token " + jsonData['access_token']);
      print("id " + jsonData['id']);
      sharedPreferences.setString('token', jsonData['access_token']);
      sharedPreferences.setString('id', jsonData['id']);
      //aggiungere il controllo quando si aggiungerà il ruolo (passato da API)
      sharedPreferences.setString('role', jsonData['appRole']);

      if (jsonData['access_token'] != null) {
        Navigator.of(context).pushReplacementNamed('pages');
      }
      setState(() {});
    } else {
      jsonData = jsonDecode(response.body);
      if (jsonData['Message'] != null) {
        isLoading = false;
        showDialog(
            context: context,
            builder: (_) => DefaultErrorDialog(
                  subtitle: jsonData['Message'],
                ));

        setState(() {});
      } else if (jsonData['error'] != null) {
        isLoading = false;
        showDialog(
            context: context,
            builder: (_) => DefaultErrorDialog(
                  title: jsonData['error'],
                  subtitle: jsonData['error_description'],
                ));
        setState(() {});
      } else {
        isLoading = false;
        showDialog(context: context, builder: (_) => DefaultErrorDialog());
      }
    }
  }

  signin(BuildContext context, User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var response = await apiPost(API_REGISTRAZIONE, user.toMap());
    var jsonData = jsonDecode(response.body);
    print(response);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print("risposta(buon fine): " + response.body.toString());
      setState(() {
        isLoading = false;
      });
      showDialog(
          context: context,
          builder: (_) => new AlertDialog(
                title: new Text("Congratulazioni!"),
                content: new Text(jsonData),
                actions: <Widget>[
                  TextButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pushReplacementNamed('login');
                    },
                  )
                ],
              ));
    } else {
      print("risposta(bad): " + response.body.toString());
      if (response.statusCode == 400) {
        showDialog(
            context: context,
            builder: (_) => new AlertDialog(
                  title: new Text("Attenzione!"),
                  content: new Text(jsonData),
                  actions: <Widget>[
                    TextButton(
                      child: Text('Ok'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                ));
      } else
        showDialog(context: context, builder: (_) => DefaultErrorDialog());
    }
  }

  forgotPassword(BuildContext context, String email) async {
    Map usernameJson = {"username": email};
    var response = await apiPost(API_FORGOT_PASSWORD, usernameJson);
    var jsonData = jsonDecode(response.body);
    print(jsonData);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      setState(() {
        isLoading = false;
      });
      showDialog(
          context: context,
          builder: (_) => new AlertDialog(
                title: new Text("Congratulazioni!"),
                content: new Text(jsonData),
                actions: <Widget>[
                  TextButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pushReplacementNamed("login");
                    },
                  )
                ],
              ));
    } else {
      if (jsonData['Message'] != null) {
        setState(() {
          isLoading = false;
        });
        showDialog(
            context: context,
            builder: (_) => DefaultErrorDialog(
                  title: 'Attenzione!',
                  subtitle: jsonData['Message'],
                ));
      } else {
        setState(() {
          isLoading = false;
        });
        showDialog(context: context, builder: (_) => DefaultErrorDialog());
      }
    }
  }

  updateProfile(User updatedUser, BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? id = sharedPreferences.getString('id');
    String? role = sharedPreferences.getString('role');
    User myUser = User();
    List<User> users = [];
    var response = await authenticatedPut(API_UPDATE_USER(id!), updatedUser.toMap());
    var jsonData;
    if (response.statusCode >= 200 && response.statusCode < 300) {
      isLoading = false;
      jsonData = jsonDecode(response.body);
      showDialog(
          context: context,
          builder: (_) => new AlertDialog(
                title: new Text("Congratulazioni!"),
                content: new Text(jsonData),
                actions: <Widget>[
                  TextButton(
                    child: Text('Ok'),
                    onPressed: () {
                      if (role == 'user') {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PagesBottomTabbar(
                              currentTab: 3,
                            ),
                          ),
                        );
                      } else {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PagesBottomTabbar(
                              currentTab: 2,
                            ),
                          ),
                        );
                      }
                    },
                  )
                ],
              ));
    } else {
      isLoading = false;
      jsonData = jsonDecode(response.body);
      print(jsonData);
      showDialog(context: context, builder: (_) => DefaultErrorDialog());
    }
  }
}
