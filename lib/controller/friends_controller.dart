import 'dart:async';
import 'dart:convert';

import 'package:calciobalilla24/models/Friend.dart';
import 'package:calciobalilla24/models/FriendRequest.dart';
import 'package:calciobalilla24/models/User.dart';
import 'package:calciobalilla24/repositories/friend_repo.dart';
import 'package:calciobalilla24/repositories/notification_repo.dart';
import 'package:calciobalilla24/utils/commons_methods.dart';
import 'package:calciobalilla24/utils/global_constant.dart';
import 'package:calciobalilla24/widgets/default_error_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FriendsController extends ControllerMVC {
  List<Friend>? friendsList = [];
  int _start = 1;
  String keywardNextList = "";
  bool isLoading = false;
  Timer? _timer;

  void updateList(String keyward) async {
    if (_timer != null) _timer!.cancel();
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) async {
        if (_start == 0) {
          print(isLoading);
          fetchFirstList(keyward);
          setState(() {
            isLoading = false;
            keywardNextList = keyward;
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  fetchFirstList(String keyward) async {
    List<Friend> bufferList = await searchFriends(keyward, PAGINATOR_LIST_LENGHT, 0);
    setState(() {
      friendsList = bufferList;
    });
  }

  fetchNextList() async {
    List<Friend> bufferList = await searchFriends(keywardNextList, PAGINATOR_LIST_LENGHT, friendsList!.length);
    setState(() {
      for (Friend newFriend in bufferList) {
        friendsList!.add(newFriend);
      }
    });
  }

  sendRequest(String idReciver, BuildContext context, User myUser) async {
    var response = await sendFriendRequest(idReciver);
    var jsonData = jsonDecode(response.body);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      if (jsonData != null) {
        sendNotification("Richiesta d'amicizia",
            "nuova richiesta d'amicizia da parte di " + myUser.firstname + " " + myUser.lastname + ".\n" + "email: " + myUser.email, [idReciver]);
        showDialog(
            context: context,
            builder: (_) => AlertDialog(
                  title: Text("Stato richiesta:"),
                  content: Text(
                    jsonData.toString(),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text("Ok"))
                  ],
                ));
      }
    } else {
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("Stato richiesta:"),
                content: Text(
                  jsonData.toString(),
                ),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text("Ok"))
                ],
              ));
    }
  }

  getFriendsRequestList() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? idUser = sharedPreferences.getString('id');
    List<FriendRequest> requestsList = [];
    var response = await authenticatedGet(API_GET_REQUESTS_LIST(idUser!));
    var jsonData;
    if (response.statusCode >= 200 && response.statusCode < 300) {
      isLoading = false;
      jsonData = jsonDecode(response.body);
      requestsList = FriendRequest().fromMapList(jsonData);
      setState(() {});
      return requestsList;
    } else {
      return requestsList;
    }
  }

  getFriends() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? id = sharedPreferences.getString('id');
    List<Friend> localFriendsList = [];
    var response = await authenticatedGet(API_GET_FRIENDS(id!));
    var jsonData;
    if (response.statusCode >= 200 && response.statusCode < 300) {
      isLoading = false;
      jsonData = jsonDecode(response.body);
      localFriendsList = Friend().fromMapList(jsonData);
      setState(() {});
      return localFriendsList;
    } else
      return localFriendsList;
  }
}
